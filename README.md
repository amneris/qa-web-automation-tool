# QA web automation Framework#



To write and run tests you do not need to compile webdriver –lib, so skip this and go to Getting started if you are
not interested in modify/add keywords.

### To modify and compile you will need to install:#

* [Maven] (http://maven.apache.org/docs/2.2.1/release-notes.html)
* [Java JDK] (http://www.oracle.com/technetwork/es/java/javase/downloads/index.html)
* [IntelliJ]  (https://www.jetbrains.com/idea/)

#The source code of webdriver lib is divided in:#

* [WebDriverLib.java]  Main class for the library

* [BrowserActions.java]  Contains all the keywords related to browser actions

* [ElementActions.java]  Contains all the keywords related to page elements actions

* [Get.java]  Contains all the keywords related to get info from the page

* [Should.java]  Contains all the keywords related to assert conditions

There are as well few utility classes to handle waits and common methods.

#A nice tutorial about “how create java libs for Robot framework”

* https://github.com/robotframework/JavalibCore/wiki/Getting-Started


### Run all tests with maven:
```
mvn clean integration-test
```

Run only a set of tests by tags:
```
mvn clean integration-test -Dincludes=example
```

Override robot framework variables:
```
mvn clean integration-test -Dincludes=smoke -Dvariables=REMOTE_SERVER:http://selenium.aba.land:4444/wd/hub
```
### Getting started

Let&#39;s write our first test suite in Robot Framework syntax
```
*** Variables ***
${LOGIN_EN_URL}	https://campus-qa.aba.land/en/login
${HOME_PAGE}	https://campus-qa.aba.land
```

```
*** Test Cases ***

I want to verify if there is a valid error message in case entering a valid email without abaEnglish account
	[Documentation]	valid email without aba account
	[Tags]	medium
	Given login page is open
	then I want to login as	aa@aba.com	passs
	then credentials are submitted
	and I should receive error message in case of credentials does not exist on the databace

I want to access to aba English web site with valid acount
	[Documentation]	I want go to access to aba english web site
	[Tags]	smoke
	Given login page is open
	then I want to login as	${SP_USER}	${SP_PASS}
	then credentials are submitted
	after entering valid credentials I should se my account
	and I close crazy popup
	Logout

```

```
*** Keywords ***

I want to login as
	[Arguments]	${email}	${password}
	#I clear local storage
	login page is open
    type	id=LoginForm_email	${email}
    type	id=LoginForm_password	${password}

I go to the aba english campus
	open page	${HOME_PAGE}

credentials are submitted
    click	id=btnStartSession

```


### How to write good Robot Framework test cases

*  https://github.com/robotframework/HowToWriteGoodTestCases/blob/master/HowToWriteGoodTestCases.rst

How run our tests
```
java -jar abaWebDriver.jar  --include stripe src/qa/1.paymentfunnel.robot
```

If you want to run all test inside a folder…

```
Java –jar abaWebDriver.jar  src/test/resources/examples/
```

Check test results

Robot will generate a file named [report.html] inside the same folder of the jar, so open this file to see the test

results.