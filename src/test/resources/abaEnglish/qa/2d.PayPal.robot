*** Settings ***
Resource	config.robot

Metadata    Version        Paypal  Payment method
Metadata	Feature	       ZOR-95
Metadata    Executed At    ${HOME_PAGE} environment

#Suite Set Up	openRemoteBrowser	${BROWSER}	${SITE URL}	${REMOTE SERVER}
Suite Set Up	openBrowser	${BROWSER}	${SITE URL}
#Test Tear Down	close browser
Suite Tear Down	close browser
Test Set Up	clear cookies
Test Tear Down	Run Keywords
...				Run Keyword If Test Failed	get screen shot	AND
...				Run Keyword If Test Failed	Logout
Default Tags	java_aps

#Run Keyword If Test Failed	Logout
*** Test Cases **

#### STRIPE

Check if the paypal option button, terms and conditions options are available when the user selects paypal option to purchase.
	[Documentation]	ZOR-95 Paypal
	[Timeout]	40s
	[Tags]	medium
	Given an user logged in payment-funnel from country and segment	es	1
	then I click on select button of monthly plan
	sleep	2s
	Country should have PayPal method available
	I select PayPal option to purchase
	PayPal button shoud be available after User selects Paypal to purchase
	Terms and conditions link should be shown
	Privacy Policy link should be shown
	Logout

user should be redirected to the PayPal website
    [Documentation]	ZOR-95 Paypal
    [Timeout]	40s
    [Tags]	medium
    Given an user logged in payment-funnel from country and segment	es	1
    then I click on select button of Six-monthly plan
    sleep	2s
    Country should have PayPal method available
    I select PayPal option to purchase
    I click on Paypal Button
    user should be redirected to paypal web site