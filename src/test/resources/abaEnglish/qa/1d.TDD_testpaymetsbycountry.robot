*** Settings ***
Resource	config.robot

Metadata    Version        java apps V5
Metadata	Feature	       plans and prices by country
Metadata    Executed At    ${HOME_PAGE} environment

Suite Set Up	openRemoteBrowser	${BROWSER}	${SITE URL}	${REMOTE SERVER}
#Suite Set Up	openBrowser	${BROWSER}	${SITE URL}
#Test Tear Down	close browser
Suite Tear Down	close browser
Test Set Up	clear cookies
Test Tear Down	Run Keyword If Test Failed	logout
Default Tags	main
Test Template	Login in different countries and check if the user is able to pay


*** Test Cases **
Afghanistan
    [Tags]	main
    ${Afghanistan}
Albania
    [Tags]	main
    ${Albania}
Algeria
    [Tags]	main
    ${Algeria}
AmericanSamoa
    [Tags]	main
    ${AmericanSamoa}
Andorra
    [Tags]	main
    ${Andorra}
Angola
    [Tags]	main
    ${Angola}
Anguilla
    [Tags]	main
    ${Anguilla}
Antarctica
    [Tags]	main  failQA
    ${Antarctica}
Argentina
     [Tags]	medium
    ${Argentina}
Armenia
    ${Armenia}
Aruba
    ${Aruba}
Australia
     [Tags]	medium
    ${Australia}
Austria
    ${Austria}
Azerbaijan
    ${Azerbaijan}
Bahamas
    ${Bahamas}
Bahrain
    ${Bahrain}
Bangladesh
    ${Bangladesh}
Barbados
    ${Barbados}
Belarus
    ${Belarus}
Belgium
    ${Belgium}
Belize
    ${Belize}
Benin
    ${Benin}
Bermuda
    ${Bermuda}
Bhutan
    ${Bhutan}
Bolivia
    ${Bolivia}
BosniaHerzegovina
    ${BosniaHerzegovina}

Botswana
    ${Botswana}
BouvetIsland
    [Tags]	main  failQA
    ${BouvetIsland}
Brazil
    ${Brazil}
BritishIndianOcean
    [Tags]	main  failQA
    ${BritishIndianOcean}
BruneiDarussalam
    ${BruneiDarussalam}
Bulgaria
    ${Bulgaria}
BurkinaFaso
    ${BurkinaFaso}
Burundi
    ${Burundi}
Cambodia
    ${Cambodia}
Cameroon
    ${Cameroon}
Canada
    [Tags]	medium
    ${Canada}
CapeVerde
    ${CapeVerde}
CaymanIslands
    ${CaymanIslands}
CentralAfricanRepublic
    ${CentralAfricanRepublic}
Chad
    ${Chad}
Chile
    [Tags]	medium
    ${Chile}
China
    ${China}
ChristmasIsland
    [Tags]	main  failQA
    ${ChristmasIsland}
CocosIslands
    [Tags]	main  failQA
    ${CocosIslands}
Colombia
    [Tags]	smoke
    ${Colombia}
Comoros
    ${Comoros}
Congo
    ${Congo}
CongoRepublic
    ${CongoRepublic}
CookIslands
    ${CookIslands}
CostaRica
    ${CostaRica}
CoteDIvoire
    ${CoteDIvoire}
Croatia
    ${Croatia}
Cuba
    ${Cuba}
Cyprus
    ${Cyprus}
CzechRepublic
    ${CzechRepublic}
Denmark
    ${Denmark}
Djibouti
    ${Djibouti}
Dominica
    ${Dominica}
DominicanRepublic
    ${DominicanRepublic}
Ecuador
    [Tags]	medium
    ${Ecuador}
Egypt
    ${Egypt}
ElSalvador
    ${ElSalvador}
EquatorialGuinea
    ${EquatorialGuinea}
Eritrea
    ${Eritrea}
Estonia
    ${Estonia}
Ethiopia
    ${Ethiopia}
FalklandIslands
    ${FalklandIslands}
FaroeIslands
    ${FaroeIslands}
Fiji
    ${Fiji}
Finland
    ${Finland}
France
    [Tags]	medium
    ${France}
FrenchGuiana
    ${FrenchGuiana}
FrenchPolynesia
    ${FrenchPolynesia}
Gabon
    ${Gabon}
Gambia
    ${Gambia}
Georgia
    ${Georgia}
Germany
    ${Germany}
Ghana
    ${Ghana}
Gibraltar
    ${Gibraltar}
Greece
    [Tags]	medium
    ${Greece}
Greenland
    ${Greenland}
Grenada
    ${Grenada}
Guadeloupe
    ${Guadeloupe}
Guam
    ${Guam}
Guatemala
    ${Guatemala}
Guinea
    ${Guinea}
GuineaBissau
    ${GuineaBissau}
Guyana
    ${Guyana}
Haiti
    ${Haiti}
Honduras
    ${Honduras}
HongKong
    ${HongKong}
Hungary
    [Tags]	medium
    ${Hungary}
Iceland
    ${Iceland}
India
    [Tags]	medium
    ${India}
Indonesia
    ${Indonesia}
Iran
    ${Iran}
Iraq
    ${Iraq}
Ireland
    ${Ireland}
Israel
    ${Israel}
Italy
    [Tags]	smoke
    ${Italy}
Jamaica
    ${Jamaica}
Japan
    ${Japan}
Jordan
    ${Jordan}
Kazahgstan
    ${Kazakhstan}
Kenya
    ${Kenya}
Kiribati
    ${Kiribati}
Korea
    [Tags]	medium
    ${KoreaRepublic}
Kuwait
    ${Kuwait}
Kyrgyzstan
    ${Kyrgyzstan}
Lao
    ${Lao}
Latvia
    ${Latvia}
Lebanon
    ${Lebanon}
Lesotho
    ${Lesotho}
Liberia
    ${Liberia}
Libyan
    ${Libyan}
Liechtenstein
    ${Liechtenstein}
Lithuania
    ${Lithuania}
Luxembourg
    ${Luxembourg}
Macao
    ${Macao}
Macedonia
    ${Macedonia}
Madagascar
    ${Madagascar}
Malawi
    ${Malawi}
Malaysia
    ${Malaysia}
Maldives
    ${Maldives}
Mali
    ${Mali}
Malta
    ${Malta}
MarshallIslands
    ${MarshallIslands}
Martinique
    ${Martinique}
Mauritania
    ${Mauritania}
Mauritius
    ${Mauritius}
Mayotte
    [Tags]	main  failQA
    ${Mayotte}
Mexico
    [Tags]	smoke
    ${Mexico}
Micronesia
    ${Micronesia}
MoldovaRepublic
    ${MoldovaRepublic}
Monaco
    ${Monaco}
Mongolia
    ${Mongolia}
Montserrat
    ${Montserrat}
Morocco
    ${Morocco}
Mozambique
    ${Mozambique}

Myanmar
    ${Myanmar}
Namibia
    ${Namibia}
Nauru
    ${Nauru}
Nepal
    ${Nepal}
Netherlands
    ${Netherlands}
NetherlandsAntilles
    [Tags]	main  failQA
    ${NetherlandsAntilles}
NewCaledonia
    ${NewCaledonia}
NewZealand
    ${NewZealand}
Nicaragua
    ${Nicaragua}
Niger
    ${Niger}
Nigeria
    ${Nigeria}
Niue
    ${Niue}
NorfolkIsland
    ${NorfolkIsland}
NorthernMarianaIslands
    ${NorthernMarianaIslands}
Norway
    ${Norway}
Oman
    ${Oman}
Pakistan
    ${Pakistan}
Palau
    ${Palau}
PalestinianTerritory
    [Tags]	main  failQA
    ${PalestinianTerritory}
Panama
    ${Panama}
PapuaNewGuinea
    ${PapuaNewGuinea}
Paraguay
    ${Paraguay}
Peru
    ${Peru}
Philipines
    ${Philippines}
Pitcairn
    ${Pitcairn}
Poland
    [Tags]	medium
    ${Poland}
Portugal
    ${Portugal}
Puerto Rico
    ${PuertoRico}
Qatar
    ${Qatar}
Reunion
    ${Reunion}
Romania
    [Tags]	medium
    ${Romania}
Russian Federation
    ${RussianFederation}
Rwanda
    ${Rwanda}
Samoa
    ${Samoa}
SanMarino
    ${SanMarino}
SaudiArabia
    ${SaudiArabia}
Senegal
    ${Senegal}
Seychelles
    ${Seychelles}
Singapore
    ${Singapore}
Slovakia
    ${Slovakia}
Slovenia
    ${Slovenia}
Solomo islans
    ${SolomonIslands}
Somalia
    ${Somalia}
SoutAfrica
    ${SouthAfrica}
Spain
    [Tags]	smoke
    ${Spain}
Srilanka
    ${SriLanka}
Sudan
    ${Sudan}
Suriname
    ${Suriname}
Swaziland
    ${Swaziland}
Sweden
    ${Sweden}
Switzerland
    ${Switzerland}
Taiwan
    ${Taiwan}

Tajikistan
    ${Tajikistan}
Tanzana
    ${Tanzania}
Thailand
    ${Thailand}
Timor Leste
    [Tags]	main  failQA
    ${TimorLeste}
Togo
    ${Togo}
Tokelau
    ${Tokelau}
Tonga
    ${Tonga}
Trinidad tobago
    ${TrinidadTobago}

Tunisia
    ${Tunisia}
Turkey
    [Tags]	medium
    ${Turkey}
Tukmenistan
    ${Turkmenistan}
Tuvalu
    ${Tuvalu}
Uganda
    ${Uganda}
Ukraine
    ${Ukraine}
ArabEmirates
    ${ArabEmirates}
United Kindom
    ${UnitedKingdom}

USA
    ${UnitedStates}
Uriguay
    ${Uruguay}
Uzbekistan
    ${Uzbekistan}
Vanuatu
    ${Vanuatu}
Venezuela
    ${Venezuela}
Vietman
    ${VietNam}
Yemen
    ${Yemen}
Zambia
    ${Zambia}
Zimbabwe
    ${Zimbabwe}
