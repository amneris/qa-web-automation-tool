*** Settings ***
Resource	config.robot

Documentation  	test cases when the customer decides to cancel, renew or refound  a subscription, depend of the status the user should be free or premium user



Metadata    Version        java apps V5
Metadata	Feature	       payments ans subscriptions working with zuora
Metadata    Executed At    ${HOME_PAGE} environment

Suite Set Up	openRemoteBrowser	${BROWSER}	${SITE URL}	${REMOTE SERVER}
#Suite Set Up	openBrowser	${BROWSER}	${SITE URL}
#Test Tear Down	close browser
Suite Tear Down	close browser
Test Set Up	clear cookies
Test Tear Down	Run Keywords
...				Run Keyword If Test Failed	get screen shot	AND
...				Run Keyword If Test Failed	Logout  AND
...				Run Keyword If Test Failed	In zuora I click on logout
Default Tags	java_aps

#Run Keyword If Test Failed	Logout
*** Test Cases **

#intranet
#    I want to login in intranet    aospina@abaenglish.com    q1w2e3r4!
#    I go to user section filtered by account user    9345510
#    User should have user type  Premium
#  #I go to payments section filtered by account user    9345510
#   #in payments section I filter by status   SUCCESS
#    sleep  2s
#    #I go to users section
#    #I enter the userid in table    9345510
#

Scenary in case of the user decides to cancel the premium subscription en guaranty time. So I cancel the subscription effective immediately
    [Documentation]	ZOR-500 cancel
    [Timeout]	120s
    [Tags]	cancel   NewFunctionality
    given I want the time
    Given a new user logued in the campus   Arya Stark -Cancel Inmediatey
    then the created user should have idcountry num  ${Germany}   ${NEWUSEREMAIL}
    then I go to the plan page with segment number   2
    then I click on select button of monthly plan
    and I pay with credit card option
    confirmation payment page should be displayed
    I get the subscription identifier
    Given I login in zuora
    then I go to Subscriptions list
    and I access to the suscription      ${SubscriptionID}
    then I cancel the subscription by Entering specific dates on fields   ${dateAct}  ${dateAct}
    The subscription information should be updated
    In zuora I click on logout
    sleep   3s
    I go to the aba english campus
    then free users should have the option to go to plans page
    sleep   6s
    logout


Scenary in case of the user decides to cancel the comming renovation. So I cancel the subscription at the end of the current term
    [Documentation]	ZOR-500  cancel current term
    [Timeout]	120s
    [Tags]	cancel   NewFunctionality
    given I want the time
    Given a new user logued in the campus   Tarlson Rak - Cancel-EndOfCurrentTime
    then the created user should have idcountry num  ${italy}   ${NEWUSEREMAIL}
    then I go to the plan page with segment number   3
    then I click on select button of monthly plan
    and I pay with credit card option
    confirmation payment page should be displayed
    I get the subscription identifier
    Given I login in zuora
    then I go to Subscriptions list
    and I access to the suscription      ${SubscriptionID}
    I cancel the subscription at the end of the current term    ${dateAct}
    The subscription information should be updated
    In zuora I click on logout
    sleep   2s
    I go to the aba english campus
    users with premium suscription should not have the option to go to plans page
    sleep   5s
#     #ir a intranet y verificar fecha
     logout
###### TODO cancel a renewal


user decides to Refund his suscription effective immediately
    [Documentation]	ZOR-500 full refund
    [Timeout]	120s
    [Tags]	refund   NewFunctionality
    given I want the time
    Given a new user logued in the campus   Erick Cruz - Refund now
    then the created user should have idcountry num  ${italy}   ${NEWUSEREMAIL}
    then I go to the plan page with segment number   3
    then I click on select button of monthly plan
    and I pay with credit card option
    confirmation payment page should be displayed
    I get the subscription identifier
    Given I login in zuora
    then I go to Subscriptions list
    and I access to the suscription   ${SubscriptionID}
    #${SubscriptionID}
    then I cancel the subscription by Entering specific dates on fields   ${dateAct}  ${dateAct}
    The subscription information should be updated
    I click on the Customer Name link
    in transactions I click on payments
    I click on the payment number
    I click on the more details button
    Click on Refund this payment
    Click on yes button
    I want to comment the refund
    I click on confirm refund
    In zuora I click on logout
    I go to the aba english campus
    then free users should have the option to go to plans page
     sleep   4s
#      #TODO VERIFICAR AMMOUNT EN INTRANET Y FECHA
    logout
#

Parcial Refund suscription
    [Documentation]	ZOR-500 Parcial Refund suscription
    [Timeout]	120s
    [Tags]	refund   NewFunctionality
    given I want the time
    Given a new user logued in the campus   Emilia Clarke - ParcialRefund
    then the created user should have idcountry num  ${italy}   ${NEWUSEREMAIL}
    then I go to the plan page with segment number   2
    then I click on select button of annual plan
    and I pay with credit card option
    confirmation payment page should be displayed
    I get the subscription identifier
    Given I login in zuora
    then I go to Subscriptions list
    and I access to the suscription   ${SubscriptionID}
    I click on the Customer Name link
    in transactions I click on payments
    I click on the payment number
    I click on the more details button
    Click on Refund this payment
    Click on yes button
    I want to comment the refund
    I enter de refund ammount   10.00
    I click on confirm refund
    In zuora I click on logout
    I go to the aba english campus
    users with premium suscription should not have the option to go to plans page
         #TODO VERIFICAR AMMOUNT EN INTRANET Y FECHA
   logout
   I want to login in intranet    aospina@abaenglish.com    q1w2e3r4!
   I go to user section filtered by account user     ${AbaUserID}
   User should have user type  Premium
   sleep    5s
   I go to payments section filtered by account user    ${AbaUserID}
   sleep    5s


#renewal

#A new subscription can be created after user cancel the current subscription
#    [Tags]  zoura   createSubscription  test1
#    Given I login in zuora
#    And I go to the subcription     A-S00004190
#    When I cancel the subscription
#    And I go to the create a subscription page
#    And I complete the create subscription information for one month
#    Then new subscription should be activated
#
#A new subscription can be created after user cancel the current subscription
#    [Tags]  zoura   createSubscription
#    Given I login in zuora
#    And I go to the subcription     A-S00004026
#    When I cancel the subscription
#    And I go to the create a subscription page
#    And I complete the create subscription information for one month
##    Then new subscription should be activated
#    And I go to the amendment area
#    And I complete the information to create the amendment
#    And I activate the amendment
#    And I go to the customer account
#    And I save the account number
#    And I execute the create bin run for the current subscription
#    Then I confirm the bill status is Pending