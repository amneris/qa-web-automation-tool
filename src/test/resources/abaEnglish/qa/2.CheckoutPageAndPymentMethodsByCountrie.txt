*** Settings ***
Resource	config.robot


Documentation   ZOR-171 As a Head of Monetization I want an adaptive design so that I can be able to add new payment methods
...				ZOR-94 As a Head of Monetization I want to select which payment gateways should be displayed per country so that I can optimize sales per payment gateway

Metadata    Version        java apps V5
Metadata	Feature	       ZOR-94 ZOR-171
Metadata    Executed At    ${HOME_PAGE} environment

Suite Set Up	openRemoteBrowser	${BROWSER}	${SITE URL}	${REMOTE SERVER}
#Suite Set Up	openBrowser	${BROWSER}	${SITE URL}
#Test Tear Down	close browser
Suite Tear Down	close browser
Test Set Up	clear cookies
Test Tear Down	Run Keywords
...				Run Keyword If Test Failed	get screen shot	AND
...				Run Keyword If Test Failed	Logout	
Default Tags	java_aps

#Run Keyword If Test Failed	Logout
*** Test Cases **


I want to Check if each of the payment options are selectable
	[Documentation]	ZOR-94 Check if each of the payment options are selectable
	[Timeout]	40s
	[Tags]	smoke
	Given an autologed user in plan page with country and segment	es	1
	then I click on select button of monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	Credit card number should be available
	Select Main Frame
	I want to pay by BankTransfer
	Bank Acoount name should be available
	Select Main Frame
	I click on paypal payment method

With an user register in spain -I want to go to checkout page and compare user details, prices and currency of the selected plan, finally confirm the payment methods available for the country
	[Documentation]	ZOR-94 ZOR-171
	[Timeout]	40s
	[Tags]	smoke
	Given an autologed user in plan page with country and segment	es	1
	The Currency of the plan page should be on	${EUR}
	then I click on select button of monthly plan
    and checkout page should contain name of the user	qastudent
    and checkout page should contain email of the user	${ES}
    and checkout page should contain selected plan name	Monthly
    and The price of the selected plan should be the same of checkout page	${MYPLANPRICE1M}
    and checkout page should contain a currency on	${EUR}
    and I compare The renewal-date displayed with the number of days of the selected plan	30 days
    then Credit Card option via stripe should be present
    and BankTransfer option via SEPA should be present
    and Country should have PayPal method available
    Logout

With an user register in Portugal -I want to go to checkout page compare user details, prices and currency of the selected plan, finally confirm the payment methods available for the country
	[Documentation]	ZOR-94 ZOR-171
	[Timeout]	40s
	[Tags]	smoke
	Given an autologed user in plan page with country and segment	pt	1
	The Currency of the plan page should be on	${EUR}
	then I click on select button of monthly plan
    and checkout page should contain name of the user	qa
    and checkout page should contain email of the user	${PT}
    and checkout page should contain selected plan name	Monthly
    and The price of the selected plan should be the same of checkout page	${MYPLANPRICE1M}
    and checkout page should contain a currency on	${EUR}
    and I compare The renewal-date displayed with the number of days of the selected plan	30 days
    then Credit Card option via stripe should be present
    and BankTransfer option via SEPA should be present
    and Country should have PayPal method available
    Logout
   
With an user register in France -I want to go to confirm the payment methods available for the country
	[Documentation]	ZOR-171
	[Timeout]	40s
	[Tags]	smoke
	Given an autologed user in plan page with country and segment	fr	1
	The Currency of the plan page should be on	${EUR}
	then I click on select button of monthly plan
    and checkout page should contain name of the user	qa
    and checkout page should contain email of the user	${FR}
    and checkout page should contain selected plan name	Monthly
    and The price of the selected plan should be the same of checkout page	${MYPLANPRICE1M}
    and checkout page should contain a currency on	${EUR}
    and I compare The renewal-date displayed with the number of days of the selected plan	30 days
    then Credit Card option via stripe should be present
    and BankTransfer option via SEPA should be present
    and Country should have PayPal method available
    ##Carte bancaire
    Logout
	
With an user register in Mexico - in Six-monthly plan compare prices currency, summary details and payment methods available on checkout page
	[Documentation]	ZOR-94 ZOR-171 
	[Timeout]	40s
	[Tags]	smoke
	Given an autologed user in plan page with country and segment	mx	1
	The Currency of the plan page should be on	${MXN}
	then I click on select button of Six-monthly plan
    and checkout page should contain name of the user	qastudent
    and checkout page should contain email of the user	${MX}
    and checkout page should contain selected plan name	Six-monthly
    and The price of the selected plan should be the same of checkout page	${MYPLANPRICE6M}
    and checkout page should contain a currency on	${MXN}
    and I compare The renewal-date displayed with the number of days of the selected plan	185 days
	then Credit Card option via stripe should be present
    and Country should have PayPal method available
    #Country should have an OXXO method to play
    Logout

like a loged user from Colombia - I want to confirm the available payment methods that users should have
	[Documentation]	ZOR-94 ZOR-171
	[Timeout]	40s 
	[Tags]	smoke
	Given an autologed user in plan page with country and segment	co	1
	The Currency of the plan page should be on	${USD}
	then I click on select button of annual plan
    and checkout page should contain name of the user	qastudent
    and checkout page should contain email of the user	${CO}
    and checkout page should contain selected plan name	Annual
    and The price of the selected plan should be the same of checkout page	${MYPLANPRICE12M}
    and checkout page should contain a currency on	${USD}
    and I compare The renewal-date displayed with the number of days of the selected plan	365 days
	then Credit Card option via stripe should be present
	and Country should have PayPal method available
    Logout

With user register in italy -I want to go to checkout page compare user details, prices and currency of the selected plan, finally confirm the payment methods available for the country
	[Documentation]	ZOR-94 ZOR-171
	[Timeout]	40s
	[Tags]	smoke
	Given an autologed user in plan page with country and segment	it	1
	The Currency of the plan page should be on	${EUR}
	then I click on select button of Two years plan
    and checkout page should contain name of the user	qastudent
    and checkout page should contain email of the user	${IT}
    and The price of the selected plan should be the same of checkout page	${MYPLANPRICE24M}
    and checkout page should contain a currency on	${EUR}
    and I compare The renewal-date displayed with the number of days of the selected plan	730 days
    then Credit Card option via stripe should be present
    and BankTransfer option via SEPA should be present
    and Country should have PayPal method available
    ##Carte bancaire
    Logout

Germany I access to plan page on segment 2 - 20 percent of discount
	[Documentation]	ZOR-94 ZOR-171
	[Timeout]	40s
	[Tags]	medium
	Given an autologed user in plan page with country and segment	de	2
	plan page should have a rate plan with discount of	20
	then I click on select button of Six-monthly plan
	and checkout page should contain name of the user	qastudent
	and checkout page should contain email of the user	${DE}
	and The price of the selected plan should be the same of checkout page	${MYPLANPRICE6M}
	then Credit Card option via stripe should be present
    and BankTransfer option via SEPA should be present
    and Country should have PayPal method available
    #Country should have Sofort method
    #Country should have eDirectBanking method
    #Country should have ELV method
	logout
	

like a loged user from Brasil - I want to go to the plans page select Two years plan and compare prices currency and summary details on checkout page
	[Documentation]	ZOR-94 ZOR-171
	[Timeout]	40s
	[Tags]	smoke
	Given an autologed user in plan page with country and segment	br	1
	The Currency of the plan page should be on	${BRL}
	then I click on select button of Two years plan
    and checkout page should contain name of the user	qastudent
    and checkout page should contain email of the user	${BR}
    and checkout page should contain selected plan name	Two years
    and The price of the selected plan should be the same of checkout page	${MYPLANPRICE24M}
    and checkout page should contain a currency on	${BRL}
    and I compare The renewal-date displayed with the number of days of the selected plan	730 days
    #Country should have a Credit Card AllPago method
    #Country should have PayPal method available
    #Country should have Boleto method
    Logout
    
Japon I access to plan page on segment 3 - 40 percent of discount
	[Documentation]	ZOR-94 ZOR-171
	[Timeout]	40s
	[Tags]	main
	Given an autologed user in plan page with country and segment	jp	3
	then I click on select button of annual plan
	and The price of the selected plan should be the same of checkout page	${MYPLANPRICE12M}	
	Credit Card option via stripe should be present
    and Country should have PayPal method available
	logout


like a loged user from Taiwan I want to confirm the available payment methods that users should have
	[Documentation]	I want to see my plans with the prices con Taiwan
	[Timeout]	40s
	[Tags]	main
	Given a loged user registered on Taiwan
	then like loged user I want to see the available plans to get aba premium
	and The Currency of the plan page should be on	${USD}
	then I click on select button of annual plan
	Credit Card option via stripe should be present
    and Country should have PayPal method available
	Logout
	
like a loged user from turkey I want to confirm the available payment methods that users should have
	[Documentation]	I want to see my plans with the prices con Turkey
	[Timeout]	40s
	[Tags]	medium
	Given an autologed user in plan page with country and segment	tr	3
	and The Currency of the plan page should be on	${USD}
	then I click on select button of annual plan
	then Credit Card option via stripe should be present
    Country should not have PayPal method available
	Logout
	

User should be redirected to login page if he/she tries to open the checkout page and is not logging in the Web.
	[Documentation]	ZOR-171
	[Timeout]	40s
	[Tags]	medium
	Given I login to the app with	${ES}	${PASS}
	like loged user I want to see the available plans to get aba premium
	then I click on select button of Six-monthly plan
	${GET-URL}=	Get Page Url
	 Logout
	 sleep	1s
	 Open Page	${GET-URL}
	 Url Should Contain	/login

Plan page prices should display always the same prices when user goes back to plan page coming from checkout page.
	[Documentation]	ZOR-171 italy 
	[Timeout]	40s
	[Tags]	medium
	Given an autologed user in plan page with country and segment	it	1
	then I click on select button of Six-monthly plan
	The price of the selected plan should be the same of checkout page	${MYPLANPRICE6M}
	Go Back
	The total price by six monthly should be	${PRICETOPAY}
	then I click on select button of annual plan
	The price of the selected plan should be the same of checkout page	${MYPLANPRICE12M}
	Go Back
	The total price by year should be	${PRICETOPAY}
	then I click on select button of annual plan
	The price of the selected plan should be the same of checkout page	${MYPLANPRICE12M}
	logout

