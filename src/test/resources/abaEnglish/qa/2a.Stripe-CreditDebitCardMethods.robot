*** Settings ***
Resource	config.robot

Metadata    Version        Zuora - Stripe Ayden methods
Metadata	Feature	       ZOR-165
Metadata    Executed At    ${HOME_PAGE} environment

#Suite Set Up	openRemoteBrowser	${BROWSER}	${SITE URL}	${REMOTE SERVER}
Suite Set Up	openBrowser	${BROWSER}	${SITE URL}
#Test Tear Down	close browser
Suite Tear Down	close browser
Test Set Up	clear cookies
Test Tear Down	Run Keywords
...				Run Keyword If Test Failed	get screen shot	AND
...				Run Keyword If Test Failed	Logout	
Default Tags	java_aps

#Run Keyword If Test Failed	Logout
*** Test Cases **

#### STRIPE

QA ENV - I want all my QA users like free type so I Update the user type in the database
	[Timeout]	40s
	[Tags]	QAenv
	Run And Return Rc	mysql -h "dbqa.cfbep2aydaua.eu-west-1.rds.amazonaws.com" -u abaadmin --password="XZ!FXYT^wDZLCX" "aba_b2c" -e "UPDATE aba_b2c.user SET userType = '1' WHERE email LIKE 'qastudent+__@abaenglish.com'"
	Run And Return Rc	mysql -h "dbqa.cfbep2aydaua.eu-west-1.rds.amazonaws.com" -u abaadmin --password="X

Check if the card options allows card number, expiry date, cvv value text fields.
	[Documentation]	ZOR-165 stripe 
	[Timeout]	40s
	[Tags]	medium
	Given a logged user in payment-funnel	es	1
	then I click on select button of monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	Cardholder Name should be available
	Credit card number should be available
	expiration date should be available
	CVV number should be available
	Logout
	
Terms and conditions and Privacy Policy links should be available after selection credit card opcion
	[Documentation]	ZOR-171 Terms and conditions and Privacy Policy
	[Timeout]	40s
	[Tags]	medium
	Given a logged user in payment-funnel	fr	1
	then I click on select button of monthly plan
	I click on Credit card option
	sleep	2s
	Terms and conditions link should be shown 
	Privacy Policy link should be shown 
	Logout
	
VISA Like new student I want to be premium user by purchasing with credit card. 
	[Documentation]	ZOR-165 Like new student I want to be premium user by purchasing with credit card.
	[Timeout]	60s
	[Tags]	smoke   QAenv
	Given a new user logued in the campus
	then I open plan page in segment id	11
	then I click on select button of monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name	${NEWUSEREMAIL}
	I enter my Credit card number	4242424242424242
	Visa card should be enable
	Master card should be disabled
	american express should be disabled
	I enter the expiration date	11	2019
	I enter the CVV number	111
	I click on confirm Button
	sleep	4s
	and confirmation payment page should be displayed
	confirmation payment page should have an option to go to the campus online
	total price in confirmation payment page should be present
	confirmation payment page should contain a currency on  ${EUR}
	#I want to be sure that the price of my subscription is the same thay I select on plan page	${MYPLANPRICE1M} ${EUR}		
	User should have the option to download the aba english app by iTunes link
    User should have the option to download the aba english app by google play link
	#I go to the aba english campus
	#users with premium suscription should not have the option to go to plans page
	Logout

VISA Like a student from portugal I want to be premium user by purchasing with credit card
	[Documentation]	ZOR-165 stripe- Credit card visa enter valid info card type should be enabled
	[Timeout]	40s
	[Tags]	smoke   QAenv
	Given an available user logged in plan page ready to be premium	pt	1
	then I click on select button of monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name	Nick Murray
	I enter my Credit card number	4242424242424242
	Visa card should be enable
	Master card should be disabled
	american express should be disabled
	I enter the expiration date	11	2019
	I enter the CVV number	111
	I click on confirm Button
	sleep	4s
	and confirmation payment page should be displayed
	Logout

VISA Like a student from germany I want to be premium user by purchasing with credit card
	[Documentation]	Credit Card - Visa debito enter valid info 
	[Timeout]	40s
	[Tags]	medium  QAenv
	Given an available user logged in plan page ready to be premium	de	2
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name	Bren Braitano
	I enter my Credit card number	4000056655665556		
	I enter the expiration date	11	2020
	I enter the CVV number	111
	I click on confirm Button
	sleep	4s
	and confirmation payment page should be displayed
	Logout
	
	
MasterCard Like a student from Albany I want to be premium user by purchasing with credit card
	[Documentation]	Credit Card - Master card debit enter valid info
	[Timeout]	40s
	[Tags]	medium  QAenv
	Given an available user logged in plan page ready to be premium	al	3
	then I click on select button of annual plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name	Juroden Su
	I enter my Credit card number	5200828282828210
	Master card should be enable
	Visa card should be disabled
	american express should be disabled 	
	I enter the expiration date	11	2020
	I enter the CVV number	111
	I click on confirm Button
	sleep	4s
	and confirmation payment page should be displayed
	Logout

Master card Prepaid enter valid info - Andorra
	[Documentation]	Credit Card - Master card Prepaid enter valid info  
	[Timeout]	40s
	[Tags]	medium    QAenv
	Given an available user logged in plan page ready to be premium	ad	4
	then I click on select button of Two years plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name	Wolam Moonwood	
    I enter my Credit card number	5105105105105100
	Master card should be enable		
	I enter the expiration date	11	2020
	I enter the CVV number	111
	I click on confirm Button
	and confirmation payment page should be displayed
	Logout



#Visa Valid card from Brazil
#	[Documentation]	Visa Valid card from Brazil  
#	[Timeout]	40s
#	[Tags]	Main	Payment-methods
#	Given a logged user in payment-funnel	br	1
#	then I click on select button of Six-monthly plan
#	sleep	2s
#	I want to pay with credit card - Stripe
#	I enter the Cardholder Name 	${BR}
#	I enter my Credit card number	4000000760000002
#	Visa card should be enable		
#	I enter the expiration date	11	2020
#	I enter the CVV number	111
#	I click on submitButton
#	Old confirmation page should be open
#	In confirmation page I Get the amount price of my subscription 
#	I want to be sure that the price of my subscription is the same thay I select on plan page	${MYPLANPRICE6M}	
#	I go to the aba english campus
#	users with premium suscription should not have the option to go to plans page

Visa Valid card form Canada
	[Documentation]	Visa Valid card form Canada
	[Timeout]	40s
	[Tags]	Main    QAenv
	Given a logged user in payment-funnel	ca	1
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name 	Wolam Moonwood
	I enter my Credit card number	4000001240000000
	Visa card should be enable
	I enter the expiration date	11	2020
	I enter the CVV number	111
	I click on confirm Button
	confirmation payment page should be displayed
	I go to the aba english campus
	users with premium suscription should not have the option to go to plans page

Visa Valid card form Mexico
	[Documentation]	Visa Valid card form Mexico 
	[Timeout]	40s
	[Tags]	smoke   QAenv
	Given an available user logged in plan page ready to be premium	mx	1
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name	Zaq Rekkon	
	I enter my Credit card number	4000004840000008
	Visa card should be enable		
	I enter the expiration date	11	2020
	I enter the CVV number	111
	I click on confirm Button
	sleep	4s
	and confirmation payment page should be displayed
	Logout

American Express enter valid info Belgium
	[Documentation]	Credit Card - American Express enter valid info card type should be enabled 
	[Timeout]	40s
	[Tags]	medium    QAenv
	Given an available user logged in plan page ready to be premium	be	1
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name 	Shandi Jago
	I enter my Credit card number	378282246310005
	american express should be enable	
	I enter the expiration date	11	2019
	I enter the CVV number	111
	I click on confirm Button
	sleep	4s
	and confirmation payment page should be displayed
	Logout

Master card Error should be displayed after entering an invalid card number  
	[Documentation]	Credit Card - Master card enter invalid card number 
	[Timeout]	40s
	[Tags]	medium
	Given a logged user in payment-funnel	de	1
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name	AUDREY TIMMONS
	I enter my Credit card number	5555 5555 5555 4466
	I enter the expiration date	11	2016
	I enter the CVV number	698
	I click on submitButton
	an error about the card number is invalid should be displayed
	Logout
	
	
Europe error message should be displayed After entering a valid Diners Club card number
	[Documentation]	Credit Card - Error message in case of Empthy Cardholder Name 
	[Timeout]	40s
	[Tags]	Main
	Given a logged user in payment-funnel	es	1
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name 	${ES}
	I enter my Credit card number	30569309025904		
	I enter the expiration date	11	2020
	I enter the CVV number	111
	I click on submitButton
	an error about the card type is invalid should be displayed
	Logout
	#error Your card is not supported. Please use a Visa, MasterCard, or American Express card



Credit Card - Error message in case of Empty Cardholder Name 
	[Documentation]	Credit Card - Error message in case of Empthy Cardholder Name 
	[Timeout]	40s
	[Tags]	Main
	Given a logged user in payment-funnel	es	1
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name 	${EMPTY}
	I enter my Credit card number	4111 1111 1111 1111
	I enter the expiration date	11	2019
	I enter the CVV number	555
	I click on submitButton
	an error about the CardHolder name is required should be displayed
	Logout

Credit Card - Charge will be declined with an incorrect cvc code. 
	[Documentation]	Charge will be declined with an incorrect cvc code 
	[Timeout]	40s
	[Tags]	medium
	Given a logged user in payment-funnel	es	1
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name 	${ES}
	I enter my Credit card number	4000000000000127
	I enter the expiration date	11	2019
	I enter the CVV number	555
	I click on submitButton
	an error by default about transaction denied should be displayed
	Logout

Credit Card - Charge will be declined becouse the card has expired
	[Documentation]	Charge will be declined becouse Your card has expired
	[Timeout]	40s
	[Tags]	medium
	Given a logged user in payment-funnel	es	1
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name 	${ES}
	I enter my Credit card number	4000 0000 0000 0069
	I enter the expiration date	11	2019
	I enter the CVV number	555
	I click on submitButton
	an error by default about transaction denied should be displayed
	Logout

Credit Card - Charge will be declined becouse of processing error
	[Documentation]	Charge will be declined becouse Your card has expired
	[Timeout]	40s
	[Tags]	Main
	Given a logged user in payment-funnel	es	1
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name 	${ES}
	I enter my Credit card number	4000 0000 0000 0119
	I enter the expiration date	11	2019
	I enter the CVV number	555
	I click on submitButton
	an error by default about transaction denied should be displayed
	Logout

Credit Card - Charge will be declined becouse of fraudulent reason
	[Documentation]	Charge will be declined becouse Your card has expired
	[Timeout]	40s
	[Tags]	Main
	Given a logged user in payment-funnel	es	1
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name 	${ES}
	I enter my Credit card number	4100 0000 0000 0019
	I enter the expiration date	11	2019
	I enter the CVV number	555
	I click on submitButton
	an error by default about transaction denied should be displayed
	Logout


