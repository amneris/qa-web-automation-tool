*** Settings ***
Resource	config.robot

Metadata    Version        Zuora - Bank Transfer SEPA methods
Metadata	Feature	       ZOR-165
Metadata    Executed At    ${HOME_PAGE} environment

Suite Set Up	openRemoteBrowser	${BROWSER}	${SITE URL}	${REMOTE SERVER}
#Suite Set Up	openBrowser	${BROWSER}	${SITE URL}
#Test Tear Down	close browser
Suite Tear Down	close browser
Test Set Up	clear cookies
Test Tear Down	Run Keywords
...				Run Keyword If Test Failed	get screen shot	AND
...				Run Keyword If Test Failed	Logout	
Default Tags	java_aps

#Run Keyword If Test Failed	Logout
*** Test Cases **

#### STRIPE

Check if the bank transfer options allows Acoount name,Acoount number and country
	[Documentation]	ZOR-165 stripe 
	[Timeout]	40s
	[Tags]	medium
	Given an user logged in payment-funnel from country and segment	es	1
	then I click on select button of monthly plan
	sleep	2s
	I want to pay by BankTransfer
	Bank Acoount name should be available
	Account Numbert name should be available
	Country field should be available	
	Logout

Terms and conditions and Privacy Policy links should be available after select bank transfer opcion
	[Documentation]	ZOR-171 Terms and conditions and Privacy Policy
	[Timeout]	40s
	[Tags]	main
	Given an user logged in payment-funnel from country and segment	es	1
	then I click on select button of monthly plan
	I click on BankTransfer option
	sleep	2s
	Terms and conditions link should be shown 
	Privacy Policy link should be shown 
	Logout

Bank transfer - valid account number from Germany
	[Documentation]	Bank transfer - valid account number from Germany
	[Timeout]	40s
	[Tags]	smoke
	Given an user logged in payment-funnel from country and segment	de	1
	then I click on select button of monthly plan
	sleep	2s
	I want to pay by BankTransfer
	I enter the Account Name	Schneider	
	I enter Account Number	DE60444488880023456789
	I select a country	Germany
	#I enter de email	${DE}
	I click on confirm Button
    and confirmation payment page should be displayed
	#In confirmation page I Get the amount price of my subscription 
	#I want to be sure that the price of my subscription is the same thay I select on plan page	${MYPLANPRICE1M} ${EUR}		
	I go to the aba english campus
	users with premium suscription should not have the option to go to plans page
	Logout
	
	
Bank transfer - valid account number from Italy
	[Documentation]	Bank transfer - valid account number from iTaly
	[Timeout]	40s
	[Tags]	medium
	Given an user logged in payment-funnel from country and segment	it	2
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay by BankTransfer
	sleep	3s
	I enter the Account Name	A. Pacini	
	I enter Account Number	IT60X0542811101000000123456
	I select a country	Italy
	#I enter de email	${DE}
	I click on confirm Button
    and confirmation payment page should be displayed
	#In confirmation page I Get the amount price of my subscription 
	#I go to the aba english campus
	users with premium suscription should not have the option to go to plans page
	Logout

Bank transfer - valid account number from France
	[Documentation]	Bank transfer - valid account number from France
	[Timeout]	40s
	[Tags]	medium
	Given an user logged in payment-funnel from country and segment	fr	3
	then I click on select button of annual plan
	sleep	2s
	I want to pay by BankTransfer
	I enter the Account Name	A. Grand		
	I enter Account Number	FR1420041010050500013M02606
	I select a country	France
	I click on confirm Button
    and confirmation payment page should be displayed
	#In confirmation page I Get the amount price of my subscription 
	#I want to be sure that the price of my subscription is the same thay I select on plan page	${MYPLANPRICE1M} ${EUR}		
	I go to the aba english campus
	users with premium suscription should not have the option to go to plans page
	Logout 
	
Bank transfer - Positive New user registered on spain pay with bank transfer
	[Documentation]	Positive New user registered on spain pay with bank transfer
	[Timeout]	40s
	[Tags]	main
	Given a new user logued in the campus
	then I open plan page in segment id	11
	then I click on select button of Two years plan
	sleep	2s
	I want to pay by BankTransfer
	I enter the Account Name	benjamin Button	
	I enter Account Number	ES9121000418450200051332
	I select a country	Spain
	I click on confirm Button
    and confirmation payment page should be displayed
	#In confirmation page I Get the amount price of my subscription 
	I go to the aba english campus
	users with premium suscription should not have the option to go to plans page
	Logout
	
Bank transfer - invalid account number 
	[Documentation]	Bank transfer - invalid account number from Germany
	[Timeout]	40s
	[Tags]	main
	Given an user logged in payment-funnel from country and segment	at	1
	then I click on select button of monthly plan
	sleep	2s
	I want to pay by BankTransfer
	I enter the Account Name	Schneider	
	I enter Account Number	DE604444888800234567AA
	I select a country	Germany
	I click on submitButton
	an error by default about transaction denied should be displayed
	
Bank transfer - error message should be displayed in case of name field is empty
	[Documentation]	error message should be displayed in case of name field is empty
	[Timeout]	40s
	[Tags]	medium
	Given an user logged in payment-funnel from country and segment	at	4
	then I click on select button of Two years plan
	sleep	2s
	I want to pay by BankTransfer
	I enter the Account Name	${EMPTY}	
	I enter Account Number	ES9121000418450200051332
	I select a country	Spain
	I click on submitButton
	an error about the Account holders name is required should be displayed
	Logout

Bank transfer - error message should be displayed in case of account number is empty
	[Documentation]	error message should be displayed in case of account number is empty
	[Timeout]	40s
	[Tags]	medium
	Given an user logged in payment-funnel from country and segment	at	4
	then I click on select button of Two years plan
	sleep	2s
	I want to pay by BankTransfer
	I enter the Account Name	Lyon King	
	I enter Account Number	${EMPTY}
	I select a country	Spain
	I click on submitButton
	an error about the Account number is required should be displayed
	Logout

#Bank transfer - error message should be displayed in case of country field is empty
#	[Documentation]	 error message should be displayed in case of country field is empty
#	[Timeout]	40s
#	[Tags]	Main	Payment-methods
#	Given an user logged in payment-funnel from country and segment	at	4
#	then I click on select button of Two years plan
#	sleep	2s
#	I want to pay by BankTransfer
#	I enter the Account Name	Lyon King	
#	I enter Account Number	ES9121000418450200051332
#	I select a country	${EMPTY}
#	I click on submitButton
#	error message should be displayed in case of country field is empty
#	Logout