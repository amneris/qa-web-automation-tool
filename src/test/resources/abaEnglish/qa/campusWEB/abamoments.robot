*** Settings ***
Documentation  	This is the test plan for the aba Moment functionality.

Resource	../config.robot

Suite Set Up	openRemoteBrowser	${BROWSER}	${SITE URL}	${REMOTE SERVER}
#Suite Set Up	open Browser	chrome	${SITE URL}
Suite Tear Down	close browser
Test Set Up	clear cookies
Test Tear Down	Run Keywords
...				Run Keyword If Test Failed	get screen shot	AND
...				Run Keyword If Test Failed	Go Back
...				Run Keyword If Test Failed	Logout

*** Test Cases ***
Free users should not be able to see the ABA Moments
#    [Tags]  abamoment   smoke   incompleted
    Given I login to the app with   ${FREE-USER-EMAIL}  ${FREE-USER-PASSWORD}
    And I add cookie to hide hide starting pop ups and member get member
    Then I should not see the ABA Moments option in the menu
    And Logout

New users should not be able to see the ABA Moments
#    [Tags]  abamoment   smoke   incompleted     pendingImplementation
    Given a new user logued in the campus   Moments
    And I add cookie to hide hide starting pop ups and member get member
    Then I should not see the ABA Moments option in the menu
    And Logout

As I spanish user, I should see the aba activities in Spanish
#    [Tags]  abaMoment   main
    [Documentation]     The code is on the branch.
    Given I login to the app with    ${MX}   ${PASS}
    When I go to the ABA Moment section
    Then I should see in the aba moments description        Eres capaz de aprender 10 palabras
    And I should see the colors activity with the name      Colores
    And I should see the animals activity with the name     Animales
    And Logout

I should be redirected to the current unit after complete the activity and I click in the pop-up
    [Tags]  abamoment   smoke
    Given I login to the app with   ${PREMIUM-USER-LEVEL-2}     ${PREMIUM-PASS-LEVEL-2}
    When I go to the ABA Moment section
    And I start the colors activity
    And I complete this amount of aba activities    10
    And I accept the ABA Moment confirmation pop-up
    Then I should see in the course lesson title    COURSE UNIT
    And I should see in the unit title    2. A New Friend
    And Logout

I should be redirected to the current unit after complete the activity and I do not click in the pop-up
    [Tags]  abamoment   medium
    Given I login to the app with   ${PREMIUM-USER-LEVEL-2}     ${PREMIUM-PASS-LEVEL-2}
    When I go to the ABA Moment section
    And I start the colors activity
    And I complete this amount of aba activities    10
    And I wait until ABA Moment confirmation pop-up does not exist
    Then I should see in the course lesson title    COURSE UNIT
    And I should see in the unit title    2. A New Friend
    And Logout

I can stay in the ABA Moment section after an activity is completed clisong the pop-up
    [Tags]  abamoment   main
    Given I login to the app with   ${premiumUser}     ${PASS}
    When I go to the ABA Moment section
    And I start the colors activity
    And I complete this amount of aba activities    10
    And I close the ABA Moment confirmation pop-up
    Then I should be in the ABA Moment menu
    And Logout

I should lost the progress when the ABA Moment activity is not completed
    [Tags]  abamoment   smoke
    Given I login to the app with   ${premiumUser}     ${PASS}
    When I go to the ABA Moment section
    And I start the colors activity
    And I complete this amount of aba activities    2
    And I close the activity
    And I start the colors activity
    Then I should be in the question    0
    And Go Back
    And Logout

I can do two activities of ABA Moment one after another
    [Tags]  abamoment   main
    Given I login to the app with   ${premiumUser}     ${PASS}
    When I go to the ABA Moment section
    And I start the colors activity
    And I complete this amount of aba activities    10
    And I close the ABA Moment confirmation pop-up
    And I start the animals activity
    And I complete this amount of aba activities    10
    And I close the ABA Moment confirmation pop-up
    Then I should be in the ABA Moment menu
    And Logout

I should not change the answer if I select an wrong answer
    [Tags]  abamoment   medium
    Given I login to the app with   ${premiumUser}     ${PASS}
    When I go to the ABA Moment section
    And I start the colors activity
    And I select a wrong answer
    And I should be in the question    0
    And I complete this amount of aba activities    1
    Then I should be in the question    1
    And Go Back
    And Logout

I should be redirected to the abamoment menu when I close an activity
    [Tags]  abamoment   main
    Given I login to the app with   ${premiumUser}     ${PASS}
    When I go to the ABA Moment section
    And I start the colors activity
    And I close the activity
    Then I should be in the ABA Moment menu
    And Logout

I should be redirected to the abamoment menu when I go back from an activity
    [Tags]  abamoment   main
    Given I login to the app with   ${premiumUser}     ${PASS}
    When I go to the ABA Moment section
    And I start the colors activity
    And Go Back
    Then I should be in the ABA Moment menu
    And Logout