*** Settings ***
Resource	config.robot

Metadata    Version        Abba WebAapps
Metadata	Feature	      cleaning Database in QA environment
Metadata    Executed At    ${HOME_PAGE} environment

Suite Set Up	openRemoteBrowser	${BROWSER}	${SITE URL}	${REMOTE SERVER}
#Suite Set Up	openBrowser	${BROWSER}	${SITE URL}
#Test Tear Down	close browser
Suite Tear Down	close browser
Test Set Up	clear cookies
Test Tear Down	Run Keywords
...				Run Keyword If Test Failed	get screen shot	AND
...				Run Keyword If Test Failed	Logout	
Default Tags	db


*** Test Cases ***

Cleaning our databace in QA environment
   [Tags]	db
   #Delete users table in DBQA
   #Delete users_zuora table in DBQA
   QA ENV - I want all my QA users like free type so I Update the user type in the database

#delete and create dbqa users
#    [Tags]	takecare
#   Delete users table in DBQA
#   create qaUsers in data base to execute our test

*** Keywords ***

QA ENV - I want all my QA users like free type so I Update the user type in the database
        Run And Return Rc	mysql -h "dbqa-migrated.cfbep2aydaua.eu-west-1.rds.amazonaws.com" -u abaadmin --password="XZ!FXYT^wDZLCX" "aba_b2c" -e "UPDATE aba_b2c.user SET userType = '1' WHERE email LIKE 'qastudent+%@abaenglish.com'"
	Run And Return Rc	mysql -h "dbqa-migrated.cfbep2aydaua.eu-west-1.rds.amazonaws.com" -u abaadmin --password="XZ!FXYT^wDZLCX" "aba_b2c" -e "UPDATE aba_b2c.user SET userType = '1' WHERE email LIKE 'abaenglishqa+%@gmail.com'"

#dbqa-migrated.cfbep2aydaua.eu-west-1.rds.amazonaws.com
##dbqa-migrated.cfbep2aydaua.eu-west-1.rds.amazonaws.com
##dbqa.cfbep2aydaua.eu-west-1.rds.amazonaws.com


a new user from database with idCountry number
    [Arguments]	${IDcountry}
    I want a random number
    Run And Return Rc    mysql -h "dbqa-migrated.cfbep2aydaua.eu-west-1.rds.amazonaws.com" -u abaadmin --password="XZ!FXYT^wDZLCX" "aba_b2c" -e "INSERT aba_b2c.user (name,surnames, email, password,city,countryId,userType,keyExternalLogin) VALUES ('qastudent','fromDatabace','qastudent+${random}@abaenglish.com','e7ee2a8281efc413f5789e4409e2bc8c','NULL',${IDcountry},1,'${random}1e9c1566723ca5dd878eb15f769139b62');"
    sleep   3s
    open page	 ${HOME_PAGE}/en/plans?autol=${random}1e9c1566723ca5dd878eb15f769139b62


Delete users_zuora table in DBQA
    Run And Return Rc    mysql -h "dbqa-migrated.cfbep2aydaua.eu-west-1.rds.amazonaws.com" -u abaadmin --password="XZ!FXYT^wDZLCX" "aba_b2c" -e "DELETE FROM aba_b2c.user_zuora WHERE userId in ( SELECT id FROM aba_b2c.user WHERE email LIKE 'abaenglishqa+%@gmail.com' OR email LIKE 'qastudent+%@abaenglish.com')"
    #mysql -h "dbqa-migrated.cfbep2aydaua.eu-west-1.rds.amazonaws.com" -u abaadmin --password="XZ!FXYT^wDZLCX" "aba_b2c" -e "DELETE FROM aba_b2c.user_zuora"



Delete users table in DBQA
    Run And Return Rc	mysql -h "dbqa-migrated.cfbep2aydaua.eu-west-1.rds.amazonaws.com" -u abaadmin --password="XZ!FXYT^wDZLCX" "aba_b2c" -e "DELETE FROM aba_b2c.user where userType <> 4"
    #DELETE FROM aba_b2c.user where userType = 1


create qaUsers in data base to execute our test
    ${dd}   Run And Return Rc	mysql -h --verbose "dbqa.cfbep2aydaua.eu-west-1.rds.amazonaws.com" -u abaadmin --password="XZ!FXYT^wDZLCX" "aba_b2c" < ../dataset_test_users2.sql