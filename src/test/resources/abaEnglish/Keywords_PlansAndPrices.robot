*** Variables ***

*** Keywords ***  
### full escenary

Login in different countries and check currency and ensure that the prices are the same
    [Arguments] 	${COUNTRIETOGO}     ${currencyff}
    Given a logged user in payment-funnel	${COUNTRIETOGO}	1
    The Currency of the plan page should be on	${currencyff}
    then I click on select button of annual plan
    and The price of the selected plan should be the same of checkout page	${MYPLANPRICE12M}
    and checkout page should contain a currency on  ${currencyff}
    Logout

Login in different countries and check if the user is able to pay
    [Arguments] 	${COUNTRIETOGO}
    Given a new user logued in the campus   TestPaymentISO3 ${COUNTRIETOGO}
    then the created user should have idcountry num  ${COUNTRIETOGO}  ${NEWUSEREMAIL}
    then I go to the plan page with segment number   2
    then I click on select button of Six-monthly plan
    And I pay with credit card option
    Then confirmation payment page should be displayed
    Logout

I pay with credit card option
	I want to pay with credit card - Stripe
	I enter the Cardholder Name	Bren Braitano
	I enter my Credit card number	4000056655665556
	I enter the expiration date	02	2018
	I enter the CVV number	111
	I click on confirm Button
	the payment transaction should start

I pay by bank transfer
	I want to pay by BankTransfer
    I enter the Account Name	benjamin Button
	I enter Account Number	ES9121000418450200051332
	I select a country	Spain
	I click on confirm Button
	the payment transaction should start


As a user logued in a country I want to verify if the Monthly price of the plan is updated
    [Arguments] 	${COUNTRIE}	${priceff}	${currencyff}
   	Given a logged user in payment-funnel	${COUNTRIE}	1
    The total price of a month should be	${priceff}
    The Currency of the plan page should be on	${currencyff}
    then I go to the plan page with 20 percent of discount
    The total price of a month should be	${priceff}
	Logout

I go to the plan page with segment number
	[Arguments]	${SEGMENTNUMBER}
	open page	${PLANS_PAGE_BY_DEFAULT}/plans?segm_id=${SEGMENTNUMBER}

I go to plan page in other language
    [Arguments]    ${lang}    ${SEGM}
    open page    ${PLANS_PAGE_BY_DEFAULT}/${lang}/plans?segm_id=${SEGM}


#plans cases

page should have the option in the subscription plan to pay monthly
	element should be visible	xpath=//*[@class='plan-select']/../..//*[contains(text(),'Monthly')]
	
page should have the option in the subscription plan to pay Six-monthly
	element should be visible	xpath=//*[@class='plan-select']/../..//*[contains(text(),'Six-monthly')]

page should have the option in the subscription plan to pay Annual
	element should be visible	xpath=//*[@class='plan-select']/../..//*[contains(text(),'Annual')]

page should have the option in the subscription plan to pay for two years
	element should be visible	xpath=//*[@class='plan-select']/../..//*[contains(text(),'Two years')]	

page should have the option to pay for a family plan
	element should be visible	xpath=//*[@class='plan-select']/../..//*[contains(text(),'family plan')]	


page should NOT have the option in the subscription plan to pay monthly
	Element Should Not Be Visible	xpath=//*[@class='plan-select']/../..//*[contains(text(),'Monthly')]
	
page should NOT have the option in the subscription plan to pay Six-monthly
	Element Should Not Be Visible	xpath=//*[@class='plan-select']/../..//*[contains(text(),'Six-monthly')]

page should NOT have the option in the subscription plan to pay Annual
	Element Should Not Be Visible	xpath=//*[@class='plan-select']/../..//*[contains(text(),'Annual')]

page should NOT have the option in the subscription plan to pay for two years
	Element Should Not Be Visible	xpath=//*[@class='plan-select']/../..//*[contains(text(),'Two years')]	

page should NOT have the option to pay for a family plan
	Element Should Not Be Visible	xpath=//*[@class='plan-select']/../..//*[contains(text(),'family plan')]	

error in plan page by default should be displayed
	element should be visible	class=error-container

error message by default should not be shown
   Element Should Not Be Visible    class=error-container

botton try again should be shown
	element should be visible	class=error-button
	
free users should have the option to go to plans page
	element should be visible	id=idDivGoPremium

users with premium suscription should not have the option to go to plans page
	Element Should Not Be Visible	id=idDivGoPremium

user should not be able to see the plan page
    page should NOT have the option in the subscription plan to pay Annual
    page should NOT have the option in the subscription plan to pay Six-monthly
    page should NOT have the option in the subscription plan to pay monthly

as a free user logued in the campus I click on upgrade aba premium
	Like a loged user I want to go to home page
	click	id=idDivGoPremium

Plan page should have 14 days of guarantee
	element should be visible	xpath=//*[@id='idLi12_months']//*[contains(text(),'14-Day Guarantee')]
	element should be visible	xpath=//*[@id='content']//*[contains(text(),'14-Day Guarantee')]
	Element Should Not Be Visible	xpath=//*[@id='content']//*[contains(text(),'30-Day Guarantee')]

congratulations message about especial offert should be shown
    Element Should Contain	xpath=//*[contains(@class, 'label-welcome-offer')]	We have a special offer for you




### LANG TO IMPROVE

I change language of the plan page
    [Arguments]	${COUNTLANG}
    open page	${PLANS_PAGE_BY_DEFAULT}/${COUNTLANG}/plans
    element should be visible   class=plans-list-container

Plan Page should be displayed in german language
	element should be visible	xpath=//*[@class='plan-select']/../..//*[contains(text(),'Monatlich')]

Plan Page should be displayed in italian language
	element should be visible	xpath=//*[@class='plan-select']/../..//*[contains(text(),'Mensile')]

Plan Page should be displayed in french language
	element should be visible	xpath=//*[@class='plan-select']/../..//*[contains(text(),'Mensuel')]

Plan Page should be displayed in spanish language
	element should be visible	xpath=//*[@class='plan-select']/../..//*[contains(text(),'Mensual')]


checkout page should be displayed in spanish language
    element should be visible   xpath=//*[@class = 'summary-label'][contains(text(),'Nombre')]
    element should be visible   xpath=//*[@class = 'section-title'][contains(text(),'Resumen de tu compra')]
    element should be visible   xpath=//*[@class = 'section-title'][contains(text(),'Selecciona método de pago')]

credict card method should be displayed in spanish language
    element should be visible	xpath=//*[@class='payment-method-toggler-container']/..//*[contains(text(),'Tarjeta')]

Bank Transfer method should be displayed in spanish lanuage
     element should be visible	xpath=//*[@class='payment-method-toggler-container']/..//*[contains(text(),'Transferencia bancaria')]

checkout page should be displayed in french language
    element should be visible   xpath=//*[@class = 'summary-label'][contains(text(),'Nom')]
    element should be visible   xpath=//*[@class = 'section-title'][contains(text(),'Résumé de votre achat)]
    element should be visible   xpath=//*[@class = 'section-title'][contains(text(),'Sélectionnez une méthode de paiement')]

credict card method should be displayed in french language
    element should be visible	xpath=//*[@class='payment-method-toggler-container']/..//*[contains(text(),'Débit ou crédit carte')]

Bank Transfer method should be displayed in french lanuage
     element should be visible	xpath=//*[@class='payment-method-toggler-container']/..//*[contains(text(),'Virement bancaire')]



checkout page should be displayed in italian language
    element should be visible   xpath=//*[@class = 'summary-label'][contains(text(),'Nome')]
    element should be visible   xpath=//*[@class = 'section-title'][contains(text(),'Riassunto del tuo acquisto')]
    element should be visible   xpath=//*[@class = 'section-title'][contains(text(),'Seleziona il metodo di pagamento')]

credict card method should be displayed in italian language
    element should be visible	xpath=//*[@class='payment-method-toggler-container']/..//*[contains(text(),'Carta di credito')]

Bank Transfer method should be displayed in italian lanuage
     element should be visible	xpath=//*[@class='payment-method-toggler-container']/..//*[contains(text(),'Bonifico bancario')]


checkout page should be displayed in german language
    element should be visible   xpath=//*[@class = 'summary-label'][contains(text(),'Nombre')]
    element should be visible   xpath=//*[@class = 'section-title'][contains(text(),'Zusammenfassung deines Kaufs')]
    element should be visible   xpath=//*[@class = 'section-title'][contains(text(),'Wähle eine Zahlungsmöglichkeit')]

credict card method should be displayed in german language
    element should be visible	xpath=//*[@class='payment-method-toggler-container']/..//*[contains(text(),'Debit- oder Kreditkarte')]

Bank Transfer method should be displayed in german lanuage
     element should be visible	xpath=//*[@class='payment-method-toggler-container']/..//*[contains(text(),'Banküberweisung')]


Confirmation page should be displayed in spanish language
    element should be visible   xpath=//*[@class = 'user-status-text'][contains(text(),'Ya estás suscrito')]
    element should be visible   xpath=//*[@class = 'section-title'][contains(text(),'Resumen de tu compra')]


######

like loged user I want to see the available plans to get aba premium
	sleep	1s
	open page   ${PLANS_PAGE}
	#urlShouldContain	${PLANS_PAGE}
	element should be visible	class=plan-list-element

User access to a url with some promotion
	[Arguments]	${SEGMENT_URL}
	open page	${SEGMENT_URL}
	#urlShouldContain	${PLANS_PAGE}
	#element should be vissible	class=loading-container
	element should be visible	class=plan-list-element
	
I go to the plan plage by default
	open page	${PLANS_PAGE_BY_DEFAULT}

I go to th plan page in segment 1 without discounts
    open page	${PLANS_PAGE}
    Plan page should not have promotions


I open plan page in segment id
	[Arguments]	${SEGMENT_ID}
	open page	${PLANS_PAGE_BY_DEFAULT}/en/plans?segm_id=${SEGMENT_ID}

I go to the plan page with 20 percent of discount
	open page	${PLANS_PAGE_SEGMENT2}
	plan page should have a rate plan with discount of	20

I go to the plan page with 40 percent of discount
	open page	${PLANS_PAGE_SEGMENT3}
	plan page should have a rate plan with discount of	40

I go to the plan page with 60 percent of discount
	open page	${PLANS_PAGE_SEGMENT4}
	plan page should have a rate plan with discount of	60


The price by month should be
	[Arguments]	${MonthPrice}
	Element Should Contain	xpath=//*[contains(@data-price-monthly, '${MonthPrice}')]	${MonthPrice}

The total price of a month should be
	[Arguments]	${finalPriceM}
	element should be visible	xpath=//*[@data-plan-id='plansList0:plan0']//*[contains(text(),'${finalPriceM}')]
	
The total price by six monthly should be
	[Arguments]	${finalPrice6M}
	element should be visible	xpath=//*[@id='idLi6_months']//*[contains(@data-price-total, '${finalPrice6M}')]
	
The total price by year should be
	[Arguments]	${finalPriceY}
	element should be visible	xpath=//*[@id='idLi12_months']//*[contains(@data-price-total, '${finalPriceY}')]
	
The total price for two years should be
	[Arguments]	${finalPrice2Y}
	element should be visible	xpath=//*[@id='idLi24_months']//*[contains(@data-price-total, '${finalPrice2Y}')]

The Currency of the plan page should be on
	[Arguments]	${ABAcurrency}
	element should be visible	xpath=//*[@class='plan-price-current']/../..//*[contains(text(),'${ABAcurrency}')]

The Price of the available range of subscription plans should be
	[Arguments]	${ratePlan}	${ABAprice}
	element should be visible	xpath=//*[@id='${ratePlan}']/../..//*[contains(text(),'${ABAprice}')]	


The tolal price on the plan page should be
	[Arguments]	${finalPrice}
	Element Should Contain	xpath=//*[@class='plan-select']/../..//*[contains(text(),'${finalPrice}')]	${finalPrice}

### feature	product

User should see the monthly plan like the most popular
	Element Should Contain	xpath=//*[@data-plan-id='plansList0:plan0']//*[contains(text(),'The most popular')]	The most popular

User should see the six-monthly plan like the most popular
	Element Should Contain	xpath=//*[@id='idLi6_months']//*[contains(text(),'The most popular')]	The most popular	

User should see the annual plan like the most popular
	Element Should Contain	xpath=//*[@id='idLi12_months']//*[contains(text(),'The most popular')]	The most popular

User should see the twoYears plan like the most popular
	Element Should Contain	xpath=//*[@id='idLi24_months']//*[contains(text(),'The most popular')]	The most popular


monthly plan should not be feature product
	Element Should Not Be Present	xpath=//*[@data-plan-id='plansList0:plan0']//*[contains(text(),'The most popular')]

six-monthly plan should not be feature product
	Element Should Not Be Present	xpath=//*[@id='idLi6_months']//*[contains(text(),'The most popular')]	

annual plan should not be feature product
	Element Should Not Be Present	xpath=//*[@id='idLi12_months']//*[contains(text(),'The most popular')]
twoYears plan should not be feature product
	Element Should Not Be Present	xpath=//*[@id='idLi24_months']//*[contains(text(),'The most popular')]


The Plan page should contain a rate plan with name
	[Arguments]	${ratePlanName}	
	element should be visible	xpath=//*[@class='aba-plans-list-container']//*[contains(text(),'${ratePlanName}')]

#### PROMOCIONES 

User should see the monthly plan with promotion 
	element should be visible	xpath=//*[@data-plan-id='plansList0:plan0']//*[@class='plan-promo-container']

User should see the six-monthly plan with promotion 
	element should be visible	xpath=//*[@id='idLi6_months']//*[@class='plan-promo-container']

User should see the annual plan with promotion 
	element should be visible	xpath=//*[@id='idLi12_months']//*[@class='plan-promo-container']
	
User should see the twoYears plan with promotion 
	element should be visible	xpath=//*[@id='idLi24_months']//*[@class='plan-promo-container']

///

the discount of the six-monthly plan should be
	[Arguments]	${percentage}
	Element Should Contain	xpath=//*[@id='idLi6_months']//*[@class='plan-promo-container']	${percentage}


the discount of the annual plan should be
	[Arguments]	${percentage}
	Element Should Contain	xpath=//*[@id='idLi12_months']//*[@class='plan-promo-container']	${percentage}

the discount of two-years plan should be
	[Arguments]	${percentage}
	Element Should Contain	xpath=//*[@id='idLi24_months']//*[@class='plan-promo-container']	${percentage}
	
Plan page should not have promotions
	Element Should Not Be Visible	class=plan-promo-container

Plan page should have promotions
	element should be visible	class=plan-promo-container
	
plan page should have a rate plan with discount of
	[Arguments]	${percentage}
	Element Should Contain	xpath=//*[@class='plan-promo-container']	${percentage}

#### GET PRICES 
 
I get the final price of one month plan
	${PLANPRICE1M}=	getAtributeValue	xpath=//*[@id='idLi1_month']/div/div[2]/div/dl	data-price-monthly
	 Set Suite Variable	${MYPLANPRICE1M}	${PLANPRICE1M}
	
	
I get the final price of Six-monthly plan
	${PLANPRICE6M}=	getAtributeValue	xpath=//*[@id='idLi6_months']//*[contains(@class, 'plan-price-total')]	data-price-total
	Set Suite Variable	${MYPLANPRICE6M}	${PLANPRICE6M}
I get the final price of annual plan
	${PLANPRICE12M}=	getAtributeValue	xpath=//*[@id='idLi12_months']//*[contains(@class, 'plan-price-total')]	data-price-total	
	Set Suite Variable	${MYPLANPRICE12M}	${PLANPRICE12M}
I get the final price of two-years plan
	${PLANPRICE24M}=	getAtributeValue	xpath=//*[@id='idLi24_months']//*[contains(@class, 'plan-price-total')]	data-price-total	
	Set Suite Variable	${MYPLANPRICE24M}	${PLANPRICE24M}

I get the price by month of the Six-monthly plan
	${PLANMONPRICE6M}=	getAtributeValue	xpath=//*[@id='idLi6_months']/div/div[2]/div/dl	data-price-monthly
	 Set Suite Variable	${MONTHPPRICE6M}	${PLANMONPRICE6M}
	
I get the price by month of the annual plan
	${PLANMONPRICE12M}=	getAtributeValue	xpath=//*[@id='idLi12_months']/div/div[2]/div/dl	data-price-monthly
	 Set Suite Variable	${MONTHPPRICE12M}	${PLANMONPRICE12M}

I get the price by month of the two-years plan
	${PLANMONPRICE24M}=	getAtributeValue	xpath=//*[@id='idLi24_months']/div/div[2]/div/dl	data-price-monthly
	 Set Suite Variable	${MONTHPPRICE24M}	${PLANMONPRICE24M}
	
##############COMPARE PRICES 


	
#six months price should be
#	[Arguments]	${finalPrice}
#	${data-price-monthly}=	Evaluate	${finalPrice}/6
#	 ${result}=	Convert To Number	${data-price-monthly}	-4
#	 ##Set Suite Variable	${result}	${data-price-monthly}
#	 log to console  ${result}
#	Should Be Equal	${result}	99.99





user should have the price by default
plan-promo-text

Total charge for plan

Promotion applied to plan

###############GO TO CHECKOUT PAGE

I click on select button of monthly plan
	I get the final price of one month plan
	click	xpath=//*[@id='idLi1_month']//*[@class= 'plan-select-button']
	element should be visible	class=payment-summary-and-methods-container
	
I click on select button of Six-monthly plan 
	I get the final price of Six-monthly plan
	click	xpath=//*[@id='idLi6_months']//*[@class= 'plan-select-button']
	element should be visible	class=payment-summary-and-methods-container
I click on select button of annual plan
	I get the final price of annual plan
	click	xpath=//*[@id='idLi12_months']//*[@class= 'plan-select-button']
	element should be visible	class=payment-summary-and-methods-container
I click on select button of two years plan 
	I get the final price of two-years plan
	click	xpath=//*[@id='idLi24_months']//*[@class= 'plan-select-button']
	element should be visible	class=payment-summary-and-methods-container

I go to checkout page of the rate plan 
	[Arguments]	${PlanToSelect}
	click	xpath=//*[contains(text(),'${PlanToSelect}')]/..//*[contains(@class, 'plan-select-button')]
	element should be visible	class=payment-summary-and-methods-container

######################### CHECKOUT PAGE ###############


checkout page should contain payment methods
		element should be visible	class=payment-method-container
		element should be visible	class=money-back-and-support-info-container
 
 
checkout page should contain a summary with user details
	element should be visible	class=payment-summary-and-methods-container

checkout page should contain name of the user
	[Arguments]	${USERNAME}
	element should be visible	xpath=//*[@class = 'summary-info'][contains(@data-user-name, '${USERNAME}')]
	

checkout page should contain email of the user
	[Arguments]	${EMAILUSER}
	element should be visible	xpath=//*[@class = 'summary-info'][contains(@data-user-email, '${EMAILUSER}')]

checkout page should contain selected plan name 
	[Arguments]	${PLANPAGENAME}
	element should be visible	xpath=//*[@class = 'summary-label']/..//*[contains(text(),'${PLANPAGENAME}')]

checkout page should contain a Price

checkout page should contain a currency on
	[Arguments]	${ABACURRENCY}
	element should be visible	xpath=//*[@class = 'summary-info-big']/../..//*[contains(text(),'${ABACURRENCY}')]

checkout page should have a renewal date
	element should be visible	xpath=//*[@class = 'summary-label']/..//*[contains(text(),'To be renewed on')]
	#### pend get atrribute


I get the price in checkout page of the selected plan  
	${DATAPRICETOTAL}=	getAtributeValue	xpath=//*[@class = 'summary-info-big'][@data-price-total]	data-price-total
	Set Suite Variable	${PRICETOPAY}	${DATAPRICETOTAL}
	
The price of the selected plan should be the same of checkout page
	[Arguments]	${PLANPRICESELECTED}
	I get the price in checkout page of the selected plan  
	Should Be Equal	${PRICETOPAY}	 ${PLANPRICESELECTED} 	
	
	
	
I get the renewal date
	${RENEWAL}=	getAtributeValue	xpath=//*[@class = 'summary-label']/..//*[@data-renewal-date]	data-renewal-date
	Set Suite Variable	${RENEWALDATE}	${RENEWAL}


I compare The renewal-date displayed with the number of days of the selected plan
	[Arguments]	${numday}
	I get the renewal date
	I want the time
	${MYRENEWALDATE} =    Add Time To Date    ${yy1}-${mm1}-${dd1}    ${numday} 	result_format=%m-%Y
	Should Contain	${RENEWALDATE}	 ${MYRENEWALDATE}
	#%d-     ### tenemos que igualar fecha de renovacion los dias no me cuadran solo comparo mes y año
	#Should Be Equal	${RENEWALDATE}	 ${MYRENEWALDATE}
	#element should be visible	xpath=//*[@class = 'summary-label']/..//*[contains(text(),'${MYRENEWALDATE}')]

#terms and conditions and Privacy policy

Terms and conditions link should be shown 
	element should be visible	id=popover-terms

Privacy Policy link should be shown 
	element should be visible	id=popover-privacy

I click on Terms and conditions link
	Mouse Over	xpath=//*[@id='popover-terms']
	click	xpath=//*[@id='popover-terms']
	Element Should Contain	xpath=//*[@id='popover-container']	General Terms
I close de popover
	click	id=popover-close

I click on Privacy Policy link
	click	id=popover-privacy
	Element Should Contain	class=popover-content-body	How do we use the information you provide


checkout page should have a message about five days to cancel the subscription
    Element Should Contain  xpath=//*[@class='cancellation-info-container']    You can cancel your subscription whenever you want up to five days before your current

#horario de atencion

I want to confirm the text support-opening-hours-info
    Element Should Contain  xpath=//*[contains(@class, 'support-opening-hours-info')]    rom Monday to Thursday from 09:00 to 19:00 (GMT+1) Friday from 09:00 to 13:00 (GMT+1)


	
########################### payment methods ##########################

Country should have a Credit Card method
	element should be visible	xpath=//*[@class='payment-method-toggler-container']/..//*[contains(text(),'CreditCard')] 	

Country should have a Bank Transfer method
	element should be visible	xpath=//*[@class='payment-method-toggler-container']/..//*[contains(text(),'BankTransfer')]


Country should have PayPal method available
	element should be visible	xpath=//*[@class='payment-method-toggler-container']/..//*[contains(text(),'PayPal')]


Country should have a Credit Card AllPago method
	#pending

Country should have a SEPA method to play
	#pending

Country should have an OXXO method to play
	#pending


Country should have Boleto method
	#pending


Country should have Sofort method
	#pending

Country should have eDirectBanking method
	#pending

Country should have Giropay method
	#pending
Country should have ELV method
	#pending

Credit Card option via stripe should be present
	element should be visible	xpath=//*[@class='payment-method-title']/..//*[contains(@data-reactid, '${StripeID}')]	
	##xpath=//*[@class='payment-method-toggler-container']//*[contains(@data-reactid, '2c92c0f855c9f4620155de7e2bbc75ad')]

Credit Card option via Adyen should be present
	element should be visible	xpath=//*[@class='payment-method-title']/..//*[contains(@data-reactid, '${AdyenID}')]

BankTransfer option via SEPA should be present
	element should be visible	xpath=//*[@class='payment-method-title']/..//*[contains(@data-reactid, '${SEPA}')]

###################################### ZUORA Stripe/Adyen credit/Debit card  #####################################

I click on Credit card option
	click	xpath=//*[@class='payment-method-title']/..//*[contains(@data-reactid, '${StripeID}')]
	##//*[@class='payment-method-toggler-container']/..//*[contains(text(),'Credit Card')]

I want to pay with credit card - Stripe
	click	xpath=//*[@class='payment-method-title']/..//*[contains(@data-reactid, '${StripeID}')]
	sleep   2s
	Select Frame	id=z_hppm_iframe

I want to pay with credit card option -AdyenBackup
	#Adyen Hosted Page should be present on checkout page
	MouseOver	xpath=//*[@class='payment-method-title']/..//*[contains(@data-reactid, '${AdyenID}')]
	click	xpath=//*[@class='payment-method-title']/..//*[contains(@data-reactid, '${AdyenID}')]
	#//*[@class='payment-method-title']/..//*[contains(@data-reactid, '2c92c0f955ca02920155de1e5f142cf2')]
	Select Frame	id=z_hppm_iframe
	 
I click on BankTransfer option
	click	xpath=//*[@class='payment-method-title']/..//*[contains(text(),'Bank Transfer')] 

	
I want to pay with credit card
	click	xpath=//*[@class='payment-method-title']/..//*[contains(text(),'Credit Card')]
	##//*[@class='payment-method-toggler-container']/..//*[contains(text(),'Credit Card')] 
	Select Frame	id=z_hppm_iframe

I want to pay by BankTransfer
	click	xpath=//*[@class='payment-method-title']/..//*[contains(@data-reactid, '${SEPA}')]
	sleep	2s
	Select Frame	id=z_hppm_iframe

I select PayPal option to purchase
    click   xpath=//*[@class='payment-method-toggler-container']/..//*[contains(text(),'PayPal')]



Cardholder Name should be available
	element should be visible	id=input-creditCardHolderName
Credit card number should be available
	element should be visible	id=input-creditCardNumber	 
expiration date should be available
	element should be visible	xpath=//*[@id='input-creditCardExpirationMonth']
	element should be visible	xpath=//*[@id='input-creditCardExpirationYear']
CVV number should be available
	element should be visible	xpath=//*[@id='input-cardSecurityCode']	


I enter the Cardholder Name
	[Arguments]	${CARDHOLDER}
	type	id=input-creditCardHolderName	${CARDHOLDER}
		
I enter my Credit card number
	[Arguments]	${CCNUMBER}
	type	id=input-creditCardNumber	${CCNUMBER}
	
I enter the expiration date
	[Arguments]	${EXP-MONTH}	${EXP-YEAR}
	click	xpath=//*[@id='input-creditCardExpirationMonth']
	Select From List By Label	xpath=//*[@id='input-creditCardExpirationMonth']	${EXP-MONTH}
	click	xpath=//*[@id='input-creditCardExpirationYear']
	Select From List By Label	xpath=//*[@id='input-creditCardExpirationYear']	${EXP-YEAR}
I enter the CVV number
	[Arguments]	${CVV-CC}
	type	xpath=//*[@id='input-cardSecurityCode']	${CVV-CC}

I click on submitButton
	click	id=submitButton

I click on confirm Button
	click	id=submitButton
	Select Main Frame
	
### ERROR MESSAGE 
an error about the CardHolder name is required should be displayed
	Element Should Contain	id=error-creditCardHolderName	Required Field Not Completed

an error meessage about the security code is incorrect should be displayed
	Element Should Contain	id=error	Transaction declined.incorrect_cvc - Your card's security code is incorrect.

an error meessage about the expiration date is invalid
	Element Should Contain	id=error	Transaction declined.expired_card - Your card has expired


an error by default about transaction denied should be displayed
	Element Should Contain	id=error	Transaction denied. Please verify your card details

an error about the card number is invalid should be displayed
	Element Should Contain	id=error-creditCardNumber	Invalid Card Number

an error about the card type is invalid should be displayed
	Element Should Contain	id=error-creditCardType	Invalid Card Type


#### 
Visa card should be enable
	${svtrue}=	Get Atribute Value	id=card-image-container-Visa	aria-selected
	Should Be Equal	${svtrue}	true
Master card should be enable
	${smtrue}=	Get Atribute Value	id=card-image-container-MasterCard	aria-selected
	Should Be Equal	${smtrue}	true
american express should be enable
	${saetrue}=	Get Atribute Value	id=card-image-container-AmericanExpress	aria-selected
	Should Be Equal	${saetrue}	true

Visa card should be disabled
	${svtrue}=	Get Atribute Value	id=card-image-container-Visa	aria-selected
	Should Be Equal	${svtrue}	false
Master card should be disabled
	${smtrue}=	Get Atribute Value	id=card-image-container-MasterCard	aria-selected
	Should Be Equal	${smtrue}	false
american express should be disabled
	${saetrue}=	Get Atribute Value	id=card-image-container-AmericanExpress	aria-selected
	Should Be Equal	${saetrue}	false



############


##### BANK TRANSFER CASES
I enter the Account Name 
	[Arguments]	${ACCOUNTNAME}
	type	id=input-bankAccountName	${ACCOUNTNAME}

I enter Account Number 
	[Arguments]	${ACCOUNTNUMBER}
	type	id=input-bankAccountNumber	${ACCOUNTNUMBER}

I select a country
	[Arguments]	${COUNTRY}
	#click	id=input-country
	Select From List By Label	id=input-country	${COUNTRY}

I enter de email
	[Arguments]	${EMAIL}
	type	id=input-email	${EMAIL}

Bank Acoount name should be available
	element should be visible	id=input-bankAccountName

Account Numbert name should be available
	element should be visible	id=input-bankAccountNumber

Country field should be available		
	element should be visible	id=input-country



an error about the Account holders name is required should be displayed
	Element Should Contain	id=error-bankAccountName	Required field

an error about the Account number is required should be displayed
	Element Should Contain	id=error-bankAccountNumber	Required field

error message should be displayed in case of country field is empty
	Element Should Contain	id=error-country	Required field

########################### paypal

I click on paypal payment method
	click	xpath=//*[@class='payment-method-toggler-container']/..//*[contains(text(),'PayPal')]
	
Country should not have PayPal method available
	Element Should Not Be Visible	xpath=//*[@class='payment-method-toggler-container']/..//*[contains(text(),'PayPal')]


PayPal button shoud be available after User selects Paypal to purchase
    element should be visible   class=paypal-container-inner

I click on Paypal Button
    PayPal button shoud be available after User selects Paypal to purchase
    click   xpath=//*[@class='paypal-container-inner']/..//*[contains(@class, 'paypal-button')]

A message about user will be redirected to paypal website should be shown
     element should be visible  xpath=//*[@class='paypal-redirect-container']
     #/..//*[contains(text(),'You are being redirected to PayPal')]
    #element should be visible   xpath=//*[contains(@class, 'paypal-redirect-container')]
    #Element Should Contain  class=paypal-redirect-text      You are being redirected to PayPal

user should be redirected to paypal web site
    Url Should Contain  paypal.com

mensaje que aparece cuando estas esperando a que confirme paypal
         Url Should Contain class=success-text  Your payment is being processed by PayPal. You will receive a payment confirmation email shortly.


after selecting plan abaEnglsih logo should be present on paypal url
  element should be visible    xpath=//*[@class='headerWrapper']/..//*[contains(@src, 'static.abaenglish.com/images/')]

the students email should be present on paypal web site
    [Arguments]   ${paypalUser}
   #Element Should Contain  id=email   ${paypalUser}
   element should be visible    xpath=//input[@id='email']/..//*[contains(@value, '${paypalUser}')]



#########  new confirmation page

the payment transaction should start
    #element should be visible   xpath=//*[@id='sticky-tricky']/div/div/div/div/span
    #//*[@class='processing-payment-container']
    #class=processing-payment-text
    #processing-payment-container
    ##//*[@id='sticky-tricky']/div/div/div/div/span
   ##processing-payment-text    We are processing your payment
   #class=aba-loading
    sleep   2s

confirmation payment page should be displayed
    element should be visible  class=confirmation-status-container
    element should be visible  class=payment-summary-and-info-container

confirmation payment page should have an option to go to the campus online
    element should be visible   class=goToCampus-button

confirmation payment page should contain the students name
	[Arguments]	${STUDENTNAME}
	element should be visible	xpath=//*[@class='summary-label']/..//*[contains(@data-user-name, '${STUDENTNAME}')]

confirmation payment page should contain the students email
	[Arguments]	${STUDENT_EMAIL}
	element should be visible	xpath=//*[@class='summary-label']/..//*[contains(@data-user-email, '${STUDENT_EMAIL}')]

total price in confirmation payment page should be present
	${GETPURCHASED_PRICE}=	getAtributeValue	xpath=//*[@class = 'summary-info-big'][@data-price-total]	data-price-total
	Set Suite Variable	${PURCHASED_PRICE}	${GETPURCHASED_PRICE}


confirmation payment page should contain a currency on
	[Arguments]	${ABACURRENCY}
	element should be visible	xpath=//*[@class='summary-info-big']/../..//*[contains(text(),'${ABACURRENCY}')]

confirmation payment page should have a renewal date
	${GETRENEWAL}=	getAtributeValue	xpath=//*[@class = 'summary-label']/..//*[@data-renewal-date]	data-renewal-date
    Set Suite Variable	${CONF_PAGE_RENEWALDATE}	${GETRENEWAL}
	element should be visible	xpath=//*[@class = 'summary-label']/..//*[contains(text(),'To be renewed on')]


Confirmation page should have the same renewal date of checkout page
    confirmation payment page should have a renewal date
    Should Be Equal	${RENEWALDATE}	 ${CONF_PAGE_RENEWALDATE}
    ## en checkotu page se debe hacer un get de la fecha de renovacion

the price of the selected plan should be the same of confirmation page
	[Arguments]	${PLANPRICESELECTED2}
	I get the price in checkout page of the selected plan
	Should Be Equal	${PURCHASEDPRICE}	 ${PLANPRICESELECTED2}

User should have the option to download the aba english app by iTunes link
    element should be visible   xpath=//*[@class = 'download-app-buttons-container']/../..//*[contains(text(),'iTunes')]

User should have the option to download the aba english app by google play link
    element should be visible   xpath=//*[@class = 'download-app-buttons-container']/../..//*[contains(text(),'Google Play')]

Help Center link available
     element should be visible  link=Help Center

confirmation page with title about personalizing your experience should be displayed
    Element Should Contain     xpath=//*[contains(@class, 'user-status-text')]  personalizing your experience

I get the subscription identifier
    ${GETIDPURCHASE}=	Get Element Text	class=subscription-id-value
    Set Suite Variable	${SubscriptionID}	${GETIDPURCHASE}

if Something went wrong executing the transaction user should have an error message by default
     element should be visible  class=error-text
     Element Should Contain  class=error-text    There has been an error. Please try again.

user should have the option to try again If the payment transaction get an error.
    element should be visible  class=error-button

I click on try again button
    click   class=error-button
    ##//div[@id='sticky-tricky']/div/div/button

confirmation page should have a message about five days to cancel the subscription
    Element Should Contain  xpath=//*[@class='subscription-info-container']    You can cancel your subscription whenever you want up to five days before your current subscription is due to expire.