*** Variables ***

*** Keywords ***  
##### LOGIN

I go to intranet
	open page	${INTRANET}
	urlShouldContain	${INTRANET}

I want to login in intranet
	[Arguments]	${user}	${passw}
	#I clear local storage
	I go to intranet
    type	id=LoginForm_username	${user}
    type	id=LoginForm_password	${passw}
    click	id=btnLogin
    element should be visible 	id=cssmenu

in Intranet I go to users section
   open page    ${INTRANET}/index.php?r=abaUser/admin
    element should be visible 	id=aba-user-grid_cid

User should have user type
    [Arguments]	${usertype}
    element should be visible 	xpath=//*[@id='aba-user-grid']/table/tbody/tr//*[contains(text(),'${usertype}')]


in intranet user section I enter the userid
    [Arguments]	${Intranetuserid}
    type    xpath=//div[@id='aba-user-grid']/table/thead/tr[2]/td/input    ${Intranetuserid}
    click   xpath=//*[@id='aba-user-grid']/table/thead/tr[2]/td[2]/input
    element should be visible 	xpath=//*[@id='aba-user-grid']/..//*[contains(text(),'${Intranetuserid}')]

I go to user section filtered by account user
    [Arguments]	${userid}
     open page    ${INTRANET}/index.php?r=abaUser/admin_id&id=${userid}
     element should be visible 	xpath=//*[@id='aba-user-grid']/..//*[contains(text(),'${userid}')]

I go to payments section filtered by account user
    [Arguments]	${userid-Pay}
    open page    ${INTRANET}/index.php?r=abaPayments/admin_id&id=${userid-Pay}
    element should be visible 	xpath=//*[@id='aba-payments-grid']/..//*[contains(text(),'${userid-Pay}')]


in payments section I filter by status
    [Arguments]	${status}
    click    xpath=(//select[@name='AbaPayments[status]'])[2]
    Select From List By Label 	xpath=(//select[@name='AbaPayments[status]'])//*[contains(text(),'${status}')]  ${status}
    sleep    8s
    #element should be visible 	xpath=//*[@id='aba-payments-grid']/..//*[contains(text(),'SUCCESS')]
    #Element Should Not Be Visible   xpath=//*[@id='aba-payments-grid']/..//*[contains(text(),'PENDING')]

