
*** Variables ***

${ZUSER}	qa@abaenglish.com
${ZUPWUSER}	Q1w2e3r44!
${TESTproductCatalogName}	QA-TESTPR5
${TESTdiscountCatalogName}	QA-TESTD5
${Zuora_URL}	https://apisandbox.zuora.com/apps/newlogin.do

*** Keywords ***  

as a Head of Monetization I marc a Rate plan like a Featured product
	[Arguments]		${cat}	${RatePlanTE}
	given I login in zuora
	#Go to the prodict catalog QA-TESTPR5
	then I go to the product catalog with name	${cat}
	then I click on Edit rate plan	${RatePlanTE}
	and I edit the product like featured rate plan
	I click on save
	In zuora I click on logout

In Zuora I enter the original prices for monthly plan
	#given I login in zuora
	#Go to the prodict catalog QA-TESTPR5
	#then I go to the product catalog with name	${TESTproductCatalogName}
	I want to edit the price of the rate plan in Europe	1M	${1_month_EU}
	I click on edit list price in other countries
	sleep	1s
	I edit price of USD Currency	${1_month_USD}
	I edit price of MXN Currency	${1_month_MXN}
	I edit price of BRL Currency	${1_month_BRL}
	I close price in other currencies popup
	I click on save
	#In zuora I click on logout
		

In Zuora I enter the original prices for annual plan
	#given I login in zuora
	#Go to the prodict catalog QA-TESTPR5
	#then I go to the product catalog with name	${TESTproductCatalogName}
	I want to edit the price of the rate plan in Europe	12M	${12_months_EU}
	I click on edit list price in other countries
	I edit price of USD Currency	${12_months_USD}
	I edit price of MXN Currency	${12_months_MXN}
	I edit price of BRL Currency	${12_months_BRL}
	I close price in other currencies popup
	I click on save
	#In zuora I click on logout


In Zuora I enter the original prices for two Years plan
	#given I login in zuora
	#Go to the prodict catalog QA-TESTPR5
	#then I go to the product catalog with name	${TESTproductCatalogName}
	I want to edit the price of the rate plan in Europe	24M	${12_months_EU}
	I click on edit list price in other countries
	I edit price of USD Currency	${24_months_USD}
	I edit price of MXN Currency	${24_months_MXN}
	I edit price of BRL Currency	${24_months_BRL}
	I close price in other currencies popup
	I click on save 



##### LOGIN 
I login in zuora
	#[Arguments]	${emailZuora}	${passwordZuora}
	#I clear local storage
	open page	${Zuora_URL}	
    type	id=id_username	${ZUSER}
    type	id=id_password	${ZUPWUSER} 
    click	xpath=//*[@id='loginForm']/div/a
    element should be visible	class=z__page
    sleep	1s
	#Run Keyword And Ignore Error	Popup after update

Popup after update
	element should be visible	xpath=//*[@id='wm-shoutout-11167']
	click	xpath=//*[@id='wm-shoutout-11167']/div[3]/div[2]/span
  

    
I go to product catalog
	Open Page	https://apisandbox.zuora.com/apps/Product.do?menu=Z-Billing

I click on list view
	click	xpath=//dd[@id='tab1']/span/a

Go to the prodict catalog QA-TESTPR5
	open page	https://apisandbox.zuora.com/apps/Product.do?method=view&id=2c92c0f9555351c1015558ac83606b24

I go to product name
	[Arguments]	${productname}
	click	xpath=//*[@id='Productname_${productname}']
	element should be visible	xpath=//*[@id='product_name_panel']
    
I go to the product catalog with name
	[Arguments]	${productCatalogName}
	I go to product catalog
	#I click on list view
 	I go to product name	${productCatalogName}	

In a product catalog I wan to access to a currency section
	[Arguments]	${currencyName}
	#Mouse Over	xpath=//*[@id='currencylist']/..//*[contains(text(),'${currencyName}')]
	click	xpath=//*[@id='currencylist']/..//*[contains(text(),'${currencyName}')]
	
	
I want to edit the price of the rate plan in Europe
	[Arguments]	${ratePlan}	${Newprice}
 	Mouse Over	xpath=//*[contains(@id, 'ST_${ratePlan}')]/table/tbody/tr[3]/td[2]/div/table/tbody/tr/td[2]/div[1]/table/tbody/tr/td[1]/div[2]
 	click	xpath=//*[@id='edit_recurring_${ratePlan}_${ratePlan}']
 	element should be visible	xpath=//*[@id='RPComponentForm']
 	click	class=formtext
 	type	xpath=//*[@id='ListPrice']/div/span/span[1]/input	${Newprice}
 	Set Suite Variable	${NEWEUR}	${Newprice}



## prices by countrie

I click on edit list price in other countries
	click	xpath=//div[@id='ListPrice']/div/span/span[4]/img
	#Mouse Over	id=EditMultiCurrencyPriceDialog


I edit price of USD Currency
	[Arguments]	${NewUSD_Currency}
	type	css=#content > #tier_1 > input[name="_priceTables_[0].USD"]	${NewUSD_Currency}
	Set Suite Variable	${NEWUSD}	${NewUSD_Currency}
	
I edit price of MXN Currency
	[Arguments]	${NewMXN_Currency}
	type	css=#content > #tier_1 > input[name="_priceTables_[0].MXN"]	${NewMXN_Currency}
	Set Suite Variable	${NEWMXN}	${NewMXN_Currency}
 	
 	
I edit price of BRL Currency
	[Arguments]	${NewBRL_Currency}
	type	xpath=(//div[@id='tier_1']/input[3])[2]	${NewBRL_Currency}
	Set Suite Variable	${NEWBRL}	${NewBRL_Currency}
	
I close price in other currencies popup
	click	css=body > #EditMultiCurrencyPriceDialog > #close
	element should be visible	id=ListPrice
	
	 	
In zuora I click on logout
	#open page	https://apisandbox.zuora.com/apps/Product.do?menu=Z-Billing
	#click	xpath=//*[@id='z-topnav']/ul/li[1]/a/span
	open page	https://apisandbox.zuora.com/apps/logout.do?method=redirect

As a user logued in zuora I want to edit the price of a rate plan of a product
	[Arguments]	${productCatalogNameff}	${RatePlanToEdit}	${price}
	given I login in zuora
	then I go to the product catalog with name	${productCatalogNameff}
	and I want to edit the price of the rate plan	${RatePlanToEdit}	${price}
	In zuora I click on logout
	
##### 

 	
I click on save
	element should be visible	xpath=//*[@id='rate_plan_close_edit']/div[1]/a[1]/span
 	click	xpath=//*[@id='rate_plan_close_edit']/div[1]/a[1]/span
 	##//div[@id='rate_plan_close_edit']//*[contains(text(),'save')]
 	##xpath=//div[@id='rate_plan_close_edit']/div/a/span
 	#css=a.button-formAction > span
 	#xpath=//*[@id='rate_plan_close_edit']/div[1]/a[1]/span 
 	element should be visible	id=RatePlans
 	element should not be visible	xpath=//div[@id='rate_plan_close_edit']/div/a/span

###  	featured rate plan 
I click on Edit rate plan
	[Arguments]	${right-RatePlanName}
	mouse over	xpath=//*[@id='ST_${right-RatePlanName}']/table/tbody/tr[1]/td
	sleep	0.5s
	click	xpath=//*[contains(@id, 'edit_${right-RatePlanName}')]
	element should be visible	xpath=//*[@id='RatePlanForm']

I edit the product like featured rate plan
	Select From List By Label	xpath=//*[@id='rate_info_area']/table[5]/tbody/tr/td/ul/li/select	true
	#Selected Option Should Be	xpath=//*[@id='rate_info_area']/table[5]/tbody/tr/td/ul/li/select	true
		
	
I edit the product like non featured rate plan
	Select From List By Label	xpath=//*[@id='rate_info_area']/table[5]/tbody/tr/td/ul/li/select	false
	
 
#### discounts ####

I click on edit button on recurring discount period
	[Arguments]	${discountPlan}	${ratePlan}
	Mouse Over	xpath=//*[contains(@id, 'ST_Discount ${discountPlan} ${ratePlan}')]/table/tbody/tr[3]/td[2]/div/table/tbody/tr/td[2]/div[1]
 	click	xpath=//*[@id='edit_recurring_Discount ${discountPlan} ${ratePlan}_Discount ${discountPlan} ${ratePlan}']

I edit the percentage of the discount amount
	[Arguments]	${discountAmount}
	type	xpath=//*[@id='discountAmount_percentage']/span[1]/input		${discountAmount}
	Set Suite Variable	${NEWDISCOUNT}	${discountAmount}
	#click	id=discountLevel
	click	xpath=//*[@id='rate_plan_close_edit']/div[1]/a[1]/span
	
 
 #####   
	
I get the total price of the edited field
	#[Arguments]	${ratePlan}
	${PLANMON}=	getAtributeValue	xpath=//*[@id='ListPrice']/div/span/span[1]/input	value
	 Set Suite Variable	${total}	${PLANMON}

	
##### subscriptions

I go to Subscriptions list
    Open page   https://apisandbox.zuora.com/apps/Subscription.do?menu=Z-Billing

Subscription should be present on the list
   [Arguments]	${IDSUBSRIPTION}
    element should be visible  xpath=//*[@id='subscription_list']//*[contains(text(),'${IDSUBSRIPTION}')]

I access to the suscription
    [Arguments]	${SubID}
    click   xpath=//*[@id='subscription_list']//*[contains(text(),'${SubID}')]
    element should be visible   xpath=//*[@id='titleLabelID']/..//*[contains(text(),'${SubID}')]



####accesing to the subscription ID#########

subscription should be active
     [Arguments]	${SUBID}
      element should be visible   xpath=//*[@id='titleLabelID']/..//*[contains(text(),'${SubID}')]


the Customer Name should be
     [Arguments]	${userName}
       element should be visible    link=${userName}

the Current Term should be
    [Arguments]	${ratePlanSelected}
    element should be visible    xpath=//*[@id='b11']//*[contains(text(),'${ratePlanSelected}')]

the Change History should have the date of the purchace
    [Arguments]    ${dateOnChangeHistory}
    element should be visible   xpath=//*[@id='b11']//*[contains(text(),'${dateOnChangeHistory}')]



I go to a subcription
    [Arguments]    ${SubID}
   I go to Subscriptions list
    I access to the suscription     ${SubID}

                     ############ CANCEL SUBSCRIPTION ################


I click on cancel button
    click    xpath=//*[@id='z-action-nav']/..//*[contains(text(),'cancel')]
    element should be visible   id=contractEffectiveDate_id

### cancel popup #######

I Specify the Contract Effective Date
    [Arguments]    ${CEDATE}
   #click   xpath=//*[@id="contractEffectiveDate_id_img"]
   type    id=contractEffectiveDate_id    ${CEDATE}
   # click   xpath=//*[@class='daysrow']//*[contains(text(),'${CEDATE}')]


in cancelation effective date I select option to End of Current Term
    click    id=cancellationPolicy_id
    ${LABELOPTN}=    Get Element Text    xpath=//*[@id='cancellationPolicy_id']/..//*[contains(text(),'End of Current Term')]
    Set Suite Variable    ${LABELOPTION}    ${LABELOPTN}
    Select From List By Label  xpath=//*[@id='cancellationPolicy_id']   ${LABELOPTION}


in cancelation effective date I select an specific date
    Select From List By Label  xpath=//*[@id='cancellationPolicy_id']     Enter a Date


I enter a Cancellation day on field
     [Arguments]    ${BILINGDate}
      #click   id=requestDate_id_img
      type    id=requestDate_id    ${BILINGDate}

I save changes to cancel the subscription
    click   id=z-boxPopup-formAction-button-save


I cancel the subscription by Entering specific dates on fields
    [Arguments]    ${ContractEfectiveDate}    ${CancelationDate}
    I click on cancel button
    I Specify the Contract Effective Date  ${ContractEfectiveDate}
    in cancelation effective date I select an specific date
    I enter a Cancellation day on field   ${CancelationDate}
    I save changes to cancel the subscription


I cancel the subscription at the end of the current term
    [Arguments]    ${ContractEfectiveDate}
    I click on cancel button
    I Specify the Contract Effective Date  ${ContractEfectiveDate}
    in cancelation effective date I select option to End of Current Term
    I save changes to cancel the subscription

#### fin popup

The subscription information should be updated
    The subscription status should be   Cancelled
    The button should contain   delete

The subscription status should be
    [Arguments]    ${Status}
    Element Should Contain      id=status_val   ${Status}


The button should contain
    [Arguments]    ${information}
    Element Should Contain      id=z-action-nav   ${information}

I click on the Customer Name link
    click   xpath=//*[@id="b11"]/div[2]/div/div[2]/div/table/tbody/tr[1]/td/span/a
    element should be visible   id=basicInfo_view
    I get the ABA User IDof the custumer



# inside of acount details

I click on create new subscription button
    click   xpath=//*[@id='z-action-nav']/..//*[contains(text(),'create new subscription')]


in transactions I click on payments
    click     xpath=//div[@id='Payments_tabPanel']/h2/span

I click on the payment number
    click    xpath=//*[@id='id__payments_[0].paymentNumber']/a
    ### seria mejor hacerlo con el link del payment number


I get the ABA User IDof the custumer
    ${newAbaUserID}=    Get Element Text    xpath=//*[@id='basicInfo_table']/tbody/tr[10]/td/span
    Set Suite Variable    ${AbaUserID}    ${newAbaUserID}


###  refound payment



I click on the more details button
    click    xpath=//li[@id='payment_detail_more_li']/a/span

Click on Refund this payment
   click    xpath=//*[@id='payment_detail_more']/..//*[contains(text(),'Refund this payment')]

Click on yes button
    click  id=confirm_yes

I want to comment the refund
    type     id=comment     TEST REFOUND A SUBSCRIPTION

I enter de refund ammount
    [Arguments]     ${ferundAmmount}
    type    xpath=//*[contains(@id, '_amount')]/..//*[@class='z-text-input']    ${ferundAmmount}


I click on confirm refund
    click   xpath=//*[@id='b11']/div[2]/div/div/div/table/tbody/tr/td/div/a[1]/span
    click    id=confirm_yes


### CREADAS POR ALEJANDRO


I complete the create subscription information for one month
    I select months in both selectors in the creation subscription
    I fill the initial term field with  1
    I fill the reneval term field with  1
    I select the checkbox of invoice the subscription
    I fill the notes field
    I select date for the opportunity close date
    I select product TESTP5 in the selector
    I select rate plan 1M in the selector
    I click in the save and active button to activate the subscription
    I complete the subscription activation dates

I select months in both selectors in the creation subscription
    Select from list by label   id=initialTermPeriodType    Month(s)
    Select from list by label   id=renewalTermPeriodType    Month(s)

I fill the initial term field with
    [Arguments]     ${value}
    type    id=initialTerm_id   ${value}

I fill the reneval term field with
    [Arguments]     ${value}
    type    id=renewalTerm_id   ${value}

I select the checkbox of invoice the subscription
    Select Checkbox     id=invoiceSeparate_id

I fill the notes field
    Type    xpath=//*[@id="b11"]/div[2]/div/div[2]/div/table/tbody/tr[12]/td/textarea   Subscription created by QA Team.

I select date for the opportunity close date
    ${day} Get Time   return day
    ${month}   Get Time   return month
    ${year}        Get Time   return year

    ${date} =  Catenate   SEPARATOR=/   ${day}   ${month}       ${year}
    type    id=edit_Subscription__quoteIntegrationData_Opportunity Close Date    ${date}


I select product TESTP5 in the selector
    click   xpath=//*[@id="_arrow_product[0].id"]
    click   xpath=//*[@id="2c92c0f9555351c1015558ac83606b24"]

I select rate plan 1M in the selector
    click   xpath=//*[@id="_arrow_rateplan[0].id"]
    click   xpath=//*[@id="2c92c0f9555351c1015558ac83b36b31"]

I click in the save and active button to activate the subscription
    click   xpath=//*[@id="form1"]/div[2]/a[1]

I complete the subscription activation dates
    ${day} Get Time   return day
    ${month}   Get Time   return month
    ${year}        Get Time   return year
    ${lastMonth} =  Evaluate    ${month} - 1
    ${date} =  Catenate   SEPARATOR=/   ${day}   ${lastMonth}       ${year}
    log     ${date}
    type    xpath=//*[@id="CE_date"]    ${date}
    click   xpath=//*[@id="z-boxPopup-formAction-button-save"]

new subscription should be activated
    The subscription status should be   Active
    The button should contain   cancel

I complete the information to create the amendment
    I complete the amendment name
    I select renewal in the amendment type
    I click in the save button to save the amendment
    Sleep   3s

I go to the amendment area
    click   xpath=//*[@id="z-action-nav"]/li[4]/a

I complete the amendment name
    type    id=amendment_name   New amendment qa-aba auto

I select renewal in the amendment type
    Select from list by label   xpath=//*[@id="amendmentType"]    Renewal

I click in the save button to save the amendment
    Click   xpath=//*[@id="AmendmentForm"]/div[2]/a[1]

I activate the amendment
    I click in the active button
    I select today in the active amendmet calendar
    I save changes to active amendment
    I save the user-name in the amendment page

I click in the active button
    Click   xpath=//*[@id="amendmentview_return"]/dl/dt/a[4]

I select today in the active amendmet calendar
    ${day} Get Time   return day
    ${month}   Get Time   return month
    ${year}        Get Time   return year
    ${date} =  Catenate   SEPARATOR=/   ${day}   ${month}       ${year}
    type    xpath=//*[@id="requestDate"]    ${date}

I save changes to active amendment
    click   xpath=//html/body/div[8]/div[2]/div/div/dl/dd/table/tbody/tr[6]/td/div/a[1]

I save the user-name in the amendment page
    ${GET-USERName} =  Get Element Text    xpath=//*[@id="AmendmentForm"]/div/table/tbody/tr[2]/td[3]/a
    Set Global Variable     ${USER-NAME}     ${GET-USERName}


I go to the customer account
    ${user-name} =  set variable    qastudent
    Click   id=Customers_BillingAccount
    Click   id=expandedView
    Click   xpath=//*[@id="ST_View_${user-name}"]
    Sleep   2s

I save the account number
    ${GET-AccountNumber} = Get Element Text    xpath=//*[@id="accountNumber"]
    Set Global Variable     ${USER-ACCOUNT-NUMBER}     ${GET-AccountNumber}

I execute the create bin run for the current subscription
    I go to the new Bill runs area
    I go to the Single Customer Account
    I check the checkboxs 1-2-4
    I click in the create bill run button

I go to the new Bill runs area
    Click   xpath=//*[@id="dataBox"]/ul/li[3]/a
    Click   id=Billing_BillRuns
    Click   xpath=//*[@id="z-action-nav"]/li[1]/a

I go to the Single Customer Account
    Click   id=singleAccount
    Set Global Variable     ${USER-ACCOUNT-NUMBER}     A00002329
    I fill the Account Number field     ${USER-ACCOUNT-NUMBER}
    I click in the go button to search the account

I fill the Account Number field
    [Arguments]     ${value}
    type    id=cusSearch   ${value}

I click in the go button to search the account
    click    xpath=//*[@id="listConten2"]/a

I check the checkboxs 1-2-4
    Select Checkbox     id=renewalFlag
    Select Checkbox     id=ST_suppressionZeroTotalAmountInvoiceEmail
    Select CheckBox     id=ST_AutoEmail

I click in the create bill run button
    Click   id=ST_yes
    Sleep   2s
    Click   xpath=//*[@id="confirm_button"]/table/tbody/tr/td[1]/a

I confirm the bill status is Pending
    Click   id=expandedView
    Click   xpath=//*[@id="Search_Result2"]//*[contains(text(),'BR-00000063')]
    The bill status is  Pending

The bill status is
    [Arguments]     ${value}
    type    id=cusSearch   ${value}


I go to the subscription created
    I go to the subcription     ${USER-ACCOUNT-NUMBER}