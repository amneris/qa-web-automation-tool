*** Settings ***
Resource	../config.robot

Documentation  	FEATURE GOLDBUSTER-59


Metadata    Version        AbaWebApps
Metadata	Feature	      GOLDBUSTER-14 hide starting pop ups and member get member
Metadata    Executed At    ${HOME_PAGE} environment

Suite Set Up	openRemoteBrowser	${BROWSER}	${SITE URL}	${REMOTE SERVER}
#Suite Set Up	openBrowser	${BROWSER}	${SITE URL}
#Test Tear Down	close browser
Suite Tear Down	close browser
Test Set Up	clear cookies
Test Tear Down	Run Keywords
...				Run Keyword If Test Failed	get screen shot	AND
...				Run Keyword If Test Failed	Logout	
Default Tags	AbaWebApps


*** Test Cases **


#Users should see welcome pop up that describe how to start
#	[Documentation]	show pop up test
#	[Timeout]	80s
#	[Tags]	smoke
#	Given a new user logued in the campus   Pepe con PopUps
#	Then a welcome level test modal should be displayed
#    sleep   2s
#    in welcome modal I click on beginners level A1
#    and I click on continue button
#    sleep   2s
#    welcome pop up that describe how to start should be displayed
#    logout
#    I login to the app with   ${NEWUSEREMAIL}   eureka
#    sleep   2s
#    Introduction popup should be displayed
#    I close introduction popup
#    I dont want to invite friends
#    welcome pop up that describe how to start should be displayed
#    I click on start course
#   popup to invite friends should be displayed
#    sleep   5

starting pop up should not be displayed after new user select beginners level
 	[Documentation]	hide popups in beginners level
 	[Timeout]	80s
 	[Tags]	smoke
 	Given a new user logued in the campus   hidePP in beginersLevel
 	Given I add cookie to hide hide starting pop ups and member get member
    then in welcome modal I select beginners level A1
    and welcome pop up that describe how to start should not be displayed
     sleep  3s
#starting pop up should not be displayed when the user is navegating for complete course level 1
   then I go to complete course section
   in complete course secction I select on a unit   1001
   and welcome pop up that describe how to start should not be displayed
    then logout
#After the second register, the member get member one popup should not be displayed in beginners level
     given I login to the app with     ${NEWUSEREMAIL}   eureka
     sleep   3s
     then I close the introduction popup
     and welcome pop up that describe how to start should not be displayed
     and popup to invite friends should NOT be displayed
     sleep   3

hide starting pop ups and member get memberselect intermediate level
 	[Documentation]	hide popups in intermediate
 	[Timeout]	80s
 	[Tags]	medium
 	Given a new user logued in the campus   HidePP in intermediateLevel
 	Given I add cookie to hide hide starting pop ups and member get member
    Then in welcome modal I select intermediate level B1
    and welcome pop up that describe how to start should not be displayed
     sleep  3s
#starting pop up should not be displayed when the user is navegating for complete course unit49
     then I go to complete course section
     then in complete course secction I select on a unit   1049
     and welcome pop up that describe how to start should not be displayed
     logout
#After the second register, the member get member one popup should not be displayed in intermediate level
     given I login to the app with     ${NEWUSEREMAIL}   eureka
     sleep   3s
     then I close the introduction popup
     and welcome pop up that describe how to start should not be displayed
     and popup to invite friends should NOT be displayed
     sleep   5

hide starting pop ups and member get member after select Advanced level
 	[Documentation]	hide popups in Advanced
 	[Timeout]	80s
 	[Tags]	main
 	Given a new user logued in the campus   Pepe sin PopUps
 	Given I add cookie to hide hide starting pop ups and member get member
    then in welcome modal I select Advanced level B2 - C1
    and welcome pop up that describe how to start should not be displayed
     sleep  3s
#starting pop up should not be displayed when the user is navegating for complete course unit 1097
     then I go to complete course section
     then in complete course secction I select on a unit   1097
     and welcome pop up that describe how to start should not be displayed
     logout
#After the second register, the member get member one popup should not be displayed in lowerintermediate level
     I login to the app with     ${NEWUSEREMAIL}   eureka
     sleep   3s
     then I close the introduction popup
     and welcome pop up that describe how to start should not be displayed
     and popup to invite friends should NOT be displayed
     sleep   5

hide starting pop ups and member get member after select Business level
 	[Documentation]	hide popups in Business
 	[Timeout]	80s
 	[Tags]	main
 	Given a new user logued in the campus   Pepe sin PopUps
 	Given I add cookie to hide hide starting pop ups and member get member
    then in welcome modal I select Business level C1
     welcome pop up that describe how to start should not be displayed
     sleep  3s
#starting pop up should not be displayed when the user is navegating for complete course unit 1121
     then I go to complete course section
     then in complete course secction I select on a unit   1121
     and welcome pop up that describe how to start should not be displayed
     logout
#After the second register, the member get member one popup should not be displayed in Business level
     I login to the app with     ${NEWUSEREMAIL}   eureka
     sleep   3s
     then I close the introduction popup
     and welcome pop up that describe how to start should not be displayed
     and popup to invite friends should NOT be displayed


