*** Settings ***
Resource	../config.robot

#Metadata    Version        webapps V4
#Metadata    More Info      contact with aospina@abaenglish.com
#Metadata    Executed At    ${HOME_PAGE}

Suite Set Up	openRemoteBrowser	${BROWSER}	${SITE URL}	${REMOTE SERVER}
#Suite Set Up	openBrowser	${BROWSER}	${SITE URL}
#Test Tear Down	close browser
Suite Tear Down	close browser
Test Set Up	clear cookies
Test Tear Down	Run Keyword If Test Failed	get screen shot
Force Tags      req-42  
#Default Tags	main

*** Test Cases ***

###login cases####

	
login with invalid email address should fail
	[Documentation]	invalid email addres should have an error message 
	[Tags]	medium
	Given login page is open
	then I want to login as	invalidMail	${empty}
	and I should receive an error message in case of invalid email address

I want to verify if there is a valid error message in case of empty password
	[Documentation]	empty password should have an error message
	[Tags]	main	
	Given login page is open
	then I want to login as	${SP_USER}	${empty}
	then credentials are submitted
	and I should receive an error message in case of empty password
	
I want to verify if there is a valid error message in case of valid email and invalid password
	[Documentation]	valid email and invalid password
	[Tags]	medium	
	Given login page is open
	then I want to login as	${SP_USER}	invalid
	then credentials are submitted
	and I should receive error message in case of invalid Password
	

I want to verify if there is a valid error message in case of valid email and case sensitive in password field
	[Documentation]	case sensitive in password field
	[Tags]	main	
	Given login page is open
	then I want to login as	${SP_USER}	bIGbANGtEST
	then credentials are submitted
	and I should receive error message in case of invalid Password

I want to verify if there is a valid error message in case entering a valid email without abaEnglish account
	[Documentation]	valid email without aba account
	[Tags]	medium	
	Given login page is open
	then I want to login as	aa@aba.com	passs
	then credentials are submitted
	and I should receive error message in case of credentials does not exist on the databace

I want to access to aba English web site with valid acount
	[Documentation]	I want go to access to aba english web site
	[Tags]	smoke
	Given login page is open
	then I want to login as	${SP_USER}	${SP_PASS}
	then credentials are submitted
	after entering valid credentials I should se my account
	and I close crazy popup
	Logout


	
	
