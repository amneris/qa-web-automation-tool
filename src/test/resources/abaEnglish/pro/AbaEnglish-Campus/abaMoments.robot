
*** Settings ***
Resource	../config.robot

Documentation  	FEATURE AbaMomentsWeb - Spider-60.  This is the test plan for the aba Moment functionality.


Metadata    Version        AbaWebApps
Metadata	Feature	      Spider-60 ABA Moments Information Screens (Web)
Metadata    Executed At    ${HOME_PAGE} environment

#Suite Set Up	openRemoteBrowser	${BROWSER}	${SITE URL}	${REMOTE SERVER}
Suite Set Up	openBrowser	${BROWSER}	${SITE URL}
#Test Tear Down	close browser
Suite Tear Down	close browser
Test Set Up	clear cookies
Test Tear Down	Run Keywords
...				Run Keyword If Test Failed	get screen shot	AND
...				Run Keyword If Test Failed	Logout
Default Tags	abaMoments



*** Variables ***

*** Test Cases ***
#Free users should not be able to see the ABA Moments
#    [Documentation]	Credit Card - Error message in case of Empthy Cardholder Name
#    [Timeout]	40s
#    [Tags]	smoke    QAenv    ABAMoments
#    Given a new user logued in the campus   Leia Organa
#    Given I add cookie to hide hide starting pop ups and member get member
#    then in welcome modal I select beginners level A1
#    and I should not see the ABA Moments option in the menu
#    logout

users with premmium subscription should be able to see the ABA Moments
   [Documentation]	Credit Card - Error message in case of Empthy Cardholder Name
   [Timeout]	40s
   [Tags]	medium    QAenv    ABAMoments
    Given a premium user logged in campus leveL   1
    Given I add cookie to hide hide starting pop ups and member get member
    Then ABAmoments section section should be available
    When I click on ABAmoment option available in main menu
    Then List of ABAmoments should be displayed
    #and I should not see the ABA Moments option in the menu
    logout

##expremium users should not be able to see the abaMomments section
#
#
#
#abaMommets section should be displayed after user subscribe like premium user
#
#
#
#abaMomments section should be displayed in diferent languages
#
#
#
#
#
#
#I should be redirected to the current unit after complete the activity and I click in the pop-up
#    Given I login in the application with premium user that is in the unit 2
#    When I click on ABA Moment
#    And I complete the first activity
#    And I accept the ABA Moment confirmation pop-up
#    Then I should be in the course unit    2. A New Friend
#
#I should be redirected to the current unit after complete the activity and I do not click in the pop-up
#    Given I login in the application with premium user that is in the unit 2
#    When I click on ABA Moment
#    And I complete the first activity
#    And I wait until ABA Moment confirmation pop-up does not exist
#    Then I should be in the course unit    2. A New Friend
#
#I can stay in the ABA Moment section after an activity is completed
#    Given I login in the applicatiom with premium user
#    When I click on ABA Moment
#    And I complete the first activity
#    And I close the ABA Moment confirmation pop-up
#    Then I should be in the ABA Moment menu
#
#I should lost the progress when the ABA Moment activity is not completed
#    Given I login in the applicatiom with premium user
#    When I click on ABA Moment
#    And I complete the first two questions of the first ABA Moment
#    And I click on Complete course
#    And I click on ABA Moment
#    And I start the first activity
#    Then I should be in the first question of the ABA Moment
#
#I can do two activities of ABA Moment one after another
#    Given I login in the applicatiom with premium user
#    When I click on ABA Moment
#    And I complete the first activity
#    And I close the ABA Moment confirmation pop-up
#    And I start the second activity
#    Then I should be in the first question of the ABA Moment
#
#I should not change the answer if I select an wrong answer
#    Given I login in the applicatiom with premium user
#    When I click on ABA Moment
#    And I start the first activity
#    And I select a wrong answer
#    Then I should be in the first question of the ABA Moment

*** Keywords ***

I login with an premium user
    I login to the app with    ${premiumUser}  ${PASS}

I login in the application with premium user that is in the unit 2
    I login to the app with     qavargas@qa.com     test123




I should be in the course unit
    [Arguments]    ${course-unit}
    Element Should Contain  xpath=//*[@id="course_content"]/div[2]/p/span    ${course-unit}

I complete the first activity
    I start the first activity

I start the first activity
    click   id=
    I accept the introduction pop-up

I accept the introduction pop-up
    click   id=

I accept the ABA Moment confirmation pop-up
    click   id=

I close the ABA Moment confirmation pop-up
    click   id=

I should be in the first question of the ABA Moment
    The ABA Moment progress should be   0%

The ABA Moment progress should be 0%
    #    Element Should Be Visible   xpath=//*[@id="content"]/div/div/div/div[1]/div*[@style='width 0%']
    Element should be visible   xpath=//*[@id="content"]/div/div/div/div[1]/div


cambio de flow
un tipo de ejercicio por ahora
pantalla de introoduccion ciao de momento
nueva pagina en el listado
con add de texto de abamoments clikar el elemento del listado
muestra una cuenta atras 3 -2 1 (punto gaming)
pantaya final de abamoment compltado
se rellena el elemento del listado + popup de experiencia lineal (conclution popup)
