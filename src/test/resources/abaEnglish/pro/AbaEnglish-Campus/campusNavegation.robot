*** Settings ***
Resource	../config.robot
Library 	String

Documentation  	FEATURE GOLDBUSTER-59


Metadata    Version        AbaWebApps
Metadata	Feature	      GOLDBUSTER-14 hide starting pop ups and member get member
Metadata    Executed At    ${HOME_PAGE} environment

#Suite Set Up	openRemoteBrowser	${BROWSER}	${SITE URL}	${REMOTE SERVER}
Suite Set Up	openBrowser	${BROWSER}	${SITE URL}
#Test Tear Down	close browser
Suite Tear Down	close browser
Test Set Up	clear cookies
Test Tear Down	Run Keywords
...				Run Keyword If Test Failed	get screen shot	AND
...				Run Keyword If Test Failed	Logout	
Default Tags	AbaWebApps


*** Test Cases **



#
#Im Navegating for home section with a new user in beginersLevel
# 	[Documentation]	Im Navegating for home section
# 	[Tags]	smoke
#   Given a new user logued in the campus   navigatingTest
#   Given I add cookie to hide hide starting pop ups and member get member
#   then in welcome modal I select beginners level A1
#   and welcome pop up that describe how to start should not be displayed
#    sleep  3s
#   I click on section    ABA Film
#   User should be inside of ABA Film Section
#   I get actual progress
#   click on video
#   Sleep    35s
#   well done popup should be displayed with message go to the next section
#   Click on go to the next section
#   event progress update total should be updated
#   after complete an exercise percentage progress should increase
#   I click on section    Speak
#   User should be inside of Speak section
#   paragraph number should contain a text   1   It is very nice here
#   I click on a paragraph    1
#   tip tip tool should by displayed
#   Next button should be present
#   I click on next button
#   previous button should be present
#   I click on previous
#   I click on section    Write
#   I click on section    Interpret
#   I click on section    Video Class
#   I click on section    Exercises
#   I click on section    Vocabulary
#   I click on section    Assessment
#   then I go to complete course section
#   in complete course secction I select on a unit   1001
#   and welcome pop up that describe how to start should not be displayed
#   then logout
##After the second register, the member get member one popup should not be displayed in beginners level
 #    given I login to the app with     ${NEWUSEREMAIL}   eureka
 #    sleep   3s
 #    then I close the introduction popup
 #    and welcome pop up that describe how to start should not be displayed
 #    and popup to invite friends should NOT be displayed
 #    sleep   3
 #    Logout



Visa Valid card test abaebglish pro
	[Documentation]	Visa Valid card form Canada
	[Timeout]	60s
	[Tags]	stripe    QAenv    Confirmation Page
	Given a new user logued in the campus   Alex Ospina
    I go to the plan page with segment number   1
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name 	FRANCISCO J SANZ ARRANZ
	I enter my Credit card number	4273672185571013
	Visa card should be enable
	I enter the expiration date	10	2021
	I enter the CVV number	905
	I click on confirm Button
	the payment transaction should start
	sleep   2s
	confirmation payment page should be displayed
	I go to the aba english campus
	users with premium suscription should not have the option to go to plans page
    Logout

#Bank transfer - Positive New user registered on spain pay with bank transfer
#	[Documentation]	Positive New user registered on spain pay with bank transfer
#	[Timeout]	60s
#	[Tags]	sepa    QAenv   Confirmation Page
#	Given a new user logued in the campus   A OSPINA-SEPA
#    And I go to the plan page with segment number   2
#	then I click on select button of Two years plan
#	sleep	2s
#	I want to pay by BankTransfer
#	I enter the Account Name	Jordi Lambea
#	I enter Account Number	ES32 2100 8668 2502 0008 5522
#	I select a country	Spain
#	I click on confirm Button
#	the payment transaction should start
#	sleep    2s
#    and confirmation payment page should be displayed
#	#In confirmation page I Get the amount price of my subscription
#	I go to the aba english campus
#	users with premium suscription should not have the option to go to plans page
#	Logout


#hide starting pop ups and member get memberselect intermediate level
# 	[Documentation]	hide popups in intermediate
# 	[Timeout]	80s
# 	[Tags]	medium
# 	Given a new user logued in the campus   HidePP in intermediateLevel
# 	Given I add cookie to hide hide starting pop ups and member get member
#    Then in welcome modal I select intermediate level B1
#    and welcome pop up that describe how to start should not be displayed
#     sleep  3s
##starting pop up should not be displayed when the user is navegating for complete course unit49
#     then I go to complete course section
#     then in complete course secction I select on a unit   1049
#     and welcome pop up that describe how to start should not be displayed
#     logout
##After the second register, the member get member one popup should not be displayed in intermediate level
#     given I login to the app with     ${NEWUSEREMAIL}   eureka
#     sleep   3s
#     then I close the introduction popup
#     and welcome pop up that describe how to start should not be displayed
#     and popup to invite friends should NOT be displayed
#     sleep   5
#
#hide starting pop ups and member get member after select Advanced level
# 	[Documentation]	hide popups in Advanced
# 	[Timeout]	80s
# 	[Tags]	main
# 	Given a new user logued in the campus   Pepe sin PopUps
# 	Given I add cookie to hide hide starting pop ups and member get member
#    then in welcome modal I select Advanced level B2 - C1
#    and welcome pop up that describe how to start should not be displayed
#     sleep  3s
##starting pop up should not be displayed when the user is navegating for complete course unit 1097
#     then I go to complete course section
#     then in complete course secction I select on a unit   1097
#     and welcome pop up that describe how to start should not be displayed
#     logout
##After the second register, the member get member one popup should not be displayed in lowerintermediate level
#     I login to the app with     ${NEWUSEREMAIL}   eureka
#     sleep   3s
#     then I close the introduction popup
#     and welcome pop up that describe how to start should not be displayed
#     and popup to invite friends should NOT be displayed
#     sleep   5
#
#hide starting pop ups and member get member after select Business level
# 	[Documentation]	hide popups in Business
# 	[Timeout]	80s
# 	[Tags]	main
# 	Given a new user logued in the campus   Pepe sin PopUps
# 	Given I add cookie to hide hide starting pop ups and member get member
#    then in welcome modal I select Business level C1
#     welcome pop up that describe how to start should not be displayed
#     sleep  3s
##starting pop up should not be displayed when the user is navegating for complete course unit 1121
#     then I go to complete course section
#     then in complete course secction I select on a unit   1121
#     and welcome pop up that describe how to start should not be displayed
#     logout
##After the second register, the member get member one popup should not be displayed in Business level
#     I login to the app with     ${NEWUSEREMAIL}   eureka
#     sleep   3s
#     then I close the introduction popup
#     and welcome pop up that describe how to start should not be displayed
#     and popup to invite friends should NOT be displayed


