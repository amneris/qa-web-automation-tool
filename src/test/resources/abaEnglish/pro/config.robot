*** Settings ***
Library	com.abaenglish.qa.web.WebDriverLib
Library    OperatingSystem
Library 	DateTime
Library 	String
Resource	../Keywords_PlansAndPrices.robot
Resource	../Keywords_webapps.robot
Resource	../Keywords_zuora.robot
Resource	usersPro.txt
	
*** Variables ***
# URL for CI server. Override with your local server if run elsewhere.
${BROWSER}	ff
${SITE URL}		https://campus.abaenglish.com
${REMOTE SERVER}	http://selenium.aba.land:4444/wd/hub
${Environment}   PRO
#http://selenium.aba.land:4444/wd/hub
#http://localhost:4444/wd/hub


### urls 
${LOGIN_EN_URL}	https://campus.abaenglish.com/en/login
${HOME_PAGE}	https://campus.abaenglish.com/en
${PLANS_PAGE}	https://premium.abaenglish.com/en/plans?segm_id=1
${PLANS_PAGE_SEGMENT2}	https://premium.abaenglish.com/en/plans?segm_id=2
${PLANS_PAGE_SEGMENT3}	https://premium.abaenglish.com/en/plans?segm_id=3
${PLANS_PAGE_SEGMENT4}	https://premium.abaenglish.com/en/plans?segm_id=4
${PLANS_PAGE_BY_DEFAULT}	https://premium.abaenglish.com

#### Currencies
${EUR}	€
${USD}	US $
${BRL}	R$
${MXN}	MXN

${StripeID}	2c92a0fe567e364d01568d282de566d9
${AdyenID}	2c92a0ff567e360b01568d3479ba5224
${SEPA}	2c92a0fe56b17c5f0156b2092c9a6242


### Rateplan Prices -- final price by default
${1_month_EU}	19.99
${6_months_EU}	59.99
${12_months_EU}	74.99
${24_months_EU}	124.99


${1_month_USD}	30.00
${6_months_USD}	150.00
${12_months_USD}	79.99
${24_months_USD}	359.00


${1_month_BRL}	89.99
${6_months_BRL}	399.99
${12_months_BRL}	699.99
${24_months_BRL}	1099.99


${1_month_MXN}	399.99
${6_months_MXN}	1999.99
${12_months_MXN}	3499.99
${24_months_MXN}	5299.99



### Rateplan Prices -- final price by default PRODUCT PLAN 2 QAEU2
${1_month_EU2}	25
${6_months_EU2}	125
${12_months_EU2}	199
${24_months_EU2}	299


${1_month_USD2}	19.99
${6_months_USD2}	59.99
${12_months_USD2}	74.99
${24_months_USD2}	99.99


${1_month_BRL2}	89.99
${6_months_BRL2}	399.99
${12_months_BRL2}	699.99
${24_months_BRL2}	1099.99

${1_month_MXN2}	399.99
${6_months_MXN2}	1999.99
${12_months_MXN2}	3499.99
${24_months_MXN2}	5299.99

############## payment methods

*** Keywords ***

I clear local storage
	Execute JavaScript	localStorage.clear()
	open page	${SITE_URL}


I want a random number
	[Return]  ${time_stamp}
      ${secs}=  Get Time  epoch
      ${time}=  Get Time
      ${time_stamp}=  Convert To String     ${secs}
      Set Suite Variable	${random}	${time_stamp}

I want the time
#	[Return]	${NewDay}
	${yyyy}	Get Time	return year
	${mm}	Get Time	return month
	${dd}	Get Time	return day
	Set Suite Variable	${yy1}	${yyyy}
	Set Suite Variable	${mm1}	${mm}
	Set Suite Variable	${dd1}	${dd}
#	${Agg}=	${dd1}+${NewDay}



### loged users by country
a loged user registered on Spain
	I want to login as	${ES}	${PASS}
	credentials are submitted

a loged user registered on Colombia
	I want to login as	${CO}	${PASS}
	credentials are submitted
a loged user registered on Mexico
	I want to login as	${MXUSER}	${PASS}
	credentials are submitted

a loged user registered on Brasil
	I want to login as	${BR}	${PASS}
	credentials are submitted

a loged user registered on Canada
	I want to login as	${CA}	${PASS}
	credentials are submitted
a loged user registered on Portugal
	I want to login as	${PT}	${PASS}
	credentials are submitted
a loged user registered on Russia
	I want to login as	${RU}	${PASS}
	credentials are submitted
a loged user registered on Italy
	I want to login as	${IT}	${PASS}
	credentials are submitted
a loged user registered on Usa
	I want to login as	${US}	${PASS}
	credentials are submitted
a loged user registered on Taiwan
	I want to login as	${TH}	${PASS}
	credentials are submitted
a loged user registered on German
	I want to login as	${DE}	${PASS}
	credentials are submitted

Logout
   	open page	${HOME_PAGE}/logout



a logged user in payment-funnel
	 [Arguments] 	${COUNTRIE}	${NSEGM}
	open page	${PLANS_PAGE_BY_DEFAULT}/en/plans?autol=${COUNTRIE}1e9c1566723ca5dd878eb15f769139b62&segm_id=${NSEGM}

a logged user in the campus
	[Arguments] 	${COUNTRIE}
	open page	${HOME_PAGE}/en?autol=${COUNTRIE}1e9c1566723ca5dd878eb15f769139b62

an available user logged in plan page ready to be premium
	open page	${PLANS_PAGE_BY_DEFAULT}/en/plans?autol=${COUNTRY}${AUTOLOG2}&segm_id=${NSEGM}
    error message by default should not be shown

a logged user in payment funnel in other language
	[Arguments] 	${LANGX}  ${COUNTRY}	${NSEGM}
	open page	${PLANS_PAGE_BY_DEFAULT}/${LANGX}/plans?autol=${COUNTRY}${AUTOLOG2}&segm_id=${NSEGM}
    error message by default should not be shown

a new user logged in payment-funnel with idCountry
    [Arguments]	${IDcountry}
    I want a random number
    Run And Return Rc    mysql -h "${DBHOST} " -u abaadmin --password="XZ!FXYT^wDZLCX" "aba_b2c" -e "INSERT aba_b2c.user (name,surnames, email, password,city,countryId,userType,keyExternalLogin) VALUES ('New student','fromDatabace ${IDcountry}','qastudent+${random}@abaenglish.com','e7ee2a8281efc413f5789e4409e2bc8c','NULL',${IDcountry},1,'${random}1e9c1566723ca5dd878eb15f769139b62');"
    sleep   2s
    open page	 ${PLANS_PAGE_BY_DEFAULT}/en/plans?autol=${random}1e9c1566723ca5dd878eb15f769139b62



I close crazy popup
   click	xpath=.//*[@id='CampusModals']/div[1]/div/span

i open splittest url
    [Arguments] 	${COUNTRY}	${NSEGM}
    open page	 https://campus-qa.aba.land/es/payments/payment?autol=${COUNTRY}${AUTOLOG2}&segm_id=${NSEGM}
open split test url
     [Arguments] 	${NSEGM}
         open page	 ${PLANS_PAGE_BY_DEFAULT}/es/payments/payment?segm_id=${NSEGM}


the created user should have idcountry num
    [Arguments]   ${IDCOUNTRY}  ${EMAIL}
    Run And Return Rc	mysql -h "${DBHOST}" -u abaadmin --password="XZ!FXYT^wDZLCX" "aba_b2c" -e "UPDATE aba_b2c.user SET countryid = '${IDCOUNTRY}' WHERE email LIKE '${EMAIL}'"
    sleep   1s

I update Test users like free type
    Run And Return Rc	mysql -h "${DBHOST}" -u abaadmin --password="XZ!FXYT^wDZLCX" "aba_b2c" -e "UPDATE aba_b2c.user SET userType = '1' WHERE email LIKE 'qastudent+%@abaenglish.com'"
	Run And Return Rc	mysql -h "${DBHOST}" -u abaadmin --password="XZ!FXYT^wDZLCX" "aba_b2c" -e "UPDATE aba_b2c.user SET userType = '1' WHERE email LIKE 'abaenglishqa+%@gmail.com'"
	sleep   1s