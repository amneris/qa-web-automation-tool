*** Settings ***
Resource	config.robot

Documentation  	This is the test plan for payment funnel.
...				We are testing currency by countrie, selligent segments examples
...				After Editing prices, discounts and features of a rate plan in zuora.  Validate id the changes are uptated in the plan page
...				verify currency by counries
...             checkout page, payment methods by country
...             confirmation page


Metadata    Version        java apps V5
Metadata	Feature	       payments ans subscriptions working with zuora
Metadata    Executed At    ${HOME_PAGE} environment

Suite Set Up	openRemoteBrowser	${BROWSER}	${SITE URL}	${REMOTE SERVER}
#Suite Set Up	openBrowser	${BROWSER}	${SITE URL}
#Test Tear Down	close browser
Suite Tear Down	close browser
Test Set Up	clear cookies
Test Tear Down	Run Keywords
...				Run Keyword If Test Failed	get screen shot	AND
...				Run Keyword If Test Failed	Logout	
Default Tags	java_aps

#Run Keyword If Test Failed	Logout
*** Test Cases **


New user should be able to see the plan page
	[Documentation]	I want to create a new user in aba english
	[Timeout]	40s
	[Tags]	main    QAenv Plan Page
	given I want a random number
	Given login page is open
	then I go to register form by clicking on sing up
	then I fill out the required fields with my credentials	Ziare Vand	${random}@abatest.com	eureka
	then I click on signUp
	and I click in Continue button in the choose level popup
	then free users should have the option to go to plans page
	and like loged user I want to see the available plans to get aba premium
	logout

users with premium suscription should not have the option to go to plans page
	[Documentation]	I want to see my plans with the prices con Spain
	[Timeout]	40s
	[Tags]	medium  Plan Page
	Given I login to the app with	${premiumUser}	${PASS}
	#then Like a loged user I want to go to home page
	then users with premium suscription should not have the option to go to plans page
	open page	${PLANS_PAGE_BY_DEFAULT}
	sleep	2s
	and user should not be able to see the plan page
	logout

	
User without premium suscription should be able to access to payment funnel. Tehn Check the available plans by default and lang
	[Documentation]	As a user I want to see the available range of subscription plans
	[Timeout]	40s
	[Tags]	smoke   Plan Page
	Given a logged user in payment-funnel	es	1
	then page should have the option in the subscription plan to pay monthly
	then page should have the option in the subscription plan to pay Six-monthly
	then page should have the option in the subscription plan to pay Annual
	then page should have the option in the subscription plan to pay for two years
	and Plan page should have 14 days of guarantee
	User should see the annual plan like the most popular
	I change language of the plan page  it
	Plan Page should be displayed in italian language
	I change language of the plan page  fr
	Plan page should be displayed in french language
    I change language of the plan page  es
    Plan Page should be displayed in spanish language
	logout


like a logged user from Mexico I want to see my plans to be Premium User with the price by six monthly and currency
	[Documentation]	I want to see my plans with the prices con Mexico 
	[Timeout]	40s
	[Tags]	medium  Plan Page
	Given a logged user in payment-funnel	mx	1
	#The total price by six monthly should be	${6_months_MXN}
	and The Currency of the plan page should be on	${MXN}
	Logout
	
like a logged user from Colombia I want to see my plans to be Premium User with the price by year and currency
	[Documentation]	I want to see my plans with the prices con Colombia
	[Timeout]	40s 
	[Tags]	medium  Plan Page
	Given a logged user in payment-funnel	co	1
	The total price by year should be	${12_months_USD}
	and The Currency of the plan page should be on	${USD}
	Logout
	
like a logged user from Brasil I want to see my plans to be Premium User with the price by two years and currency
	[Documentation]	I want to see my plans with the prices con Brasil 
	[Timeout]	40s
	[Tags]	medium  Plan Page
	Given a logged user in payment-funnel	br	1
	#The total price for two years should be	${24_months_BRL}
	and The Currency of the plan page should be on	${BRL}
	Logout
	
like a logged user from Canada I want to see my plans to be Premium User
	[Documentation]	I want to see my plans with the prices con Canada 
	[Timeout]	40s
	[Tags]	main
	Given a logged user in payment-funnel	ca	1
	and The Currency of the plan page should be on	${USD}	
	Logout

like a logged user from Potugal I want to see my plans to be Premium User
	[Documentation]	I want to see my plans with the prices con Portugal
	[Timeout]	40s 
	[Tags]	medium  Plan Page
	Given a logged user in payment-funnel	pt	1
	and The Currency of the plan page should be on	${EUR}
	Logout

like a logged user from Russia I want to see my plans to be Premium User
	[Documentation]	I want to see my plans with the prices con Russia 
	[Timeout]	40s
	[Tags]	main    Plan Page
	Given a logged user in payment-funnel	ru	1
	and The Currency of the plan page should be on	${USD}
	Logout
	
like a logged user from Italy I want to see my plans to be Premium User
	[Documentation]	I want to see my plans with the prices con italy
	[Timeout]	40s
	[Tags]	medium  Plan Page
	Given a logged user in payment-funnel	it	1
	and The Currency of the plan page should be on	${EUR}
	Logout
	
like a logged user from Usa I want to see my plans to be Premium User
	[Documentation]	I want to see my plans with the prices con Usa 
	[Timeout]	40s
	[Tags]	medium  Plan Page
	Given a logged user in payment-funnel	us	1
	and The Currency of the plan page should be on	${USD}
	Logout

like a logged user from Taiwan I want to see my plans to be Premium User
	[Documentation]	I want to see my plans with the prices con Taiwan
	[Timeout]	40s
	[Tags]	main    Plan Page
	Given a logged user registered on Taiwan
	then like loged user I want to see the available plans to get aba premium
	and The Currency of the plan page should be on	${USD}
	Logout


I access to plan page on segment 2 - 20 percent of discount
	[Documentation]	segment 2 - 20 percent of discount
	[Timeout]	40s
	[Tags]	smoke   Plan Page
	Given a logged user in payment-funnel	es	2
	then congratulations message about especial offert should be shown
	page should have the option in the subscription plan to pay Six-monthly
	page should have the option in the subscription plan to pay Annual
	page should have the option in the subscription plan to pay for two years
	plan page should have a rate plan with discount of	20
	Plan page should have promotions
	logout

I access to plan page on segment 3 - 40 percent of discount
	[Documentation]	segment 3 - 40 percent of discount
	[Timeout]	40s
	[Tags]	main    Plan Page
	Given a logged user in payment-funnel	es	3
	then congratulations message about especial offert should be shown
	page should have the option in the subscription plan to pay Six-monthly
	page should have the option in the subscription plan to pay Annual
	page should have the option in the subscription plan to pay for two years
	User should see the annual plan like the most popular
	plan page should have a rate plan with discount of	40
	Plan page should have promotions
	logout

I access to plan page on segment 4 - 60 percent of discount 
	[Documentation]	segment 4 - 60 percent of discount 
	[Timeout]	40s
	[Tags]	main    Plan Page
	Given a logged user in payment-funnel	es	4
	User should see the annual plan with promotion 
	the discount of the annual plan should be	60
	Logout

Plan page by default should have the 20 percent of discount
	[Documentation]	october 7 We decide to display de lan page by default with 20 percent of discount
	[Timeout]	40s
	[Tags]	medium  Plan Page
	Given I login to the app with	${ES}	${PASS}
	Then User access to a url with some promotion	${PLANS_PAGE_BY_DEFAULT}
	plan page should have a rate plan with discount of	20
	#Plan page should not have promotions
	Logout

non logued users should not be able to see the plan page
	[Documentation]	non logued users should not be able to see plan pages
	[Timeout]	40s
	[Tags]	medium  Plan Page
	Given I login to the app with	${ES}	${PASS}
	then logout
	sleep   2s
	then open page	${PLANS_PAGE_BY_DEFAULT}
	Url Should Contain	/login

Plan page should have 14 days of guarantee
	[Documentation]	Plan page should have 14 days of guarantee
	[Timeout]	40s
	[Tags]	medium  Plan Page
	Given a logged user in payment-funnel	es	1
	Plan page should have 14 days of guarantee


########################## NEW SEGMENTS IN SELLIGENT ##############

I access segment 6 - 2 Plans with annual and six monthly plan
	[Documentation]	I access to plan page on segment 6
	[Timeout]	40s
	[Tags]	medium   QAenv   Plan Page
	Given a logged user in payment-funnel	es	6
	and The Plan page should contain a rate plan with name	Six-monthly
	and The Plan page should contain a rate plan with name	Annual
	but page should NOT have the option in the subscription plan to pay monthly
	Plan page should have promotions
	logout

I access segment 7 error message should be shown in case of the segment has an error in the id of the rate plan in selligent
	[Documentation]	this segment has an error un a product plan. user should see the error by default
	[Timeout]	40s
	[Tags]	medium   QAenv   Plan Page
	Given a logged user in payment-funnel	es	7
	error in plan page by default should be displayed
	botton try again should be shown
	logout

I access segment 11 with the prices of catalog for Europe 2 with 20 percent of discount
	[Documentation]	I access segment 11 with the prices of catalog for Europe 2 with 20 percent of discount
	[Timeout]	40s
	[Tags]	medium   QAenv   Plan Page
	Given a logged user in payment-funnel	de	11
	The Currency of the plan page should be on	${EUR}
	then I click on select button of monthly plan
    and checkout page should contain name of the user	qa
    and checkout page should contain email of the user	${DE}
    and checkout page should contain selected plan name	Monthly
    and The price of the selected plan should be the same of checkout page	25
    and checkout page should contain a currency on	${EUR}
    and I compare The renewal-date displayed with the number of days of the selected plan	30 days

I access segment 14 error message should be shown in case id of the producs catalogs available ar mixed
	[Documentation]	selligent - segment 14 error message should be shown
	[Timeout]	40s
	[Tags]	medium   QAenv   Plan Page
	Given a logged user in payment-funnel	es	14
	error in plan page by default should be displayed
	botton try again should be shown
	logout



######### CHECKOUT AND PAYMENT METHODS BY COUNTRY #############

I want to Check if each of the payment options are selectable
	[Documentation]	ZOR-94 Check if each of the payment options are selectable
	[Timeout]	40s
	[Tags]	smoke   Checkout Page
	Given a logged user in payment-funnel	es	1
	then I click on select button of monthly plan
	sleep	2s
	then I want to confirm the text support-opening-hours-info
	then I want to pay with credit card - Stripe
	and Credit card number should be available
	Select Main Frame
	and I want to pay by BankTransfer
	Bank Acoount name should be available
	Select Main Frame
	and I click on paypal payment method

With an user register in spain -I want to go to checkout page and compare user details, prices and currency of the selected plan, finally confirm the payment methods available for the country
	[Documentation]	ZOR-94 ZOR-171
	[Timeout]	40s
	[Tags]	smoke   Checkout Page
	Given a logged user in payment-funnel	es	1
	The Currency of the plan page should be on	${EUR}
	then I click on select button of monthly plan
    and checkout page should contain name of the user	qastudent
    and checkout page should contain email of the user	${ES}
    and checkout page should contain selected plan name	Monthly
    and The price of the selected plan should be the same of checkout page	${MYPLANPRICE1M}
    and checkout page should contain a currency on	${EUR}
    and I compare The renewal-date displayed with the number of days of the selected plan	30 days
    then Credit Card option via stripe should be present
    and BankTransfer option via SEPA should be present
    and Country should have PayPal method available
    Logout

With an user register in Portugal -I want to go to checkout page compare user details, prices and currency of the selected plan, finally confirm the payment methods available for the country
	[Documentation]	ZOR-94 ZOR-171
	[Timeout]	40s
	[Tags]	smoke   Checkout Page
	Given a logged user in payment-funnel	pt	1
	The Currency of the plan page should be on	${EUR}
	then I click on select button of monthly plan
    and checkout page should contain name of the user	qa
    and checkout page should contain email of the user	${PT}
    and checkout page should contain selected plan name	Monthly
    and The price of the selected plan should be the same of checkout page	${MYPLANPRICE1M}
    and checkout page should contain a currency on	${EUR}
    and I compare The renewal-date displayed with the number of days of the selected plan	30 days
    then Credit Card option via stripe should be present
    and BankTransfer option via SEPA should be present
    and Country should have PayPal method available
    Logout

With an user register in France -I want to go to confirm the payment methods available for the country
	[Documentation]	ZOR-171
	[Timeout]	40s
	[Tags]	smoke   Checkout Page
	Given a logged user in payment-funnel	fr	1
	The Currency of the plan page should be on	${EUR}
	then I click on select button of monthly plan
    and checkout page should contain name of the user	qa
    and checkout page should contain email of the user	${FR}
    and checkout page should contain selected plan name	Monthly
    and The price of the selected plan should be the same of checkout page	${MYPLANPRICE1M}
    and checkout page should contain a currency on	${EUR}
    and I compare The renewal-date displayed with the number of days of the selected plan	30 days
    then Credit Card option via stripe should be present
    and BankTransfer option via SEPA should be present
    and Country should have PayPal method available
    ##Carte bancaire
    Logout

With an user register in Mexico - in Six-monthly plan compare prices currency, summary details and payment methods available on checkout page
	[Documentation]	ZOR-94 ZOR-171
	[Timeout]	40s
	[Tags]	smoke   Checkout Page
	Given a logged user in payment-funnel	mx	1
	The Currency of the plan page should be on	${MXN}
	then I click on select button of Six-monthly plan
    and checkout page should contain name of the user	qastudent
    and checkout page should contain email of the user	${MX}
    and checkout page should contain selected plan name	Six-monthly
    and The price of the selected plan should be the same of checkout page	${MYPLANPRICE6M}
    and checkout page should contain a currency on	${MXN}
    and I compare The renewal-date displayed with the number of days of the selected plan	180 days
	then Credit Card option via stripe should be present
    and Country should have PayPal method available
    #Country should have an OXXO method to play
    Logout

like a loged user from Colombia - I want to confirm the available payment methods that users should have
	[Documentation]	ZOR-94 ZOR-171
	[Timeout]	40s
	[Tags]	smoke   Checkout Page
	Given a logged user in payment-funnel	co	1
	The Currency of the plan page should be on	${USD}
	then I click on select button of annual plan
    and checkout page should contain name of the user	qastudent
    and checkout page should contain email of the user	${CO}
    and checkout page should contain selected plan name	Annual
    and The price of the selected plan should be the same of checkout page	${MYPLANPRICE12M}
    and checkout page should contain a currency on	${USD}
    and I compare The renewal-date displayed with the number of days of the selected plan	365 days
	then Credit Card option via stripe should be present
	and Country should have PayPal method available
    Logout

With user register in italy -I want to go to checkout page compare user details, prices and currency of the selected plan, finally confirm the payment methods available for the country
	[Documentation]	ZOR-94 ZOR-171
	[Timeout]	40s
	[Tags]	smoke   Checkout Page
	Given a logged user in payment-funnel	it	1
	The Currency of the plan page should be on	${EUR}
	then I click on select button of Two years plan
    and checkout page should contain name of the user	qastudent
    and checkout page should contain email of the user	${IT}
    and The price of the selected plan should be the same of checkout page	${MYPLANPRICE24M}
    and checkout page should contain a currency on	${EUR}
    and I compare The renewal-date displayed with the number of days of the selected plan	730 days
    then Credit Card option via stripe should be present
    and BankTransfer option via SEPA should be present
    and Country should have PayPal method available
    ##Carte bancaire
    Logout

Germany I access to plan page on segment 2 - 20 percent of discount
	[Documentation]	ZOR-94 ZOR-171
	[Timeout]	40s
	[Tags]	medium  Checkout Page
	Given a logged user in payment-funnel	de	2
	plan page should have a rate plan with discount of	20
	then I click on select button of Six-monthly plan
	and checkout page should contain name of the user	qastudent
	and checkout page should contain email of the user	${DE}
	and The price of the selected plan should be the same of checkout page	${MYPLANPRICE6M}
	then Credit Card option via stripe should be present
    and BankTransfer option via SEPA should be present
    and Country should have PayPal method available
    #Country should have Sofort method
    #Country should have eDirectBanking method
    #Country should have ELV method
	logout


like a loged user from Brasil - I want to go to the plans page select Two years plan and compare prices currency and summary details on checkout page
	[Documentation]	ZOR-94 ZOR-171
	[Timeout]	40s
	[Tags]	smoke   Checkout Page
	Given a logged user in payment-funnel	br	1
	The Currency of the plan page should be on	${BRL}
	then I click on select button of Two years plan
    and checkout page should contain name of the user	qastudent
    and checkout page should contain email of the user	${BR}
    and checkout page should contain selected plan name	Two years
    and The price of the selected plan should be the same of checkout page	${MYPLANPRICE24M}
    and checkout page should contain a currency on	${BRL}
    and I compare The renewal-date displayed with the number of days of the selected plan	730 days
    #Country should have a Credit Card AllPago method
    #Country should have PayPal method available
    #Country should have Boleto method
    Logout

Japon I access to plan page on segment 3 - 40 percent of discount
	[Documentation]	ZOR-94 ZOR-171
	[Timeout]	40s
	[Tags]	main    Checkout Page
	Given a logged user in payment-funnel	jp	3
	then I click on select button of annual plan
	and The price of the selected plan should be the same of checkout page	${MYPLANPRICE12M}
	Credit Card option via stripe should be present
    and Country should have PayPal method available
	logout


like a logged user from Taiwan I want to confirm the available payment methods that users should have
	[Documentation]	I want to see my plans with the prices con Taiwan
	[Timeout]	40s
	[Tags]	main    Checkout Page
	Given a logged user registered on Taiwan
	then like loged user I want to see the available plans to get aba premium
	and The Currency of the plan page should be on	${USD}
	then I click on select button of annual plan
	Credit Card option via stripe should be present
    and Country should have PayPal method available
	Logout

like a logged user from turkey I want to confirm the available payment methods that users should have
	[Documentation]	I want to see my plans with the prices con Turkey
	[Timeout]	40s
	[Tags]	main    Checkout Page
	Given a logged user in payment-funnel	tr	3
	and The Currency of the plan page should be on	${USD}
	then I click on select button of annual plan
	then Credit Card option via stripe should be present
    Country should not have PayPal method available
	Logout


User should be redirected to login page if he/she tries to open the checkout page and is not logging in the Web.
	[Documentation]	ZOR-171
	[Timeout]	40s
	[Tags]	medium  Checkout Page
	Given I login to the app with	${ES}	${PASS}
	like loged user I want to see the available plans to get aba premium
	then I click on select button of Six-monthly plan
	${GET-URL}=	Get Page Url
	 Logout
	 sleep	1s
	 Open Page	${GET-URL}
	 Url Should Contain	/login

Plan page prices should display always the same prices when user goes back to plan page coming from checkout page.
	[Documentation]	ZOR-171 italy
	[Timeout]	40s
	[Tags]	medium  Checkout Page
	Given a logged user in payment-funnel	it	1
	then I click on select button of Six-monthly plan
	The price of the selected plan should be the same of checkout page	${MYPLANPRICE6M}
	I want to pay with credit card - Stripe
	Credit card number should be available
	Select Main Frame
	I want to pay by BankTransfer
	Bank Acoount name should be available
	Select Main Frame
	I click on paypal payment method
	Go Back
	The total price by six monthly should be	${PRICETOPAY}
	then I click on select button of annual plan
	The price of the selected plan should be the same of checkout page	${MYPLANPRICE12M}
	I want to pay with credit card - Stripe
	Credit card number should be available
	Select Main Frame
	I want to pay by BankTransfer
	Bank Acoount name should be available
	Select Main Frame
	I click on paypal payment method
	Go Back
	The total price by year should be	${PRICETOPAY}
	then then I click on select button of two years plan
	The price of the selected plan should be the same of checkout page	${MYPLANPRICE24M}
	logout

############### CONFIRMATION PAGE

Monthly plan -Euro I want to be sure that the purchase workflow has the same price. Payment-funnel to confirmation page
  	[Documentation]	ZOR-117
  	[Timeout]	60s
  	[Tags]	medium    QAenv   Confirmation Page
  	Given a new user logued in the campus   Mikal Cadav
    the created user should have idcountry num  ${Italy}   ${NEWUSEREMAIL}
  	I go to the plan page with segment number   1
  	The Currency of the plan page should be on	${EUR}
  	then I click on select button of monthly plan
    and checkout page should contain name of the user	Mikal Cadav
    and checkout page should contain email of the user	${NEWUSEREMAIL}
    and checkout page should contain selected plan name	Monthly
    and The price of the selected plan should be the same of checkout page	${MYPLANPRICE1M}
    and checkout page should contain a currency on	${EUR}
    I get the renewal date
    #and I compare The renewal-date displayed with the number of days of the selected plan	30 days
    then Credit Card option via stripe should be present
    and Country should have PayPal method available
    I pay with credit card option
    confirmation payment page should be displayed
    confirmation payment page should contain the students name  Mikal Cadav
    confirmation payment page should contain the students email    ${NEWUSEREMAIL}
    confirmation payment page should contain a currency on  ${EUR}
    total price in confirmation payment page should be present
    the price of the selected plan should be the same of confirmation page  ${MYPLANPRICE1M}
    Confirmation page should have the same renewal date of checkout page
    Help Center link available
    Logout


SIX Monthly plan -Dolar I want to be sure that the purchase workflow has the same price. Payment-funnel to confirmation page
  	[Documentation]	ZOR-117
  	[Timeout]	60s
  	[Tags]	medium  QAenv   Confirmation Page
  	Given a new user logued in the campus   Jessa Loms
    then the created user should have idcountry num  ${Colombia}   ${NEWUSEREMAIL}
    then I go to the plan page with segment number   2
  	The Currency of the plan page should be on	${USD}
  	then I click on select button of Six-monthly plan
    and checkout page should contain name of the user	Jessa Loms
    and checkout page should contain email of the user	${NEWUSEREMAIL}
    and checkout page should contain selected plan name	Six-monthly
    and The price of the selected plan should be the same of checkout page	${MYPLANPRICE6M}
    and checkout page should contain a currency on	${USD}
    I get the renewal date
    #and I compare The renewal-date displayed with the number of days of the selected plan	180 days
    then Credit Card option via stripe should be present
    and Country should have PayPal method available
    I pay with credit card option
    confirmation payment page should be displayed
    confirmation payment page should contain the students name  Jessa Loms
    confirmation payment page should contain the students email     ${NEWUSEREMAIL}
    confirmation payment page should contain a currency on  ${USD}
    total price in confirmation payment page should be present
    the price of the selected plan should be the same of confirmation page  ${MYPLANPRICE6M}
    Confirmation page should have the same renewal date of checkout page
    Logout

 Annual plan I want to be sure that the purchase workflow has the same price. Payment-funnel to confirmation page
     [Documentation]	ZOR-117
     [Timeout]	60s
     [Tags]	smoke   QAenv   Confirmation Page
     Given a new user logued in the campus   Tomax Trammer
     then the created user should have idcountry num  ${Germany}  ${NEWUSEREMAIL}
     then I go to the plan page with segment number   2
     The Currency of the plan page should be on	${EUR}
     I click on select button of annual plan
     checkout page should contain name of the user	Tomax Trammer
     checkout page should contain email of the user    ${NEWUSEREMAIL}
     checkout page should contain selected plan name	Annual
     The price of the selected plan should be the same of checkout page	${MYPLANPRICE12M}
     checkout page should contain a currency on	${EUR}
     I get the renewal date
     #I compare The renewal-date displayed with the number of days of the selected plan	365 days
     BankTransfer option via SEPA should be present
     then I pay by bank transfer
     confirmation payment page should be displayed
     confirmation payment page should contain the students name  Tomax Trammer
     confirmation payment page should contain the students email     ${NEWUSEREMAIL}
     confirmation payment page should contain a currency on  ${EUR}
     total price in confirmation payment page should be present
     the price of the selected plan should be the same of confirmation page  ${MYPLANPRICE12M}
     Confirmation page should have the same renewal date of checkout page
     Logout

I want to be sure that payments and subscriptions are working on spanish lang
  	[Documentation]	ZOR-117
  	[Timeout]	60s
  	[Tags]	medium  QAenv   Confirmation Page
  	Given a new user logued in the campus   Borran Gyyll
    then the created user should have idcountry num  ${Mexico}  ${NEWUSEREMAIL}
  	I go to plan page in other language    es   2
  	then I click on select button of Six-monthly plan
  	then checkout page should be displayed in spanish language
  	credict card method should be displayed in spanish language
    and checkout page should contain selected plan name	Semestral
    and The price of the selected plan should be the same of checkout page	${MYPLANPRICE6M}
    and checkout page should contain a currency on	${MXN}
    I pay with credit card option
    Confirmation page should be displayed in spanish language
    confirmation payment page should be displayed
    confirmation payment page should contain the students name  Borran Gyyll
    confirmation payment page should contain the students email    ${NEWUSEREMAIL}
    total price in confirmation payment page should be present
    the price of the selected plan should be the same of confirmation page  ${MYPLANPRICE6M}
    Logout


######## ZUORA SUSCRIPTIONS

by purchasing a plan subscription Id should be created on zuora
	[Timeout]	120s
	[Tags]	medium  Confirmation Page   QAenv
	Given a new user logued in the campus   Wolam Moonwood
    then the created user should have idcountry num  ${poland}  ${NEWUSEREMAIL}
    then I go to the plan page with segment number   2
	then I click on select button of two years plan
    I get the renewal date
    then Credit Card option via stripe should be present
    I pay with credit card option
    confirmation payment page should be displayed
    Confirmation page should have the same renewal date of checkout page
    I get the subscription identifier
    Logout
    I login in zuora
    I go to Subscriptions list
    Subscription should be present on the list  ${SubscriptionID}
    I access to the suscription  ${SubscriptionID}
    the Customer Name should be    ${Environment} Wolam Moonwood
    the Current Term should be     24 Month(s)
    In zuora I click on logout


##################### EDITING PRICES, FEATURES IN ZUORA AND CHECKING IN PLAN-PAGE -- ONLY WORKS IN QA ENVIRONMENT MOCK ###############################

zuora I want to edit the price of the Monthly and annual product in all the currencies
	[Tags]	Zuora  QAenv   Plan Page
	given I login in zuora
	then I go to the product catalog with name	${TESTproductCatalogName}
	I want to edit the price of the rate plan in Europe	1M	190
	Set Suite Variable	${NEWEURUP}	${NEWEUR}
	I click on edit list price in other countries
	I edit price of USD Currency	150
	Set Suite Variable	${NEWUSDUP}	${NEWUSD}
	I edit price of MXN Currency	550
	Set Suite Variable	${NEWMXNUP}	${NEWMXN}
	I edit price of BRL Currency	98
	Set Suite Variable	${NEWBRLUP}	${NEWBRL}
	I close price in other currencies popup
	I click on save 
	then I want to edit the price of the rate plan in Europe	12M	80.99
	I click on edit list price in other countries
	I edit price of USD Currency	80.99
	I edit price of MXN Currency	1220
	I edit price of BRL Currency	420
	I close price in other currencies popup
	I click on save 
	sleep	20s
	In zuora I click on logout

#### EUROPE
Edited prices of product should be updated in SPAIN users
	[Documentation]	SP users from Mexico should see the prices with discounts updated in all the segments.
	[Timeout]	40s
	[Tags]	Zuora  QAenv    Plan Page
	Given a logged user in payment-funnel	es	1
    The total price of a month should be	${NEWEURUP}
    The Currency of the plan page should be on	${EUR}
    then I go to the plan page with 20 percent of discount
    The total price of a month should be	${NEWEURUP}
    I go to th plan page in segment 1 without discounts
    Then I get the final price of annual plan
	and Should Be Equal	${MYPLANPRICE12M}	${NEWEUR}
	then I go to the plan page with 20 percent of discount
	and I get the final price of annual plan
	and Should Be Equal	${MYPLANPRICE12M}	 64.79
	then I go to the plan page with 40 percent of discount
	I get the final price of annual plan
	Should Be Equal	${MYPLANPRICE12M}	48.59
	I go to the plan page with 60 percent of discount
	I get the final price of annual plan
	Should Be Equal	${MYPLANPRICE12M}	32.4
	Logout

edited price of product should be updated in ITALY
	[Tags]	Zuora  QAenv    Plan Page
	[Timeout]	40s
	Given a logged user in payment-funnel     it      1
    The total price of a month should be	${NEWEURUP}
    The Currency of the plan page should be on	${EUR}
    then I go to the plan page with 20 percent of discount
    The total price of a month should be	${NEWEURUP}
    I click on select button of monthly plan
    and The price of the selected plan should be the same of checkout page  ${NEWEURUP}
    Logout

	

edited price of product should be updated in GERMANY
	[Tags]	Zuora  QAenv    Plan Page
	[Timeout]	40s
	Given a logged user in payment-funnel	de	1
    The total price of a month should be	${NEWEURUP}
    The Currency of the plan page should be on	${EUR}
    then I go to the plan page with 20 percent of discount
    The total price of a month should be	${NEWEURUP}
    I go to th plan page in segment 1 without discounts
    Then I get the final price of annual plan
    and Should Be Equal	${MYPLANPRICE12M}	${NEWEUR}
    then I go to the plan page with 20 percent of discount
    and I get the final price of annual plan
    and Should Be Equal	${MYPLANPRICE12M}	 64.79
    then I go to the plan page with 40 percent of discount
    I get the final price of annual plan
    Should Be Equal	${MYPLANPRICE12M}	48.59
    I go to the plan page with 60 percent of discount
    I get the final price of annual plan
    Should Be Equal	${MYPLANPRICE12M}	32.4
    Logout


edited price of product should be updated in IRLAND
	[Template]	As a user logued in a country I want to verify if the Monthly price of the plan is updated
	[Tags]	Zuora  QAenv    Plan Page
	[Timeout]	40s
	ie	${NEWEURUP}	${EUR}

edited price of product should be updated in KINGDOM	
	[Template]	As a user logued in a country I want to verify if the Monthly price of the plan is updated
	[Tags]	Zuora  QAenv    Plan Page
	gb	${NEWEURUP}	${EUR}

##America


edited price of product should be updated in ARGENTINA 
	[Template]	As a user logued in a country I want to verify if the Monthly price of the plan is updated
	[Tags]	Zuora  QAenv    Plan Page
	[Timeout]	40s
	ar	${NEWUSDUP}	${USD}



edited price of product should be updated in MEXICO
	[Documentation]	MX ausers from Mexico should see the prices with discounts updated in all the segments.
	[Timeout]	40s
	[Tags]	Zuora  QAenv    Plan Page
	Given a logged user in payment-funnel	mx	1
    The total price of a month should be	${NEWMXNUP}
    The Currency of the plan page should be on	${MXN}
    then I go to the plan page with 20 percent of discount
    The total price of a month should be	${NEWMXNUP}
    I go to th plan page in segment 1 without discounts
    I get the final price of annual plan
	Should Be Equal	${MYPLANPRICE12M}	${NEWMXN}
	then I go to the plan page with 20 percent of discount
	I get the final price of annual plan
	Should Be Equal	${MYPLANPRICE12M}	976
	then I go to the plan page with 40 percent of discount
	I get the final price of annual plan
	Should Be Equal	${MYPLANPRICE12M}	732
	I go to the plan page with 60 percent of discount
	I get the final price of annual plan
	Should Be Equal	${MYPLANPRICE12M}	488
	Logout


edited price of product should be updated in BRAZIL
	[Documentation]	BR ausers from Mexico should see the prices with discounts updated in all the segments.
	[Timeout]	40s
	[Tags]	Zuora  QAenv    Plan Page
	Given a logged user in payment-funnel	br	1
    The total price of a month should be	${NEWBRLUP}
    The Currency of the plan page should be on	${BRL}
    then I go to the plan page with 20 percent of discount
    The total price of a month should be	${NEWBRLUP}
    I go to th plan page in segment 1 without discounts
     I get the final price of annual plan
	Should Be Equal	${MYPLANPRICE12M}	${NEWBRL}
	I go to the plan page with 20 percent of discount
	I get the final price of annual plan
	Should Be Equal	${MYPLANPRICE12M}	336
	I go to the plan page with 40 percent of discount
	I get the final price of annual plan
	Should Be Equal	${MYPLANPRICE12M}	252
	I go to the plan page with 60 percent of discount
	I get the final price of annual plan
	Should Be Equal	${MYPLANPRICE12M}	168
	Logout



##Asia

edited price of product should be updated in Japan
	[Template]	As a user logued in a country I want to verify if the Monthly price of the plan is updated
	[Tags]	Zuora  QAenv    Plan Page
	[Timeout]	40s
	jp	${NEWUSDUP}	${USD}
edited price of product should be updated in HONG KONG
	[Template]	As a user logued in a country I want to verify if the Monthly price of the plan is updated
	[Tags]	EditPrices  QAenv   Plan Page
	[Timeout]	40s
	hk	${NEWUSDUP}	${USD}


Edited price of product should be updated in CHINA
	[Tags]	Zuora  QAenv    Plan Page
	[Timeout]	40s
	Given a logged user in payment-funnel	zh	1
    I get the final price of annual plan
	Should Be Equal	${MYPLANPRICE12M}	${NEWUSD}
	I go to the plan page with 20 percent of discount
	I get the final price of annual plan
	Should Be Equal	${MYPLANPRICE12M}	64.79
	I go to the plan page with 40 percent of discount
	I get the final price of annual plan
	Should Be Equal	${MYPLANPRICE12M}	48.59
	I go to the plan page with 60 percent of discount
	I get the final price of annual plan
	Should Be Equal	${MYPLANPRICE12M}	32.4
	Logout



###################### EDITING DISCOUNT PRODUCTS  ###############################

As a user logued in zuora I want to edit the discount of a rate plan 
	[Tags]	BuildTestCondition	Zuora   QAenv   Plan Page
	given I login in zuora
	then I go to the product catalog with name	QA-TESTD5
	I click on edit button on recurring discount period	60%	12M
	I edit the percentage of the discount amount	30 
	I click on save
	sleep	1s
	In zuora I click on logout
	
I access to plan page on segment 4 - And I want to ensure that the discount was edited to 30 percent
	[Documentation]	I access to plan page on segment 4 - And I want to ensure that the discount was edited to 30 percent
	[Timeout]	40s
	[Tags]	Zuora   QAenv   Plan Page
	Given I login to the app with	${ES}	${PASS}
	Then User access to a url with some promotion	${PLANS_PAGE_SEGMENT4}
	User should see the annual plan with promotion 
	the discount of the annual plan should be	30
	Logout


I enter the original discount for the edited rate plan
	[Tags]	backInitialConditions	Zuora   QAenv
	given I login in zuora
	then I go to the product catalog with name	QA-TESTD5
	I click on edit button on recurring discount period	60%	12M
	I edit the percentage of the discount amount	60 
	I click on save
	sleep	1s
	In zuora I click on logout

 
I access to plan page on segment 4 - i want to confirm that we hav the original 60 percent of discount 
	[Documentation]	I access to plan page on segment 4 - i want to confirm that we hav the original 60 percent of discount 
	[Timeout]	40s
	[Tags]	Zuora   QAenv   Plan Page
	Given I login to the app with	${ES}	${PASS}
	Then User access to a url with some promotion	${PLANS_PAGE_SEGMENT4}
	User should see the annual plan with promotion 
	the discount of the annual plan should be	60
	Logout 
	

Zuora I enter the original prices in qa configuration Product for annual plan
	[Tags]	backInitialConditions	Zuora  QAenv    Plan Page
	Given given I login in zuora
	then Go to the prodict catalog QA-TESTPR5
	and In Zuora I enter the original prices for annual plan
	and In Zuora I enter the original prices for monthly plan
	In zuora I click on logout 

###################### EDITING FEATURED PRODUCTS  ###############################

I want to see the six monthly plan like a Featured produc 
	[Documentation]	I want to see the six monthly plan like a Featured produc 
	[Tags]	BuildTestCondition	Zuora     QAenv
	given as a Head of Monetization I marc a Rate plan like a Featured product	${TESTproductCatalogName}	6M

featured product should be updated in Mexico 
	[Timeout]	40s
	[Tags]	Zuora     QAenv    Plan Page
	sleep	20s
	Given a logged user in payment-funnel	mx	1
	and User should see the six-monthly plan like the most popular
	Logout
	
featured product should be updated in Colombia 
	[Timeout]	40s
	[Tags]	Zuora     QAenv    Plan Page
	Given a logged user in payment-funnel	co	1
	and User should see the six-monthly plan like the most popular
	Logout

featured product should be updated in Brasil 
	[Timeout]	40s
	[Tags]	Zuora     QAenv    Plan Page
	Given a logged user in payment-funnel	br	1
	and User should see the six-monthly plan like the most popular
	Logout


with zuora user I enter the original featured rate plan
	[Tags]	backInitialConditions	Zuora     QAenv    Plan Page
	given I login in zuora
	then I go to the product catalog with name	${TESTproductCatalogName}
	I click on Edit rate plan	6M
	I edit the product like non featured rate plan
	I click on save
	In zuora I click on logout
	
#Now the non featured product should be updated in Spain
#	[Timeout]	40s
#	[Tags]	EditFeature
#	Given a logged user in payment-funnel	es	1
#	and six-monthly plan should not be feature product
#	Logout


######### ACTIVE PAYMENT METHODS #######

####### STRIPE ###########



Check if the card options allows card number, expiry date, cvv value text fields.
	[Documentation]	ZOR-165 stripe
	[Timeout]	40s
	[Tags]	main    stripe    medium    Checkout Page
	Given a logged user in payment-funnel	es	1
	then I click on select button of monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	Cardholder Name should be available
	Credit card number should be available
	expiration date should be available
	CVV number should be available
	Logout

Terms and conditions and Privacy Policy links should be available after selection credit card opcion
	[Documentation]	ZOR-171 Terms and conditions and Privacy Policy
	[Timeout]	40s
	[Tags]	stripe    medium    Checkout Page
	Given a logged user in payment-funnel	fr	1
	then I click on select button of monthly plan
	I click on Credit card option
	sleep	2s
	Terms and conditions link should be shown
	Privacy Policy link should be shown
	Logout

VISA Like new student frome spain I want to be premium user by purchasing with credit card.
	[Documentation]	ZOR-165 Like new student I want to be premium user by purchasing with credit card.
	[Timeout]	80s
	[Tags]	stripe    QAenv    Confirmation Page
	Given a new user logued in the campus    Jake Doneta
	then I open plan page in segment id	11
	then I click on select button of monthly plan
	sleep	1s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name    Jake Doneta
	I enter my Credit card number	4242424242424242
	Visa card should be enable
	Master card should be disabled
	american express should be disabled
	I enter the expiration date	11	2019
	I enter the CVV number	111
	I click on confirm Button
	the payment transaction should start
	sleep	1s
	and confirmation payment page should be displayed
	confirmation payment page should contain a currency on  ${EUR}
	confirmation payment page should have an option to go to the campus online
	total price in confirmation payment page should be present
	#I want to be sure that the price of my subscription is the same thay I select on plan page	${MYPLANPRICE1M} ${EUR}
	User should have the option to download the aba english app by iTunes link
    User should have the option to download the aba english app by google play link
	#I go to the aba english campus
	#users with premium suscription should not have the option to go to plans page
	Logout

VISA Like a student from portugal I want to be premium user by purchasing with credit card
	[Documentation]	ZOR-165 stripe- Credit card visa enter valid info card type should be enabled
	[Timeout]	60s
	[Tags]	stripe   QAenv    Confirmation Page
	Given a new user logued in the campus   Nick Murray
    then the created user should have idcountry num  ${portugal}   ${NEWUSEREMAIL}
    then I go to the plan page with segment number   1
	then I click on select button of monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name	Nick Murray
	I enter my Credit card number	4242424242424242
	Visa card should be enable
	Master card should be disabled
	american express should be disabled
	I enter the expiration date	11	2019
	I enter the CVV number	111
	I click on confirm Button
	the payment transaction should start
	sleep	2s
	and confirmation payment page should be displayed
	Logout

VISA Like a student from germany I want to be premium user by purchasing with credit card
	[Documentation]	Credit Card - Visa debito enter valid info
	[Timeout]	60s
	[Tags]	stripe  QAenv   Confirmation Page
	Given a new user logued in the campus   Waseem Vosh
    then the created user should have idcountry num  ${Germany}   ${NEWUSEREMAIL}
    then I go to the plan page with segment number   2
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name    Waseem Vosh
	I enter my Credit card number	4000056655665556
	I enter the expiration date	11	2020
	I enter the CVV number	111
	I click on confirm Button
	the payment transaction should start
	sleep	2s
	and confirmation payment page should be displayed
	Logout


MasterCard Like a student from Albany I want to be premium user by purchasing with credit card
	[Documentation]	Credit Card - Master card debit enter valid info
	[Timeout]	60s
	[Tags]	stripe  QAenv   Confirmation Page
	Given a new user logued in the campus   enryl Eberle
    then the created user should have idcountry num  ${Albania}   ${NEWUSEREMAIL}
    then I go to the plan page with segment number   2
	then I click on select button of annual plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name	enryl Eberle
	I enter my Credit card number	5200828282828210
	Master card should be enable
	Visa card should be disabled
	american express should be disabled
	I enter the expiration date	11	2020
	I enter the CVV number	111
	I click on confirm Button
	the payment transaction should start
	sleep	2s
	and confirmation payment page should be displayed
	Logout

Master card Prepaid enter valid info - Andorra
	[Documentation]	Credit Card - Master card Prepaid enter valid info
	[Timeout]	60s
	[Tags]	stripe    QAenv   Confirmation Page
	Given a new user logued in the campus   Tarlson Rak
    then the created user should have idcountry num  ${Andorra}   ${NEWUSEREMAIL}
    then I go to the plan page with segment number   4
	then I click on select button of Two years plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name	Tarlson Rak
    I enter my Credit card number	5105105105105100
	Master card should be enable
	I enter the expiration date	11	2020
	I enter the CVV number	111
	I click on confirm Button
	the payment transaction should start
	and confirmation payment page should be displayed
	Logout



#Visa Valid card from Brazil
#	[Documentation]	Visa Valid card from Brazil
#	[Timeout]	40s
#	[Tags]	Main	Payment-methods
#	Given a logged user in payment-funnel	br	1
#	then I click on select button of Six-monthly plan
#	sleep	2s
#	I want to pay with credit card - Stripe
#	I enter the Cardholder Name 	${BR}
#	I enter my Credit card number	4000000760000002
#	Visa card should be enable
#	I enter the expiration date	11	2020
#	I enter the CVV number	111
#	I click on submitButton
#	Old confirmation page should be open
#	In confirmation page I Get the amount price of my subscription
#	I want to be sure that the price of my subscription is the same thay I select on plan page	${MYPLANPRICE6M}
#	I go to the aba english campus
#	users with premium suscription should not have the option to go to plans page

Visa Valid card form Canada
	[Documentation]	Visa Valid card form Canada
	[Timeout]	60s
	[Tags]	stripe    QAenv    Confirmation Page
	Given a new user logued in the campus   Bevel Mrrgwharr
    the created user should have idcountry num  ${Canada}   ${NEWUSEREMAIL}
    I go to the plan page with segment number   1
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name 	Bevel Mrrgwharr
	I enter my Credit card number	4000001240000000
	Visa card should be enable
	I enter the expiration date	11	2020
	I enter the CVV number	111
	I click on confirm Button
	the payment transaction should start
	sleep   2s
	confirmation payment page should be displayed
	I go to the aba english campus
	users with premium suscription should not have the option to go to plans page

Visa Valid card form Mexico
	[Documentation]	Visa Valid card form Mexico
	[Timeout]	60s
	[Tags]	stripe   QAenv    Confirmation Page
	Given a new user logued in the campus   Ben Virai
    the created user should have idcountry num  ${Mexico}   ${NEWUSEREMAIL}
    I go to the plan page with segment number   1
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name	Ben Virai
	I enter my Credit card number	4000004840000008
	Visa card should be enable
	I enter the expiration date	11	2020
	I enter the CVV number	111
	I click on confirm Button
	sleep	2s
	the payment transaction should start
	and confirmation payment page should be displayed
	Logout

American Express enter valid info Belgium
	[Documentation]	Credit Card - American Express enter valid info card type should be enabled
	[Timeout]	60s
	[Tags]	stripe    QAenv   Confirmation Page
	Given a new user logued in the campus   Perth Terek
    the created user should have idcountry num  ${Belgium}   ${NEWUSEREMAIL}
    I go to the plan page with segment number   4
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name 	Perth Terek
	I enter my Credit card number	378282246310005
	american express should be enable
	I enter the expiration date	11	2019
	I enter the CVV number	111
	I click on confirm Button
	the payment transaction should start
	sleep	2s
	and confirmation payment page should be displayed
	Logout

Master card Error should be displayed after entering an invalid card number
	[Documentation]	Credit Card - Master card enter invalid card number
	[Timeout]	40s
	[Tags]	stripe    Checkout Page
	Given a logged user in payment-funnel	de	1
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name	AUDREY TIMMONS
	I enter my Credit card number	5555 5555 5555 4466
	I enter the expiration date	11	2017
	I enter the CVV number	698
	I click on submitButton
	an error about the card number is invalid should be displayed
	Logout


Europe error message should be displayed After entering a valid Diners Club card number
	[Documentation]	Credit Card - Error message in case of Empthy Cardholder Name
	[Timeout]	40s
	[Tags]	stripe    QAenv    Checkout Page
	Given a logged user in payment-funnel	es	1
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name 	${ES}
	I enter my Credit card number	30569309025904
	I enter the expiration date	11	2020
	I enter the CVV number	111
	I click on submitButton
	an error about the card type is invalid should be displayed
	Logout
	#error Your card is not supported. Please use a Visa, MasterCard, or American Express card



Credit Card - Error message in case of Empty Cardholder Name
	[Documentation]	Credit Card - Error message in case of Empthy Cardholder Name
	[Timeout]	40s
	[Tags]	stripe    main    Checkout Page
	Given a logged user in payment-funnel	es	1
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name 	${EMPTY}
	I enter my Credit card number	4111 1111 1111 1111
	I enter the expiration date	11	2019
	I enter the CVV number	555
	I click on submitButton
	an error about the CardHolder name is required should be displayed
	Logout

Credit Card - Charge will be declined with an incorrect cvc code.
	[Documentation]	Charge will be declined with an incorrect cvc code
	[Timeout]	40s
	[Tags]	stripe    QAenv     Checkout Page
	Given a logged user in payment-funnel	es	1
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name 	${ES}
	I enter my Credit card number	4000000000000127
	I enter the expiration date	11	2019
	I enter the CVV number	555
	I click on submitButton
	an error by default about transaction denied should be displayed
	Logout

Credit Card - Charge will be declined becouse the card has expired
	[Documentation]	Charge will be declined becouse Your card has expired
	[Timeout]	40s
	[Tags]	stripe    QAenv    Checkout Page
	Given a logged user in payment-funnel	es	1
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name 	${ES}
	I enter my Credit card number	4000 0000 0000 0069
	I enter the expiration date	11	2019
	I enter the CVV number	555
	I click on submitButton
	an error by default about transaction denied should be displayed
	Logout

Credit Card - Charge will be declined becouse of processing error
	[Documentation]	Charge will be declined becouse Your card has expired
	[Timeout]	40s
	[Tags]	stripe    QAenv    Checkout Page
	Given a logged user in payment-funnel	es	1
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name 	${ES}
	I enter my Credit card number	4000 0000 0000 0119
	I enter the expiration date	11	2019
	I enter the CVV number	555
	I click on submitButton
	an error by default about transaction denied should be displayed
	Logout

Credit Card - Charge will be declined becouse of fraudulent reason
	[Documentation]	Charge will be declined becouse Your card has expired
	[Timeout]	40s
	[Tags]	stripe    QAenv    Checkout Page
	Given a logged user in payment-funnel	es	1
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay with credit card - Stripe
	I enter the Cardholder Name 	${ES}
	I enter my Credit card number	4100 0000 0000 0019
	I enter the expiration date	11	2019
	I enter the CVV number	555
	I click on submitButton
	an error by default about transaction denied should be displayed
	Logout

SIX Monthly plan - Confirmation page should have the same renewal date of checkout page
	[Timeout]	40s
	[Tags]	medium    QAenv     Confirmation Page
	Given a new user logued in the campus   SEPA Goror Angavel
    Then the created user should have idcountry num  ${italy}   ${NEWUSEREMAIL}
    And I go to the plan page with segment number   2
	#Given a logged user in payment-funnel	it	2
	then I click on select button of two years plan
    I get the renewal date
    then Credit Card option via stripe should be present
    and Country should have PayPal method available
    I pay with credit card option
    confirmation payment page should be displayed
    Confirmation page should have the same renewal date of checkout page
    Logout



##########   SEPA #########################################

Check if the bank transfer options allows Acoount name,Acoount number and country
	[Documentation]	ZOR-165 sepa
	[Timeout]	40s
	[Tags]	sepa    Checkout Page
	Given a logged user in payment-funnel	es	1
	then I click on select button of monthly plan
	sleep	2s
	I want to pay by BankTransfer
	Bank Acoount name should be available
	Account Numbert name should be available
	Country field should be available
	Logout

Terms and conditions and Privacy Policy links should be available after select bank transfer opcion
	[Documentation]	ZOR-171 Terms and conditions and Privacy Policy
	[Timeout]	40s
	[Tags]	sepa    Checkout Page
	Given a logged user in payment-funnel	es	1
	then I click on select button of monthly plan
	I click on BankTransfer option
	sleep	2s
	Terms and conditions link should be shown
	Privacy Policy link should be shown
	Logout

Bank transfer - valid account number from Germany
	[Documentation]	Bank transfer - valid account number from Germany
	[Timeout]	50s
	[Tags]	sepa   QAenv    Confirmation Page
	Given a new user logued in the campus   SEPA Goror Angavel
    Then the created user should have idcountry num  ${Germany}   ${NEWUSEREMAIL}
    And I go to the plan page with segment number   1
	#Given an available user logged in plan page ready to be premium    de1    1
	then I click on select button of monthly plan
	sleep	2s
	I want to pay by BankTransfer
	I enter the Account Name	Schneider
	I enter Account Number	DE60444488880023456789
	I select a country	Germany
	#I enter de email	${DE}
	I click on confirm Button
	then the payment transaction should start
	sleep   2s
	and confirmation payment page should be displayed
	#In confirmation page I Get the amount price of my subscription
	#I want to be sure that the price of my subscription is the same thay I select on plan page	${MYPLANPRICE1M} ${EUR}
	I go to the aba english campus
	users with premium suscription should not have the option to go to plans page
	Logout


Bank transfer - valid account number from Italy
	[Documentation]	Bank transfer - valid account number from iTaly
	[Timeout]	50s
	[Tags]	sepa    QAenv   Confirmation Page
	Given a new user logued in the campus   SEPA A. Pacini
    Then the created user should have idcountry num  ${Italy}   ${NEWUSEREMAIL}
    And I go to the plan page with segment number   2
	#Given a logged user in payment-funnel	it1	2
	then I click on select button of Six-monthly plan
	sleep	2s
	I want to pay by BankTransfer
	sleep	3s
	I enter the Account Name	A. Pacini
	I enter Account Number	IT60X0542811101000000123456
	I select a country	Italy
	I click on confirm Button
	the payment transaction should start
	sleep   2s
    and confirmation payment page should be displayed
	#In confirmation page I Get the amount price of my subscription
	#I go to the aba english campus
	users with premium suscription should not have the option to go to plans page
	Logout

Bank transfer - valid account number from France
	[Documentation]	Bank transfer - valid account number from France
	[Timeout]	50s
	[Tags]	sepa    QAenv   Confirmation Page
	Given a new user logued in the campus   SEPA Shehish Malric
    Then the created user should have idcountry num  ${Italy}   ${NEWUSEREMAIL}
    And I go to the plan page with segment number   3
	#Given a logged user in payment-funnel	fr1	3
	then I click on select button of annual plan
	sleep	2s
	I want to pay by BankTransfer
	I enter the Account Name	A. Grand Shehish Malric
	I enter Account Number	FR1420041010050500013M02606
	I select a country	France
	I click on confirm Button
	the payment transaction should start
	sleep   2s
	and confirmation payment page should be displayed
	#In confirmation page I Get the amount price of my subscription
	#I want to be sure that the price of my subscription is the same thay I select on plan page	${MYPLANPRICE1M} ${EUR}
	I go to the aba english campus
	users with premium suscription should not have the option to go to plans page
	Logout

Bank transfer - Positive New user registered on spain pay with bank transfer
	[Documentation]	Positive New user registered on spain pay with bank transfer
	[Timeout]	60s
	[Tags]	sepa    QAenv   Confirmation Page
	Given a new user logued in the campus   SEPA benjamin Button
    Then the created user should have idcountry num  ${Spain}   ${NEWUSEREMAIL}
    And I go to the plan page with segment number   11
	then I click on select button of Two years plan
	sleep	2s
	I want to pay by BankTransfer
	I enter the Account Name	benjamin Button
	I enter Account Number	ES9121000418450200051332
	I select a country	Spain
	I click on confirm Button
	the payment transaction should start
	sleep    2s
    and confirmation payment page should be displayed
	#In confirmation page I Get the amount price of my subscription
	I go to the aba english campus
	users with premium suscription should not have the option to go to plans page
	Logout

Bank transfer - invalid account number
	[Documentation]	Bank transfer - invalid account number from Germany
	[Timeout]	40s
	[Tags]	sepa    medium    Checkout Page
	Given a logged user in payment-funnel	de	1
	then I click on select button of monthly plan
	sleep	2s
	I want to pay by BankTransfer
	I enter the Account Name	Schneider
	I enter Account Number	DE604444888800234567AA
	I select a country	Germany
	I click on submitButton
	an error by default about transaction denied should be displayed
    Logout

Bank transfer - error message should be displayed in case of name field is empty
	[Documentation]	error message should be displayed in case of name field is empty
	[Timeout]	40s
	[Tags]	sepa    medium     Checkout Page
	Given a logged user in payment-funnel	de	4
	then I click on select button of Two years plan
	sleep	2s
	I want to pay by BankTransfer
	I enter the Account Name	${EMPTY}
	I enter Account Number	ES9121000418450200051332
	I select a country	Spain
	I click on submitButton
	an error about the Account holders name is required should be displayed
	Logout

Bank transfer - error message should be displayed in case of account number is empty
	[Documentation]	error message should be displayed in case of account number is empty
	[Timeout]	40s
	[Tags]	sepa    medium    Checkout Page
	Given a logged user in payment-funnel	es	4
	then I click on select button of Two years plan
	sleep	2s
	I want to pay by BankTransfer
	I enter the Account Name	Lyon King
	I enter Account Number	${EMPTY}
	I select a country	Spain
	I click on submitButton
	an error about the Account number is required should be displayed
	Logout

#Bank transfer - error message should be displayed in case of country field is empty
#	[Documentation]	 error message should be displayed in case of country field is empty
#	[Timeout]	40s
#	[Tags]	sepa	Payment-methods
#	Given a logged user in payment-funnel	at	4
#	then I click on select button of Two years plan
#	sleep	2s
#	I want to pay by BankTransfer
#	I enter the Account Name	Lyon King
#	I enter Account Number	ES9121000418450200051332
#	I select a country	${EMPTY}
#	I click on submitButton
#	error message should be displayed in case of country field is empty
#	Logout

####### PAYPAL

Check if the paypal option button, terms and conditions options are available when the user selects paypal option to purchase.
	[Documentation]	ZOR-95 Paypal
	[Timeout]	40s
	[Tags]	medium  paypal  Checkout Page
	Given a logged user in payment-funnel	es	1
	then I click on select button of monthly plan
	sleep	2s
	Country should have PayPal method available
	I select PayPal option to purchase
	PayPal button shoud be available after User selects Paypal to purchase
	Terms and conditions link should be shown
	Privacy Policy link should be shown
	Logout

user should be redirected to the PayPal website
    [Documentation]	ZOR-95 Paypal
    [Timeout]	40s
    [Tags]	medium  paypal  Checkout Page
    Given a logged user in payment-funnel	es	1
    then I click on select button of Six-monthly plan
    sleep	2s
    Country should have PayPal method available
    I select PayPal option to purchase
    I click on Paypal Button
    user should be redirected to paypal web site


#######################  CANCEL REFUND, RENEWALS

Scenary in case of the user decides to cancel the premium subscription en guaranty time. So I cancel the subscription effective immediately
    [Documentation]	ZOR-500 cancel
    [Timeout]	120s
    [Tags]	cancel   NewFunctionality
    given I want the time
    Given a new user logued in the campus   Arya Stark -Cancel Inmediatey
    then the created user should have idcountry num  ${Germany}   ${NEWUSEREMAIL}
    then I go to the plan page with segment number   2
    then I click on select button of monthly plan
    and I pay with credit card option
    confirmation payment page should be displayed
    I get the subscription identifier
    Given I login in zuora
    then I go to Subscriptions list
    and I access to the suscription      ${SubscriptionID}
    then I cancel the subscription by Entering specific dates on fields   ${dateAct}  ${dateAct}
    The subscription information should be updated
    In zuora I click on logout
    sleep   3s
    I go to the aba english campus
    then free users should have the option to go to plans page
    sleep   6s
    logout


Scenary in case of the user decides to cancel the comming renovation. So I cancel the subscription at the end of the current term
    [Documentation]	ZOR-500  cancel current term
    [Timeout]	120s
    [Tags]	cancel   NewFunctionality
    given I want the time
    Given a new user logued in the campus   Tarlson Rak - Cancel-EndOfCurrentTime
    then the created user should have idcountry num  ${italy}   ${NEWUSEREMAIL}
    then I go to the plan page with segment number   3
    then I click on select button of monthly plan
    and I pay with credit card option
    confirmation payment page should be displayed
    I get the subscription identifier
    Given I login in zuora
    then I go to Subscriptions list
    and I access to the suscription      ${SubscriptionID}
    I cancel the subscription at the end of the current term    ${dateAct}
    The subscription information should be updated
    In zuora I click on logout
    sleep   2s
    I go to the aba english campus
    users with premium suscription should not have the option to go to plans page
    sleep   5s
#     #ir a intranet y verificar fecha
     logout
###### TODO cancel a renewal


user decides to Refund his suscription effective immediately
    [Documentation]	ZOR-500 full refund
    [Timeout]	120s
    [Tags]	refund   NewFunctionality
    given I want the time
    Given a new user logued in the campus   Erick Cruz - Refund now
    then the created user should have idcountry num  ${italy}   ${NEWUSEREMAIL}
    then I go to the plan page with segment number   3
    then I click on select button of monthly plan
    and I pay with credit card option
    confirmation payment page should be displayed
    I get the subscription identifier
    Given I login in zuora
    then I go to Subscriptions list
    and I access to the suscription   ${SubscriptionID}
    #${SubscriptionID}
    then I cancel the subscription by Entering specific dates on fields   ${dateAct}  ${dateAct}
    The subscription information should be updated
    I click on the Customer Name link
    in transactions I click on payments
    I click on the payment number
    I click on the more details button
    Click on Refund this payment
    Click on yes button
    I want to comment the refund
    I click on confirm refund
    In zuora I click on logout
    I go to the aba english campus
    then free users should have the option to go to plans page
     sleep   4s
#      #TODO VERIFICAR AMMOUNT EN INTRANET Y FECHA
    logout
#

Parcial Refund suscription
    [Documentation]	ZOR-500 Parcial Refund suscription
    [Timeout]	120s
    [Tags]	refund   NewFunctionality
    given I want the time
    Given a new user logued in the campus   Emilia Clarke - ParcialRefund
    then the created user should have idcountry num  ${italy}   ${NEWUSEREMAIL}
    then I go to the plan page with segment number   2
    then I click on select button of annual plan
    and I pay with credit card option
    confirmation payment page should be displayed
    I get the subscription identifier
    Given I login in zuora
    then I go to Subscriptions list
    and I access to the suscription   ${SubscriptionID}
    I click on the Customer Name link
    in transactions I click on payments
    I click on the payment number
    I click on the more details button
    Click on Refund this payment
    Click on yes button
    I want to comment the refund
    I enter de refund ammount   10.00
    I click on confirm refund
    In zuora I click on logout
    I go to the aba english campus
    users with premium suscription should not have the option to go to plans page
         #TODO VERIFICAR AMMOUNT EN INTRANET Y FECHA
   logout
   I want to login in intranet    aospina@abaenglish.com    q1w2e3r4!
   I go to user section filtered by account user     ${AbaUserID}
   User should have user type  Premium
   sleep    5s
   I go to payments section filtered by account user    ${AbaUserID}
   sleep    5s
