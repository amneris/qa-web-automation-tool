*** Settings ***
Resource	config.robot

Documentation   just to Check on pro some plans creating on selligent

Metadata    Version        java apps V5
Metadata	Feature	       plans and prices by country
Metadata    Executed At    ${HOME_PAGE} environment

Suite Set Up	openRemoteBrowser	${BROWSER}	${SITE URL}	${REMOTE SERVER}
#Suite Set Up	openBrowser	${BROWSER}	${SITE URL}
#Test Tear Down	close browser
Suite Tear Down	close browser
Test Set Up	clear cookies
Test Tear Down	Run Keywords
...				Run Keyword If Test Failed	get screen shot	AND
...				Run Keyword If Test Failed	Logout	
Default Tags	java_aps

#Run Keyword If Test Failed	Logout
*** Test Cases **

I want to confirm that the user is redirected to plan page by clicking on Go to premium button
	[Documentation]	new plan page shouls be displayed
	[Timeout]	40s
	[Tags]	smoke
    I login to the app with  ${CA}  ${PASS}
    then I click on See prices button
    and page should have the option in the subscription plan to pay monthly
    and page should have the option in the subscription plan to pay Six-monthly
    and page should have the option in the subscription plan to pay Annual
    and page should have the option in the subscription plan to pay for two years
    Logout

I go to segment 1 with DE User
	[Documentation]	I go to segment 1 with de User
	[Timeout]	40s
	[Tags]	smoke
	Given a logged user in payment-funnel	de	1
	The Currency of the plan page should be on	${EUR}
	page should have the option in the subscription plan to pay monthly
	page should have the option in the subscription plan to pay Six-monthly
	page should have the option in the subscription plan to pay Annual
	page should have the option in the subscription plan to pay for two years
	#The price by month should be	${1_month_EU}
	The total price by six monthly should be	${6_months_EU}
	The total price by year should be	${12_months_EU}
	The total price for two years should be	${24_months_EU}
    Logout


I go to segment 14 with FR User
	[Documentation]	I go to segment 14 with FR User
	[Timeout]	40s
	[Tags]	smoke
	Given a logged user in payment-funnel	fr	14
	The Currency of the plan page should be on	${EUR}
	page should have the option in the subscription plan to pay monthly
	page should have the option in the subscription plan to pay Six-monthly
	page should have the option in the subscription plan to pay Annual
	page should have the option in the subscription plan to pay for two years
	The price by month should be	${1_month_EU2}
	The total price by six monthly should be	${6_months_EU2}
	The total price by year should be	${12_months_EU2}
	The total price for two years should be	${24_months_EU2}
    Logout

I go to segment 15 with FR User
	[Documentation]	I go to segment 15 with FR User
	[Timeout]	40s
	[Tags]	medium
	Given a logged user in payment-funnel	fr	15
	The Currency of the plan page should be on	${EUR}
	page should have the option in the subscription plan to pay monthly
	page should have the option in the subscription plan to pay Six-monthly
	page should have the option in the subscription plan to pay Annual
	page should have the option in the subscription plan to pay for two years
	plan page should have a rate plan with discount of	20
    Logout

I go to segment 16 with FR User
	[Documentation]	I go to segment 16 with FR User
	[Timeout]	40s
	[Tags]	main
	Given a logged user in payment-funnel	fr	16
	The Currency of the plan page should be on	${EUR}
	page should have the option in the subscription plan to pay monthly
	page should have the option in the subscription plan to pay Six-monthly
	page should have the option in the subscription plan to pay Annual
	page should have the option in the subscription plan to pay for two years
	plan page should have a rate plan with discount of	40
    Logout

I go to segment 17 with FR User
	[Documentation]	I go to segment 17 with FR User
	[Timeout]	40s
	[Tags]	main
	Given a logged user in payment-funnel	fr	17
	The Currency of the plan page should be on	${EUR}
	page should have the option in the subscription plan to pay monthly
	page should have the option in the subscription plan to pay Six-monthly
	page should have the option in the subscription plan to pay Annual
	page should have the option in the subscription plan to pay for two years
	plan page should have a rate plan with discount of	60
    Logout

I go to segment 18 with FR User
	[Documentation]	I go to segment 18 with FR User
	[Timeout]	40s
	[Tags]	main
	Given a logged user in payment-funnel	fr	18
	The Currency of the plan page should be on	${EUR}
	page should have the option in the subscription plan to pay monthly
	page should have the option in the subscription plan to pay Six-monthly
	page should have the option in the subscription plan to pay Annual
	page should have the option in the subscription plan to pay for two years
    Logout

I go to segment 19 with FR User
	[Documentation]	I go to segment 19 with FR User
	[Timeout]	40s
	[Tags]	main
	Given a logged user in payment-funnel	fr	19
	The Currency of the plan page should be on	${EUR}
	page should have the option in the subscription plan to pay monthly
	page should have the option in the subscription plan to pay Six-monthly
	page should have the option in the subscription plan to pay Annual
	page should have the option in the subscription plan to pay for two years
	plan page should have a rate plan with discount of	20
    Logout

I go to segment 23 with IT User
	[Documentation]	I go to segment 23 with IT User
	[Timeout]	40s
	[Tags]	medium
	Given a logged user in payment-funnel	fr	23
	The Currency of the plan page should be on	${EUR}
	page should have the option in the subscription plan to pay monthly
	page should have the option in the subscription plan to pay Six-monthly
	page should have the option in the subscription plan to pay Annual
	page should have the option in the subscription plan to pay for two years
	plan page should have a rate plan with discount of	40
    Logout 
    
I go to segment 26 with IT User
	[Documentation]	I go to segment 26 with IT User
	[Timeout]	40s
	[Tags]	smoke
	Given a logged user in payment-funnel	fr	26
	The Currency of the plan page should be on	${EUR}
	page should have the option in the subscription plan to pay monthly
	page should have the option in the subscription plan to pay Six-monthly
	page should have the option in the subscription plan to pay Annual
	page should have the option in the subscription plan to pay for two years
	plan page should have a rate plan with discount of	60
    Logout

I go to segment 27 - discount with 30 percent
	[Documentation]	GOLDBUSTER-10
	[Timeout]	40s
	[Tags]	smoke
	Given a logged user in payment-funnel	fr	27
	The Currency of the plan page should be on	${EUR}
	page should have the option in the subscription plan to pay monthly
	page should have the option in the subscription plan to pay Six-monthly
	page should have the option in the subscription plan to pay Annual
	page should have the option in the subscription plan to pay for two years
	plan page should have a rate plan with discount of	30
	the discount of the annual plan should be   30
	the discount of the six-monthly plan should be	30
	the discount of two-years plan should be	30
    Logout

I go to segment 28 - discount with 30 percent
	[Documentation]	6M GOLDBUSTER-10
	[Timeout]	40s
	[Tags]	medium   GOLDBUSTER-10
	Given a logged user in payment-funnel	fr	28
	The Currency of the plan page should be on	${EUR}
	page should have the option in the subscription plan to pay monthly
	page should have the option in the subscription plan to pay Six-monthly
	page should have the option in the subscription plan to pay Annual
	page should have the option in the subscription plan to pay for two years
	the discount of the six-monthly plan should be	30
    Logout

I go to segment 29 - discount with 30 percent
	[Documentation]	Annual GOLDBUSTER-10
	[Timeout]	40s
	[Tags]	main   GOLDBUSTER-10
	Given a logged user in payment-funnel	fr	29
	The Currency of the plan page should be on	${EUR}
	page should have the option in the subscription plan to pay monthly
	page should have the option in the subscription plan to pay Six-monthly
	page should have the option in the subscription plan to pay Annual
	page should have the option in the subscription plan to pay for two years
	the discount of the annual plan should be  30
    Logout

I go to segment 30 - discount with 30 percent
	[Documentation]	2years GOLDBUSTER-10
	[Timeout]	40s
	[Tags]	main   GOLDBUSTER-10
	Given a logged user in payment-funnel	fr	30
	The Currency of the plan page should be on	${EUR}
	page should have the option in the subscription plan to pay monthly
	page should have the option in the subscription plan to pay Six-monthly
	page should have the option in the subscription plan to pay Annual
	page should have the option in the subscription plan to pay for two years
	the discount of two-years plan should be	30
    Logout

I go to segment 305 - discount with 70 percent
	[Documentation]	YEAR GOLDBUSTER-10
	[Timeout]	40s
	[Tags]	medium   GOLDBUSTER-10
	Given a logged user in payment-funnel	fr	305
	The Currency of the plan page should be on	${EUR}
	page should have the option in the subscription plan to pay monthly
	page should have the option in the subscription plan to pay Six-monthly
	page should have the option in the subscription plan to pay Annual
	page should have the option in the subscription plan to pay for two years
	the discount of the annual plan should be   70
    Logout

I go to segment 505 - discount with 70 percent
	[Documentation]	YEAR GOLDBUSTER-10
	[Timeout]	40s
	[Tags]	medium   GOLDBUSTER-10
	Given a logged user in payment-funnel	fr	505
	The Currency of the plan page should be on	${EUR}
	page should have the option in the subscription plan to pay monthly
	page should have the option in the subscription plan to pay Six-monthly
	page should have the option in the subscription plan to pay Annual
	page should have the option in the subscription plan to pay for two years
	the discount of the annual plan should be	72
    Logout
