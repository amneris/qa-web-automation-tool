*** Settings ***


*** Variables ***
${abamoments-file-answers}      TXT Files/abamoments.txt



*** Keywords ***  

######################### LOGIN in to th campus ##################################
I go to the aba english campus
	open page	${HOME_PAGE}

login page is open
	open page	${LOGIN_EN_URL}
	urlShouldContain	${LOGIN_EN_URL}

I want to login as
	[Arguments]	${email}	${password}
	#I clear local storage
	login page is open
    type	id=LoginForm_email	${email}
    type	id=LoginForm_password	${password}


credentials are submitted
    click	id=btnStartSession


I login to the app with
	[Arguments]   ${usEmail}   ${Userpassword}
	login page is open
	I want to login as	${usEmail}	${Userpassword}
	credentials are submitted
	after entering valid credentials I should se my account
#	I close crazy popup


after entering valid credentials I should se my account
    element should be visible 	id=my_account

### Error messages login page

I should receive an error message in case of invalid email address
	Element Should Contain	class=errorMessage	You must enter a valid email address

I should receive an error message in case of empty password
	Element Should Contain	id=LoginForm_password_em_	You must enter a password

I should receive error message in case of credentials does not exist on the databace
	Element Should Contain	id=LoginForm_email_em_	The email address is incorrect

I should receive error message in case of invalid Password
	Element Should Contain	id=LoginForm_password_em_	The password is incorrect





###################### Register cases ###############################

I go to register form by clicking on sing up
	click	id=registerForm_collapse
	element should be visible	id=RegisterForm_name

I fill out the required fields with my credentials
	[Arguments]   ${userName}	${newEmail}   ${passwordReg}
	#open page	${SITE URL}
	I go to register form by clicking on sing up
    type	id=RegisterForm_name	${userName}
    type	id=RegisterForm_email	${newEmail}
    type	id=RegisterForm_password	${passwordReg}
    
I click on signUp
	click	id=btnRegister
	
I click in Continue button in the choose level popup
	click	xpath=//*[@id='formLevelSelect']/div[3]/button

I should receive an error message in case of the email addres exist on the data base
	Element Should Contain	id=LoginForm_email_em_	This user already exists in the database
	

a new user logued in the campus
    [Arguments]   ${NAME_LASTNAME}
	I want a random number
	open page	${SITE URL}/en/login
	then I go to register form by clicking on sing up
	then I fill out the required fields with my credentials	${Environment} ${NAME_LASTNAME}   qastudent+${random}@gmail.com	eureka
	Set Suite Variable	${NEWUSEREMAIL}	qastudent+${random}@gmail.com
	Set Suite Variable	${USERNAME}    ${NAME_LASTNAME}
	then I click on signUp
	Then a welcome level test modal should be displayed
	#and I click in Continue button in the choose level popup



I create a new user in other language
	[Arguments]   ${COUNTRY}
	I want a random number
	open page	${SITE URL}/${COUNTRY}/login
	then I go to register form by clicking on sing up
	then I fill out the required fields with my credentials	qaUserCI	${random}@abatest.com	eureka		
	then I click on signUp
	and I click in Continue button in the choose level popup
	

by clicking on upgrade aba premium users should see the old plan page
	[Arguments]	${UserCountry}
	Given I want to login as	${UserCountry}	${PASS}
	then as a free user logued in the campus I click on upgrade aba premium
	in the old plan page I click con continue button in a plan	6
	old payment method page should be displayed
	Logout	



################################# HOME PAGE ################
Like a loged user I want to go to home page
	open Page	${HOME_PAGE}
	#urlShouldContain	${HOME_PAGE}

########################## OLD PLAN PAGE #####################

I open old plan page
    open page   https://campus-qa.aba.land/en/payments/payment
    old plan page should be displayed


old plan page should be displayed
	element should be visible	xpath=//*[@id='idLi1']/div[1]
	element should be visible	xpath=//*[@id='page']
	
in the old plan page I click con continue button in a plan
	[Arguments]	${planNum}
	click	xpath=//*[@id='BtnMonthContinue-${planNum}']
	
old payment method page should be displayed
	urlShouldContain	payments/paymentmethod
	element should be visible	xpath=//*[@id='payment-form']/div/div[2]/div

I click on See prices button
	Like a loged user I want to go to home page
	click	xpath=//*[@id='idBannerConversion']/div/div[4]/a/div

I pay with credit card in old payment method
    type    xpath=//*[@id='PaymentForm_creditCardNumber']   4548812049400004
    Old plan page I enter the expiration date   October    2018
    type    id=PaymentForm_CVC    111
    click    id=btnABA

Old plan page I enter the expiration date
    [Arguments]	${EXP-MONTH}	${EXP-YEAR}
    click	id=PaymentForm_creditCardMonth
    Select From List By Label	id=PaymentForm_creditCardMonth	${EXP-MONTH}
    click	id=PaymentForm_creditCardYear
    Select From List By Label	id=PaymentForm_creditCardYear   ${EXP-YEAR}
	
####################### OLD CONFIRMATION PAGE

Old confirmation page should be open
	#urlShouldContain	payments/confirmedPayment?idPay=
	element should be visible	id=pIdCongratulations

In confirmation page I Get the amount price of my subscription 
 	${GETpriceOldCheckoutPage}=	Get Element Text	xpath=//*[contains(@class, 'payment-confirmation-summary')]//*[contains(text(),'Amount')]
 	Set Suite Variable	${PRICEOFMYPURCHASE}	${GETpriceOldCheckoutPage}
I want to be sure that the price of my subscription is the same that I select on plan page
	[Arguments]	${PRICESELECTED}
	should be equal	Amount: ${PRICESELECTED}	${PRICEOFMYPURCHASE}


#############################################   CAMPUS NAVEGATION #################################################################

### campus moldal

in welcome modal I select Level
    [Arguments]	${level}
    click    xpath=//*[@id='formLevelSelect']/..//*[contains(text(),'${level}')]
    I click on continue button

a welcome level test modal should be displayed
    Element Should Be Visible   id=formLevelSelect
    Element Should Be Visible   xpath=//*[@id='formLevelSelect']/..//*[contains(text(),'Beginners')]
    #Element Should Be Visible   xpath=//*[@class='welcomeLevelTest']/..//*[contains(text(),'Level Test')]

in welcome modal I click on beginners level A1
    click    xpath=//*[@id='formLevelSelect']/..//*[contains(text(),'Beginners')]

in welcome modal I click on intermediate level B1
    click    xpath=//*[@id='formLevelSelect']/..//*[contains(text(),'B1')]

in welcome modal I click on Upper intermediate level B2
    click    xpath=//*[@id='formLevelSelect']/..//*[contains(text(),'Upper intermediate')]

in welcome modal I click on Advanced level B2 - C1
    click    xpath=//*[@id='formLevelSelect']/..//*[contains(text(),'Advanced')]

in welcome modal I click on Business level C1
    click    xpath=//*[@id='formLevelSelect']/..//*[contains(text(),'Business')]

in welcome modal I click on Level Test
    click    xpath=//*[@class='welcomeLevelTest']/..//*[contains(text(),'Level Test')]

I click on continue button
    click    xpath=//*[@id='formLevelSelect']/div[3]/button
    sleep   5s

in welcome modal I select beginners level A1
    in welcome modal I click on beginners level A1
    I click on continue button


in welcome modal I select intermediate level B1
    in welcome modal I click on intermediate level B1
    I click on continue button

in welcome modal I select Upper intermediate level B2
    in welcome modal I click on Upper intermediate level B2
    I click on continue button
    I click on continue button

in welcome modal I select Advanced level B2 - C1
    in welcome modal I click on Advanced level B2 - C1
    I click on continue button

in welcome modal I select Business level C1
    in welcome modal I click on Business level C1
    I click on continue button

####################SECTIONS######################

I click on section
    [Arguments]	${SECTION}
    click    xpath=//*[@class='stateBar']/..//*[contains(text(),'${SECTION}')]

###  ABAFILM SECTION

User should be inside of ABA Film Section
    Element Should Be Visible   id=notebook_content_SITUATION

click on video
    click    id=notebook_content_SITUATION

event watched film should be enabled



well done popup should be displayed with message go to the next section
   Element Should Be Visible    class=helpContainer
   Element Should Be Visible    xpath=//*[@id='sharerdialog']//*[contains(text(),'Well done')]
   Element Should Be Visible    xpath=//*[@id='sharerdialog']//*[contains(text(),'Now you can move on to the')]
   Element Should Be Visible    xpath=//*[@id='linkHabla']

Click on go to the next section
    click    xpath=//*[@id='linkHabla']

## Speak section
User should be inside of Speak section
     Element Should Be Visible   id=notebook_content_STUDY

Next button should be present
     Element Should Be Visible  xpath=//*[@id='siguienteText']

I click on a paragraph
    [Arguments]	${PARAGRAPH}
    click   xpath=//*[@id='${PARAGRAPH}']
    tip tip tool should by displayed


tip tip tool should by displayed
    Element Should Be Visible  xpath=//*[@id='tiptip_content']
    Element Should Be Visible  xpath=//*[@id='record']
    Element Should Be Visible  xpath=//*[@id='hear']
    Element Should Be Visible  xpath=///*[@id='compare']


click on record
    Element Should Be Visible  xpath=//*[@id='record']
    mouse over   xpath=//*[@id='tiptip_content']
    click   xpath=//*[@id='record']
    sleep   1s

tiptip holder should be shown
    Element Should Be Visible   xpath//*[@id='tiptip_holder']/div[2]/div[2]/span[3]

click on heard
    Element Should Be Visible  xpath=//*[@id='hear']
    click   xpath=//*[@id='hear']

I click on compare
    Element Should Be Visible  xpath=///*[@id='compare']
    click   xpath=///*[@id='compare']
    sleep   3s


shared dialog with option to allow url to access to your camera and microphone should be displayed
    Element Should Be Visible  xpath=//*[@id='ABAPlayer1_2']

I click on next button
    click    xpath=//*[@id='siguienteText']

previous button should be present
     Element Should Be Visible    xpath=//*[@id='anteriorText']

I click on previous
    click    xpath=//*[@id='anteriorText']

#############  PROGRESS BAR


I get actual progress
    ${ACT-PROGRESS} =  Get element text    xpath=//*[@class='f19']
     Set Global Variable     ${ACT-PROGRESS}     ${ACT-PROGRESS}


event progress update total should be updated
    ${GET-PROGRESS} =  Get element text    xpath=//*[@class='f19']
     Set Global Variable     ${PROGRESS}     ${GET-PROGRESS}
   # id=totalPercentage

after complete an exercise percentage progress should increase
        #TODO


############ hI MODAL entrada curso

welcome pop up that describe how to start should be displayed
    Element Should Be Visible     id=sharerdialog
   Element Should Be Visible    xpath=//*[@class='helpContainer']/..//*[contains(text(),'approach simulates')]

welcome pop up that describe how to start should not be displayed
    Element Should NOT Be Visible   css=div.comenzarcurso
    Element Should Not Be Present    xpath=//*[@class='helpContainer']/..//*[contains(text(),'approach simulates')]

I click on start course
    click    class=comenzarcurso

I click on close
    Click    class=cerrartext


##### INTRODUCTION POPUP
Introduction popup should be displayed
     Element Should Be Visible     class=popupIntroduccion

I close introduction popup
    click    xpath=//*[contains(@class, 'icon-closethick')]
     sleep   3s


I close the introduction popup
    Introduction popup should be displayed
    I close introduction popup




#####  invite friends pop up

popup to invite friends should be displayed
    Element Should Be Visible    xpath=//*[@id='mgmPopup']/h3

popup to invite friends should NOT be displayed
    Element Should Not Be Present   xpath=//*[@id='mgmPopup']/h3
I dont want to invite friends
    click    id=popup-closeAction
#$('#modalWindow').html($('#modalWindow').html());

########


your Level should be
    [Arguments]	${SelectedLevel}
    Element Should Contain  id=level    ${SelectedLevel}

I change of status

change status popup should be displayed
    Element Should Be Visible    class=bodyPopUpCambiodeLevel

on change status popup i click on continue button
    click   xpath=//*[@id='sharerdialog']/..//*[contains(text(),'Continue')]

 ## sections divLeftMenu
I click on Home
    click    id=idUlHome
    Element Should Be Visible   id=unitNumber

I click on Complete course
    click    id=idUlCourse
    Element Should Be Visible   class=contadorUnidadesContainer
    ##//*[contains(@class, 'ombreSeccion ')]/..//*[contains(text(),'UNITS')]
    Element Should Be Visible   id=idDivUnit1001

I click on Video Classes
    click    id=idUlVideos
    Element Should Be Visible   xpath=//*[contains(@class, 'ombreSeccion ')]/..//*[contains(text(),'VIDEO CLASS')]
    Element Should Be Visible   class=VideoClassLink

I click on Interactive Grammar
    click    id=idUlGrammarla
    Element Should Be Visible   id=grammarMenu
    Element Should Be Visible   xpath=//*[@id='grammarMenu']/li[2]/span[2]


#####  complete course

I go to complete course section
    open page   ${HOME_PAGE}/course/index


I go to a unit


I click on unit 1 a day on the beach
    click   xpath=//*[@id='idDivUnit1001']/div[2]/a

in complete course secction I select on a unit
    [Arguments]	${Unitnumber}
     #I go to complete course section
    click   xpath=//*[@id='idDivUnit${Unitnumber}']/div[2]/a
    sleep   3s



I add cookie to hide hide starting pop ups and member get member
    Execute JavaScript  document.cookie="hidePopup=true"


###############   ABA MOMENTS

ABAmoments section section should be available on the menu
    Element Should Be Visible  id=idUlAbaMoments

I should not see the ABA Moments option in the menu
    Element Should Not Be Visible   id=idUlAbaMoments

in the main menu I click on ABA Moment option
    click   id=idUlAbaMoments
    Url Should Contain  /abamoments/
    Element Should Be Visible   xpath=//*[contains(@class, 'NombreSeccion')]/..//*[contains(text(),'ABAmoment')]




I should see in the course lesson title
    [Arguments]	${course-title}
    Element Should Contain  xpath=//*[contains(@class, 'course_lesson')]    ${course-title}

I should see in the unit title
    [Arguments]	${unit-tile}
    Element Should Contain  xpath=//*[contains(@class, 'courseHeadText')]    ${unit-tile}

# ABA MOMENTS TESTS
#--- menu ABA Moments
I login with an premium user that is in the unit 2
    I login to the app with    ${PREMIUM-USER-LEVEL-2}  ${PREMIUM-PASS-LEVEL-2}

I login with an premium user
    I login to the app with    ${premiumUser}  ${PASS}

#I should not see the ABA Moments option in the menu
#    Element Should Not Be Present   id=idUlAbaMoments

I go to the ABA Moment section
    open page   ${HOME_PAGE}
    Element Should Be Visible   id=idUlAbaMoments
    click   id=idUlAbaMoments
    Element Should Be Visible   id=abamoments-container
    select frame    id=abamoments-container

I start the colors activity
    Element Should Be Visible   xpath=//*[contains(@id,'adf6bbbb-104f-4911-b648-66e7e61c3879')]
    click   xpath=//*[contains(@id,'adf6bbbb-104f-4911-b648-66e7e61c3879')]
    sleep   5s

I start the animals activity
    Element Should Be Visible   xpath=//*[contains(@id,'42c389e0-b2b7-4a47-8321-93a1a2ac38e8')]
    click   xpath=//*[contains(@id,'42c389e0-b2b7-4a47-8321-93a1a2ac38e8')]
    sleep   5s

I should be in the ABA Moment menu
    Element Should Be Visible   xpath=//*[@class='momentsList-container']

I should see in the aba moments description
    [Arguments]     ${text}
    Element Should Contain      xpath=//*[@class='category-header']     ${text}

I should see the colors activity with the name
    [Arguments]     ${text}
    Element Should Contain      xpath=//*[contains(@id,'adf6bbbb-104f-4911-b648-66e7e61c3879')]     ${text}

I should see the animals activity with the name
    [Arguments]     ${text}
    Element Should Contain      xpath=//*[contains(@id,'42c389e0-b2b7-4a47-8321-93a1a2ac38e8')]     ${text}

#--- activities ABA Moments
I complete this amount of aba activities
    [Arguments]     ${amount-activities}
    @{listElements} =   I read information of file   ${abamoments-file-answers}    0
    repeat keyword      ${amount-activities} times     I complete the aba activity     @{listElements}

I select a wrong answer
    @{listElements} =   I read information of file   ${abamoments-file-answers}    0
    ${QUESTION-ID} =    I save the question ID
    ${ANSWER-ID} =      I save the value of a list with split values and values comparison  ${QUESTION-ID}  0   2   :  @{listElements}
    I select the answer     ${ANSWER-ID}

I complete the aba activity
    [Arguments]     @{listElements}
    ${QUESTION-ID} =    I save the question ID
    ${ANSWER-ID} =      I save the value of a list with split values and values comparison  ${QUESTION-ID}  0   1   :  @{listElements}
    I select the answer     ${ANSWER-ID}

I select the answer
    [Arguments]  ${answer}
    Element Should Be Visible   xpath=//*[contains(@class, 'exercise-wrapper currentExercise')]//*[contains(@class, 'answerList-wrapper')]
    ${selector} =	Catenate	SEPARATOR=      xpath=//*[contains(@id,'     ${answer}     ')]//*[contains(@class, 'isActive-true')]
    click  ${selector}
    sleep   2s

I save the question ID
    ${selector-information}=	Get Atribute Value	xpath=//*[contains(@class, 'currentExercise')]//*[contains(@class, 'question-wrapper')]     id
    ${value}=   I split the string    ${selector-information}     :   1
    log     ${value}
    [Return]    ${value}

I close the activity
    Element Should Be Visible   xpath=//*[@class='moment-container']//*[contains(@class, 'closeButton-container')]
    click   xpath=//*[@class='moment-container']//*[contains(@class, 'closeButton-container')]

I should be in the question
    [Arguments]  ${question}
    Element Should Be Visible    xpath=//*[contains(@class, 'moment-container')]
    ${selector-information}=	Get Atribute Value	xpath=//*[contains(@class, 'exercise-wrapper currentExercise')]     id
    ${value}=   I split the string    ${selector-information}     :   1
    Should Be Equal     ${question}     ${value}

I accept the ABA Moment confirmation pop-up
    click   xpath=//*[contains(@class,'lightbox-wrapper')]//*[contains(@class,'button')]

I close the ABA Moment confirmation pop-up
    click   xpath=//*[contains(@class,'lightbox-wrapper')]//*[contains(@class,'closeButton-container')]

I wait until ABA Moment confirmation pop-up does not exist
    Element should be visible                           xpath=//*[contains(@class,'lightbox-wrapper')]//*[contains(@class,'button')]
    I wait until the page does not contain the element  xpath=//*[contains(@class,'lightbox-wrapper')]//*[contains(@class,'button')]


##-- Keywords that can be used by other keywords
I read information of file
    [Arguments]     ${file}     ${lines-deleted}
    [Documentation]     The keyword returns in a list the lines saved in the filed sent.
    ${FILE_CONTENT}=   Get File    ${file}
    @{LINES}=    Split To Lines    ${FILE_CONTENT}
    Remove From List    ${LINES}    ${lines-deleted}
    [Return]    @{LINES}

I save the value of a list with split values and values comparison
    [Documentation]     The keyword split the lines of the list, then it compares the value of one position and returns the value of other position
    ...     value-to-compare: The value that is searched.
    ...     column-compared: In the column where is the value searched.
    ...     column-returned: The column that should be returned when the value searched is fount.
    ...     split-character: The characters used to split each line of the list
    ...     lines: List that will be used.
    [Arguments]     ${QUESTION-ID}  ${column-compared}  ${column-returned}  ${split-character}  @{LINES}
    : FOR    ${LINE}    IN    @{LINES}
    \    @{COLUMNS}=            Split String    ${LINE}    separator=${split-character}
    \    ${QUESTION-CODE}=      Get From List    ${COLUMNS}    ${column-compared}
    \    ${VALID-ANSWER}=       Get From List    ${COLUMNS}    ${column-returned}
    \    ${status} =	    Run Keyword And Return Status    Should Be Equal  ${QUESTION-ID}  ${QUESTION-CODE}
    \   Run Keyword If	${status} == True   Exit For Loop
    [Return]   ${VALID-ANSWER}

I split the string
    [Documentation]     The keyword separates the string sent by the character, then it returns saved in the mtrix position required by the user. The value
    ...     that is saved before the first character sent is saved in the position 0.
    ...     line: The string sent by the user that has to be splitted by the character(s) required.
    ...     line-separator: The character used to split the text. For example:  :   /  ,
    ...     line-number: The string is saved in a matrix that starts in 0, this number is the matrix position that is returned. For example: 0  1  2
    [Arguments]     ${line}     ${line-separator}    ${line-number}
    log     ${line}
    log     ${line-separator}
    @{words} =  Split String    ${line}     ${line-separator}
    ${value} =	Set Variable	@{words}[${line-number}]
    [Return]    ${value}

I wait until the page does not contain the element
    [Documentation]  This keywords waits the time desired by the user until an elements appear, if it does not appear the test fail.
    [Arguments]  ${element}
     : FOR    ${i}    IN RANGE    0    3
     \    ${value}    Run Keyword And Return Status    Element Should Not Be Visible    ${element}
     \    Run keyword if    ${value} == True    Exit For Loop
     \    ${i}    Set Variable    ${i}+1
     Element Should Not Be Visible    ${element}