package com.abaenglish.qa.web;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Helper
{
	

	String screenShotDir = "screenshots/";
	WebDriver driver;




public Helper(WebDriver driver) {
		this.driver	=	driver;
	}


    


/*
	**
    * Take a page screenshot and return it as a FILE
    * 
    * @return File
    */
   public String getScreenShot() throws IOException
   {
       //get screen shot from web driver
       File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

       // get current date time with Date() to create unique file name
       DateFormat dateFormat = new SimpleDateFormat("dd_MMM_yyyy_hh_mm_ssaa");
       
       // get current date time with Date()
       Date date = new Date();
       
       // apply date Format
       String time = dateFormat.format(date);

       String fileName = screenShotDir + time + "screenshot.png";
       File file = new File(fileName);
       FileUtils.copyFile(screenshot, file);

       return (String.format("*HTML* %s", "<a target=\"_blank\" href=" + file.getAbsolutePath().replace("\"", "/") + "> <img width=\"20%\" src=" + file.getAbsolutePath() + " alt=\"Screenshot\" /> </a> taken at URL "
               + driver.getCurrentUrl()));
       
   }
}
