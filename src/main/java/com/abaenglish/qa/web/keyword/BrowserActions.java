package com.abaenglish.qa.web.keyword;

import com.abaenglish.qa.web.Helper;
import com.abaenglish.qa.web.OsUtils;
import com.abaenglish.qa.web.Wait;
import com.abaenglish.qa.web.WebDriverLib;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.robotframework.javalib.annotation.ArgumentNames;
import org.robotframework.javalib.annotation.RobotKeyword;
import org.robotframework.javalib.annotation.RobotKeywords;

import java.io.IOException;
import java.net.URL;

/**
 * Class that defines all the browser keywords
 * 
 * @author mturturiello aospina
 */
@RobotKeywords
public class BrowserActions extends WebDriverLib
{

    @RobotKeyword( "Launches a local browser with the given argument. Example: \n open browser ff | ie | html | op | gc | html" )
    @ArgumentNames( { "browser", "url" } )
    public void openBrowser( String browser, String url )
    {
        log( "Running openBrowser: " + browser );
     

        switch ( browser.toLowerCase() )
        {
            case "ie":
                log( "opening internet explorer browser..." );
                driver = new EventFiringWebDriver( new InternetExplorerDriver() );
                break;

           case "gc":
                log( "opening google chrome browser..." );
                System.setProperty("webdriver.chrome.driver", getChromeDriverDefaultPath());                
                driver = new EventFiringWebDriver(new ChromeDriver());
                break;

            case "html":
                log( "opening html browser..." );
                driver = new EventFiringWebDriver( new HtmlUnitDriver( true ) );
                break;

            case "safari":
                log( "opening safari browser..." );
                driver = new EventFiringWebDriver( new SafariDriver() );
                break;

            case "ff":
             
               log( "opening firefox browser..." );
               driver = new EventFiringWebDriver( new FirefoxDriver() );
               break;
            
            default:
                log( "browser not specified, opening firefox browser..." );
                driver = new EventFiringWebDriver( new FirefoxDriver() );
                break;
        }

        helper = new Helper( driver );
        // TODO set timeout variable
        waiter = new Wait( driver, 15 );
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.get( url );
        

    }

    @RobotKeyword( "Launches a Remote browser with the given arguments. Example: open Browser + siteURL + RemoteBrowser" )
	@ArgumentNames({"browser", "url", "remoteServerURL"})
	public void openRemoteBrowser(String browser, String url, String remoteServerUrl) throws Exception
	{
		log("START - Initializing remote webdriver: " + browser +  " on remote server: " + remoteServerUrl.toString());
		
		DesiredCapabilities dc = new DesiredCapabilities();

		switch(browser.toLowerCase())
		{
		 
		    case "ie":
		        dc = DesiredCapabilities.internetExplorer();
		        dc.setBrowserName("internet explorer");
		        dc.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
		        break;
            
		                
		    case "gc":
		        dc = DesiredCapabilities.chrome();
		        dc.setBrowserName("chrome");
		        break;
		        
		    case "html":
                dc = DesiredCapabilities.htmlUnit();
                dc.setBrowserName("htmlunit");
                dc.setJavascriptEnabled(true);
                break;
        
		    case "sa":
                dc = DesiredCapabilities.safari();
                dc.setBrowserName("safari");
                break;
                
		    case "ff":
                // trying to resolve lazy ff page title
                FirefoxProfile fp = new FirefoxProfile();
                
                dc = DesiredCapabilities.firefox();
                fp.setPreference("webdriver.load.strategy", "unstable"); // As of 2.19. from 2.9 - 2.18 use 'fast'
                dc.setBrowserName("firefox");
                dc.setCapability("firefox", fp);
                break;
               
		    default:    
                dc = DesiredCapabilities.firefox();
                dc.setBrowserName("firefox");
                dc.setCapability("firefox", new FirefoxProfile());
                break;
        } 

        log("END - Initializing remote webdriver: " + browser +  " on remote server: " + remoteServerUrl.toString());

        driver = new Augmenter().augment(new RemoteWebDriver(new URL(remoteServerUrl), dc));
		helper = new Helper(driver);
		//TODO set timeout variable
		waiter	= new Wait(driver,15);
        driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.get( url );

	}

    @RobotKeyword( "Open a page by given URL, like http://www.yourpage.org" )
    @ArgumentNames( { "pageUrl" } )
    public void openPage( String pageUrl )
    {
        log( "open page [" + pageUrl + "]" );
        driver.get( pageUrl );
    }

    @RobotKeyword( "Browser go back" )
    public void goBack()
    {
        log( "go back" );
        driver.navigate().back();
    }

    @RobotKeyword( "Get a browser screenshot and link it inside the report" )
    public void refresh() throws IOException
    {
        log( "refresh page" );
        driver.navigate().refresh();
    }

    @RobotKeyword( "Get a browser screenshot and link it inside the report" )
    public void getScreenShot() throws IOException
    {
        // send img preview and link to logger
        log( helper.getScreenShot() );
    }

    @RobotKeyword( "Clear all browser cookies" )
    public void clearCookies()
    {
        driver.manage().deleteAllCookies();
    }

    @RobotKeyword( "Close current browser" )
    public void closeBrowser()
    {
        driver.quit();
        driver = null;
    }
    
    
    
    private static String getChromeDriverDefaultPath() {
        if (OsUtils.isWindows()) {
            return "drivers/chromedriver-windows-32bit.exe";
        } else if (OsUtils.isMac()) {
            return "drivers/chromedriver-mac-32bit";
        } else if (OsUtils.isLinux()) {
            if (OsUtils.is64Bit()) {
                return "drivers/chromedriver-linux-64bit";
            } else {
                return "drivers/chromedriver-linux-32bit";
            }
        }
        return null;
    }
    
    
    private static String getPhantomjsDriverDefaultPath() {
        
      
        if (OsUtils.isWindows()) {
            return "drivers/phantomjs.1.9.8-windows.exe";
        } else if (OsUtils.isMac()) {
            return "drivers/phantomjs.1.9.8-mac";
        } else if (OsUtils.isLinux()) {
            if (OsUtils.is64Bit()) {
                return "drivers/phantomjs.1.9.8-linux-64bit";
            } else {
                return "drivers/phantomjs1.9.8-linux-32bit";
            }
        }
        return null;
        
    }
    
    @RobotKeyword("Exectute the given java script")
    @ArgumentNames({"javascript"})
    public Object executeJavascript(String javascript) 
    {
     return ((JavascriptExecutor)driver).executeScript(javascript);
    }
    @RobotKeyword("switch to frame by element locator")
    @ArgumentNames({"elementLocator"})
    public void selectFrame(String elementLocator) 
    {
            log(String.format("switchin to frame by '%s'.", elementLocator));
            WebElement el = driver.findElement(locator.autoLocator(elementLocator));
            driver.switchTo().frame(el);
        }
    @RobotKeyword("swith to main frame")
    public void selectMainFrame() 
    {
            driver.switchTo().defaultContent();
        }
}
