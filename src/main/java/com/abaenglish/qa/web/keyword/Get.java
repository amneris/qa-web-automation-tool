package com.abaenglish.qa.web.keyword;

import com.abaenglish.qa.web.WebDriverLib;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.robotframework.javalib.annotation.ArgumentNames;
import org.robotframework.javalib.annotation.RobotKeyword;
import org.robotframework.javalib.annotation.RobotKeywords;

import java.util.ArrayList;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;

/**
 * Class that defines all the get keywords 
 * @author mturturiello, aospina
 *
 */
@RobotKeywords
public class 	Get extends WebDriverLib {



	//public Response response;
	//public Response JsonToRobot;

	@RobotKeyword("Returns the visible label of the selected element from the select list identified by elementLocator.")
	@ArgumentNames({"elementLocator"})
	public String getSelectedListLabel(String elementLocator)
    {
    	log("get selected option for element [" + elementLocator + "]" );
    	
    	By by = locator.autoLocator(elementLocator);
    	waiter.elementDisplayed(by);
		Select selectedOption = new Select(driver.findElement(by));

		return selectedOption.getFirstSelectedOption().getAttribute("label");       
    }

	
	
	public List<String> getSelectedListLabels(String elementLocator)
	{
		List<String> labels = new ArrayList<String>();
		
		Select selectedOption = new Select(driver.findElement(locator.autoLocator(elementLocator)));

		List<WebElement> options = selectedOption.getAllSelectedOptions();
				
		for (WebElement option : options) {
			labels.add(option.getAttribute("label"));
		}
		
		if (options.size() == 0) 
		{
			throw new IllegalStateException(String.format("Select list with locator '%s' does not have any selected values.",locator));
		}

		return labels;
		
	}

	
	@RobotKeyword("Returns the value of the selected element from the select list identified by elementLocator.")
	@ArgumentNames({"elementLocator"})
	public String getSelectedListValue(String elementLocator) 
	{
		Select selectedOption = new Select(driver.findElement(locator.autoLocator(elementLocator)));

		return selectedOption.getFirstSelectedOption().getAttribute("value");
	}

	
	
	public List<String> getSelectedListValues(String elementLocator) 
	{
		List<String> values = new ArrayList<String>();

		Select selectedOption = new Select(driver.findElement(locator.autoLocator(elementLocator)));

		List<WebElement> options = selectedOption.getAllSelectedOptions();
				
		for (WebElement option : options) 
		{
			values.add(option.getAttribute("value"));
		}
		
		if (options.size() == 0) {
			throw new IllegalStateException(String.format("Select list with locator '%s' does not have any selected values.",locator));
		}

		return values;
	}

	
	@RobotKeyword("Returns the element´s text of the given elementLocator.")
	@ArgumentNames({"elementLocator"})
	public String getElementText(String elementLocator)
	{		
		By by = locator.autoLocator(elementLocator);
	
		waiter.elementDisplayed(by);
		
		return driver.findElement(by).getText();
	}
	
	@RobotKeyword("Returns page text.")
	public String getPageText()
	{
		By by = By.tagName("body");
		
		waiter.elementDisplayed(by);
		return driver.findElement(by).getText();
	}
	
	
	@RobotKeyword("Returns page title.")
	public String getPageTitle()
	{
		return driver.getTitle();
	}

	
	@RobotKeyword("Returns page url.")
	public String getPageUrl()
	{
		return driver.getCurrentUrl();
	}
	
	@RobotKeyword("Returns the value of an attibute. it can be: id=xxx, link=xxx, class=xxx, xpath=xxx, name=xxx, css=xxx, tag=xxx.")
	@ArgumentNames({"elementLocator", "myAtribute"})
	public String getAtributeValue(String elementLocator, String nameAttribute)
	{		
		By by = locator.autoLocator(elementLocator);
		waiter.elementDisplayed(by);
		System.out.println("assert element [" + by + "] contains attribute [" + nameAttribute	+ "]"); 
		//WebElement getValue =  driver.findElement(by);	
		return driver.findElement(by).getAttribute(nameAttribute);
		 		
	}
	@RobotKeyword("get jSON response.")
	@ArgumentNames({"urlApi	Gateway", "EndPoint"})
	public Response getJsonResponse(String urlApiGateway, String EndPoint)
	{
		RestAssured.baseURI = urlApiGateway;

       // return  given().get(EndPoint).asString();
		return  given().get(EndPoint).then().
				contentType(ContentType.JSON).extract().response();
	}

}
