package com.abaenglish.qa.web.keyword;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.robotframework.javalib.annotation.ArgumentNames;
import org.robotframework.javalib.annotation.RobotKeyword;
import org.robotframework.javalib.annotation.RobotKeywords;
import org.testng.Assert;

import com.abaenglish.qa.web.Wait;
import com.abaenglish.qa.web.WebDriverLib;
/**
 * Class that defines all the action that can be performed on elements keywords 
 * @author mturturiello, aospina
 *
 */
@RobotKeywords
public class ElementActions extends WebDriverLib {

	
	@RobotKeyword("Click on a element by the given elementLocator, it can be: id=xxx, link=xxx, class=xxx, xpath=xxx, name=xxx, css=xxx, tag=xxx . \n If no type of selector is passed it will use link as default.")
	 @ArgumentNames({"elementLocator"})
	 public void click(String elementLocator) throws InterruptedException
	 {
	  By by = locator.autoLocator(elementLocator);
	  waiter.elementDisplayed(by);
	  log("click to [" + by + "]");    
	     
	     
	     /* scroll to the element before click, used for browser compatibility */
	     ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView({block: 'end', behavior: 'instant'});",driver.findElement(by));     
	     Thread.sleep(500);
	     

	     WebElement element = driver.findElement(by);
	     
	     if("input".equals(element.getTagName())){
	         element.sendKeys(Keys.ENTER);
	      } 
	      else{
	         new Actions(driver).moveToElement(element).click().perform();

	      }
	 }

	@RobotKeyword("Type the given text into the given elementLocator, it can be: id=xxx, link=xxx, class=xxx, xpath=xxx, name=xxx, css=xxx, tag=xxx . \n If no type of selector is passed it will use link as default.")
	 @ArgumentNames({"textToTtype", "elementLocator"})
	 public void type(String elementLocator, String text) 
	 {
	  By by = locator.autoLocator(elementLocator);  
	  waiter.elementDisplayed(by);
	  
	     /* scroll to the element before click, used for browser compatibility */
	  ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView({block: 'end', behavior: 'instant'});",driver.findElement(by));  
	  
	  log(String.format("typing text ['%s'] into field ['%s']", text, by));
	  driver.findElement(by).clear();
	  driver.findElement(by).sendKeys(text);
	   }
	 
	
	@RobotKeyword("Simulate Enter button on the given elementLocator, it can be: id=xxx, link=xxx, class=xxx, xpath=xxx, name=xxx, css=xxx, tag=xxx . \n If no type of selector is passed it will use link as default.")
	@ArgumentNames({"elementLocator"})
	public void pressEnter(String selector) 
	{
		By by = locator.autoLocator(selector);		
		waiter.elementDisplayed(by);
		
		log(String.format("pressing enter for element ['%s']", by));
		driver.findElement(by).sendKeys(Keys.RETURN);
}
	@RobotKeyword("Move the mouse over the given elementLocator, it can be: id=xxx, link=xxx, class=xxx, xpath=xxx, name=xxx, css=xxx, tag=xxx . \n If no type of selector is passed it will use link as default.")
	@ArgumentNames({"elementLocator"})
	 public void mouseOver(String elementLocator) throws InterruptedException 
	{
		By by = locator.autoLocator(elementLocator);
		 waiter.elementDisplayed(by);
		 log("click to [" + by + "]");  
	    
		/* scroll to the element before click, used for browser compatibility */
	     ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView({block: 'end', behavior: 'instant'});",driver.findElement(by));     
	     Thread.sleep(800);
	     
        WebElement el = driver.findElement(by);

        if (el == null) {
            throw new IllegalStateException(String.format("ERROR: Element %s not found", elementLocator));
        }

        //new Actions(driver).moveToElement(el).perform();
        
        Actions builder = new Actions(driver);
        builder.moveToElement(el).build().perform();
        
    }

	@RobotKeyword("Select check box by the given elementLocator, it can be: id=xxx, link=xxx, class=xxx, xpath=xxx, name=xxx, css=xxx, tag=xxx . \n If no type of selector is passed it will use link as default.")
	@ArgumentNames({"elementLocator"})
	public void selectCheckbox(String elementLocator) 
	{
	        log(String.format("selecting checkbox '%s'.", elementLocator));

	        WebElement el = driver.findElement(locator.autoLocator(elementLocator));
	        if (!el.isSelected()) 
	        {
	            el.click();
	        }
	    }
	
	@RobotKeyword("Unselect check box by the given elementLocator, it can be: id=xxx, link=xxx, class=xxx, xpath=xxx, name=xxx, css=xxx, tag=xxx . \n If no type of selector is passed it will use link as default.")
	@ArgumentNames({"elementLocator"})
	public void unselectCheckbox(String elementLocator) 
	{
	        log(String.format("unselecting checkbox '%s'.", elementLocator));

	        WebElement el = driver.findElement(locator.autoLocator(elementLocator));
	        if (el.isSelected()) 
	        {
	            el.click();
	        }
	    }

	@RobotKeyword("Select the given label from drop down menu by the given elementLocator, it can be: id=xxx, link=xxx, class=xxx, xpath=xxx, name=xxx, css=xxx, tag=xxx . \n If no type of selector is passed it will use link as default.")
	@ArgumentNames({"elementLocator","label"})
	public void selectFromListByLabel(String elementLocator, String optionLabel)
    {
    	log("select element is [" + locator + "], select option label [" + optionLabel + "]"  );
    	
    	By by = locator.autoLocator(elementLocator);
    	waiter.elementDisplayed(by);
    	
    	Select droplist = new Select(driver.findElement(by));
        droplist.selectByVisibleText(optionLabel);        
    }

	
}


