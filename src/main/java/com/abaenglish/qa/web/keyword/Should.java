package com.abaenglish.qa.web.keyword;

import com.abaenglish.qa.web.WebDriverLib;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.robotframework.javalib.annotation.ArgumentNames;
import org.robotframework.javalib.annotation.RobotKeyword;
import org.robotframework.javalib.annotation.RobotKeywords;
import org.testng.Assert;

import java.io.UnsupportedEncodingException;

/**
 * Class that defines all the assertion keywords 
 * @author aospina
 *
 */
@RobotKeywords
public class Should extends WebDriverLib {

	
	@RobotKeyword("Assert that current page contains the given text")
	@ArgumentNames("text")
	public void pageShouldContain(String text) 
	{
		log("assert page [" + driver.getCurrentUrl()+ "] contains text [" + text + "]");
		
		WebDriverWait wait = new WebDriverWait(driver, 15);
		
		String xpath = String.format("//*[contains(., '%s')]",text);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy( By.xpath( xpath ) ));
		//Assert.assertTrue(driver.findElement(By.tagName("body")).getText().contains(text),"text [" + text +"] not found on page [" + driver.getCurrentUrl() + "]");
	}

	@RobotKeyword("Assert that current title contains the given text")
	@ArgumentNames("text")
	public void titleShouldContain(String title)
	{
		waiter.titleContains(title);
		Assert.assertTrue(driver.getTitle().contains(title),"title doesnt contains tex [" + title + "], actual title is [" + driver.getTitle() +"]");
	}
	

	@RobotKeyword("Assert that current title is equal to the given title")
	@ArgumentNames("title")
	public void titleShouldBe(String title) 
	{
		Assert.assertTrue(driver.getTitle().equals(title),"title is not equal to [" + title + "], actual title is [" + driver.getTitle() +"]");
	}

	
	@RobotKeyword("Assert that current url contains the given text")
	@ArgumentNames("text")
	public void urlShouldContain(String text) throws UnsupportedEncodingException 
	{

	    
		System.out.println("assert url [" + driver.getCurrentUrl()+ "] contains text [" + text + "]");
		waiter.urlContains( text );
		//Assert.assertTrue(url.contains(text),"url doesnt contains tex [" + text + "], actual url is [" + url +"]");
	}

	@RobotKeyword("Assert that current url is equal to the given text")
	@ArgumentNames("text")
	public void urlShouldBe(String url) throws UnsupportedEncodingException 
	{
	    String url2 = java.net.URLDecoder.decode(driver.getCurrentUrl(), "UTF-8");
	    
		System.out.println("assert url [" + driver.getCurrentUrl() + "] is equal to [" + url + "]");
		Assert.assertTrue(url2.equals(url),"url is not equal to [" + url + "], actual url is [" + url2 +"]");
	}

	
	@RobotKeyword("Assert that the given elementLocator contains the given text")
	@ArgumentNames({"elementLocator", "text"})
	public void elementShouldContain(String elementLocator, String expectedText) 
	{
		By by = locator.autoLocator(elementLocator);
		expectedText = expectedText.toLowerCase();
		System.out.println("assert element [" + by + "] contains text [" + expectedText	+ "]");
		waiter.elementTextContains(expectedText, by);
		String elementText = driver.findElement(by).getText().toLowerCase();
		
		Assert.assertTrue(elementText.contains(expectedText), "element [" + by + "] doesnt contains text [" + expectedText	+ "], atual text is [ " + elementText + "]");
	}
	
	@RobotKeyword("Assert that the given elementLocator contains one of the given text")
	@ArgumentNames({"elementLocator","text1", "text2"})
	public void elementShouldContainOneOf(String elementLocator, String... expectedText) 
	{
		By by = locator.autoLocator(elementLocator);
		System.out.println("assert element [" + by + "] contains text [" + expectedText	+ "]");

		waiter.elementDisplayed(by);
		WebElement we = driver.findElement(by);
		String actualText = we.getText().toLowerCase();
		boolean found = false;
		
		for(int i = 0; i<expectedText.length; i++)
		{
			if(actualText.contains(expectedText[i].toLowerCase()))
			{	
				System.out.println("Found match, text [" + expectedText[i].toLowerCase() + "] found inside element [" + by + "]");
				found = true;
				return;
			}
		}
		
		Assert.assertTrue(found, "element [" + by + "] doesnt contains expected text, atual text is [ " + we.getText().toLowerCase() + "]");


	}
	
	
	@RobotKeyword("Assert that the given elementLocator is visible inside the actual page")
	@ArgumentNames("elementLocator")
	public void elementShouldBeVisible(String elementLocator) 
	{
		By by = locator.autoLocator(elementLocator);
		System.out.println("assert element [" + by + "] is present on page");
		
		waiter.elementDisplayed(by);
		Assert.assertTrue( !driver.findElements(by).isEmpty() , "element [" + by + "] not present at [" + driver.getCurrentUrl() +"]");
	}
	
	

	@RobotKeyword("Assert that the given elementLocator is not present inside the actual page")
	@ArgumentNames("elementLocator")
	public void elementShouldNotBePresent(String elementLocator) 
	{
		By by = locator.autoLocator(elementLocator);
		System.out.println("assert element [" + by + "] is present on page");
		waiter.elementDoesNotExist(by);
		Assert.assertTrue( driver.findElements(by).isEmpty() , "element [" + by + "] should not be present at [" + driver.getCurrentUrl() +"]");
	}
	
	   @RobotKeyword("Assert that the given elementLocator is not visible inside the actual page")
	    @ArgumentNames("elementLocator")
	    public void elementShouldNotBeVisible(String elementLocator) 
	    {
	        By by = locator.autoLocator(elementLocator);
	        System.out.println("assert element [" + by + "] is present on page");
	        
	        WebDriverWait wait = new WebDriverWait(driver, 15);
	        boolean resutl = wait.until(ExpectedConditions.invisibilityOfElementLocated(by));	        
	        Assert.assertTrue( resutl , "element [" + by + "] should not be present at [" + driver.getCurrentUrl() +"]");
	    }
		// TODO elementShouldBeEnable IS NOT WORKING WELL 
		@RobotKeyword("Assert that the given elementLocator is enable inside the actual page")
		@ArgumentNames("elementLocator")
		public void elementShouldBeEnable(String elementLocator) 
		{
			By by = locator.autoLocator(elementLocator);
			System.out.println("assert element [" + by + "] is enable on page");
			
	    	Assert.assertTrue( driver.findElement(by).isEnabled(), "element [" + by + "] should not be enable at [" + driver.getCurrentUrl() +"]");
		}


	   @RobotKeyword("confirm that the given element is selected on the radio button, it can be: id=xxx, link=xxx, class=xxx, xpath=xxx, name=xxx, css=xxx, tag=xxx .")
		@ArgumentNames({"elementLocator"})
		public void elementShouldBeSelected(String elementLocator)
	    {
			 log(String.format("verifing if the element is selected '%s'.", elementLocator));
			 By by = locator.autoLocator(elementLocator);
		
			 Assert.assertTrue( driver.findElement(by).isSelected() , "element [" + by + "] is selected on [" + driver.getCurrentUrl() +"]");
			 
	    }
	   
	   @RobotKeyword("confirm that the drop down menu has the given text in the first option, it can be: id=xxx, link=xxx, class=xxx, xpath=xxx, name=xxx, css=xxx, tag=xxx .")
		@ArgumentNames({"elementLocator","label"})
		public void textOfTheSelectedOptionShouldBe(String elementLocator, String labelSelected)
	    {
	   log("verifing the  element  [" + locator + "], has the first option option selected with the text [" + labelSelected + "]"  );
			 By by = locator.autoLocator(elementLocator);
			// waiter.elementDisplayed(by);
			 WebElement mySelectElm = driver.findElement(by); 	
			 Select mySelect= new Select(mySelectElm);
			 WebElement option = mySelect.getFirstSelectedOption();
			 //System.out.println(option.getText());
			 Assert.assertEquals(labelSelected, option.getText());
			 		 
	    }   
}
