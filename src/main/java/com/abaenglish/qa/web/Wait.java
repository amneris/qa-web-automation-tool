package com.abaenglish.qa.web;

import java.io.UnsupportedEncodingException;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;



public class Wait
{

        final int timeout;
		WebDriver driver;


        public Wait(WebDriver driver, int passedTimeout) 
        {
            this.timeout = passedTimeout;
            this.driver = driver;

        }

            /**
             * Wait standard timeout for an element to be displayed
             * @param elementLocator
             */
        public void elementDisplayed(final By elementLocator)
        {
        	 System.out.println("Wait for element:" + elementLocator + " to be displayed within: " + timeout + " seconds" ); 
            try
            {
                new WebDriverWait(driver, timeout) {
                }.until(new ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) 
                    {
     
                        return driver.findElement(elementLocator).isDisplayed();
                    }
                });
            }
            catch(TimeoutException e)
            {
                String ErrorMsg = "Failed wait for elementDisplayed, element: " + elementLocator.toString() + " - not displayed  within timeout of:" + timeout +" seconds  at url: " + driver.getCurrentUrl();
                
                System.out.println(ErrorMsg);
                throw new TimeoutException(ErrorMsg) ;
            }
        }
            
            /**
             * Wait waitInSeconds timeout for an element to be displayed
             * @param elementLocator
             * @param waitInSeconds
             */
            public void elementDisplayed(final By elementLocator, final long waitInSeconds)
            {
                System.out.println("Wait for element:" + elementLocator + " to be displayed within: " + waitInSeconds + " seconds" ); 

                try
                {
                    new WebDriverWait(driver, waitInSeconds) {
                    }.until(new ExpectedCondition<Boolean>() {
                        public Boolean apply(WebDriver driver) {
                            return (driver.findElement(elementLocator).isDisplayed());
                        }
                    });
                }
                catch(TimeoutException e)
                {
                    String ErrorMsg = "Failed wait for elementDisplayed, element: " + elementLocator.toString() + " - not displayed  within timeout of:" + waitInSeconds +" seconds at url: " + driver.getCurrentUrl();
                    
                    
                    throw new TimeoutException(ErrorMsg) ;
                }
                
            }
        
            /**
             * Wait a timeout for an element to not exists
             * @param elementLocator
             */
            public void elementDoesNotExist(final By elementLocator) 
            {
                System.out.println("Wait for element:" + elementLocator + " does not exist"); 
                try
                {
                    new WebDriverWait(driver, timeout) {
                    }.until(new ExpectedCondition<Boolean>() {
                        
                        public Boolean apply(WebDriver driver) {
                            return (driver.findElements(elementLocator).size() < 1);
                        }
                    });
                }
                catch(TimeoutException e)
                {
                    String ErrorMsg = "Failed elementDoesNotExist, element: " + elementLocator.toString() + " - still present after a timeout of:" + timeout +" seconds at url: " + driver.getCurrentUrl();
                    
                    
                    throw new TimeoutException(ErrorMsg) ;
                }
            }

            public void elementsAreMoreThan(final int numberOfElements,final By elementLocator) {
                new WebDriverWait(driver, timeout) {
                }.until(new ExpectedCondition<Boolean>() {
                    
                    public Boolean apply(WebDriver driver) {
                        return (driver.findElements(elementLocator).size() > numberOfElements);
                    }
                });
            }

            public void elementsAreLessThan(final int numberOfElements,final By elementLocator) {
                new WebDriverWait(driver, timeout) {
                }.until(new ExpectedCondition<Boolean>() {
                    
                    public Boolean apply(WebDriver driver) {
                        return (driver.findElements(elementLocator).size() < numberOfElements);
                    }
                });
            }

            public void elementsNumberEqual(final int numberOfElements,final By elementLocator) {
                new WebDriverWait(driver, timeout) {
                }.until(new ExpectedCondition<Boolean>() {
                    
                    public Boolean apply(WebDriver driver) {
                        return (driver.findElements(elementLocator).size() == numberOfElements);
                    }
                });
            }

            public void elementsNumberDoNotEqual(final int numberOfElements,final By elementLocator) {
                new WebDriverWait(driver, timeout) {
                }.until(new ExpectedCondition<Boolean>() {
                    
                    public Boolean apply(WebDriver driver) {
                        return (driver.findElements(elementLocator).size() != numberOfElements);
                    }
                });
            }

            public void elementExistsAfterRefreshingPage(final By elementLocator) 
            {
                System.out.println("Wait for element: " + elementLocator + " exists after refreshing page"); 
                
                try
                {
                    new WebDriverWait(driver, timeout) {
                    }.until(new ExpectedCondition<Boolean>() {
                        
                        public Boolean apply(WebDriver driver) {
                            boolean bool;
                            driver.navigate().refresh();
                             if(driver.findElements(elementLocator).size() > 0)
                                 {
                                 bool = true;
                                 System.out.println(elementLocator + " FOUND");
                                 }
                             else bool = false;
                             
                             return bool;
                        }
                    });
                }
                catch(TimeoutException e)
                {
                    String ErrorMsg = "Failed elementExistsAfterRefreshingPage, element: " + elementLocator.toString() + " - Element not found at url: " + driver.getCurrentUrl();
                    
                    
                    throw new TimeoutException(ErrorMsg) ;
                }
            
            
                }

            public void doesNotExistAfterRefreshingPage(final By elementLocator) {
                new WebDriverWait(driver, timeout) {
                }.until(new ExpectedCondition<Boolean>() {
                    
                    public Boolean apply(WebDriver driver) {
                        driver.navigate().refresh();
                        return (driver.findElements(elementLocator).size() < 1);
                    }
                });
            }

            public void instancesAreMoreThanAfterRefreshingPage(final int instances,final By elementLocator) {
                new WebDriverWait(driver, timeout) {
                }.until(new ExpectedCondition<Boolean>() {
                    
                    public Boolean apply(WebDriver driver) {
                        driver.navigate().refresh();
                        return (driver.findElements(elementLocator).size() > instances);
                    }
                });
            }

            public void instancesAreLessThanAfterRefreshingPage(final int instances,final By elementLocator) {
                new WebDriverWait(driver, timeout) {
                }.until(new ExpectedCondition<Boolean>() {
                    
                    public Boolean apply(WebDriver driver) {
                        driver.navigate().refresh();
                        return (driver.findElements(elementLocator).size() < instances);
                    }
                });
            }

            public void instancesEqualAfterRefreshingPage(final int instances,final By elementLocator) {
                new WebDriverWait(driver, timeout) {
                }.until(new ExpectedCondition<Boolean>() {
                    
                    public Boolean apply(WebDriver driver) {
                        driver.navigate().refresh();
                        return driver.findElements(elementLocator).size() == instances;
                    }
                });
            }

            public void instancesDoNotEqualAfterRefreshingPage(final int instances,final By elementLocator) {
                new WebDriverWait(driver, timeout) {
                }.until(new ExpectedCondition<Boolean>() {
                    
                    public Boolean apply(WebDriver driver) {
                        driver.navigate().refresh();
                        return driver.findElements(elementLocator).size() != instances;
                    }
                });
            }

            public void elementTextIsEqualTo(final String text,final By elementLocator) 
            {
                System.out.println("Wait for element's text equal to:" + text);

                try
                {
                    new WebDriverWait(driver, timeout) {
                    }.until(new ExpectedCondition<Boolean>() {
                        
                        public Boolean apply(WebDriver driver) {
                            return driver.findElement(elementLocator).getText().equals(text);
                        }
                    });
                }
                catch(TimeoutException e)
                {
                    String ErrorMsg = "Failed wait for elementTextIsEqualTo text: " + text + " within timeout of : " + timeout + " seconds  at url:" + driver.getCurrentUrl();
                    
                    
                    throw new TimeoutException(ErrorMsg);
                }
            }

            public void elementTextDoesNotEqual(final String text, final By elementLocator) 
            {
                System.out.println("Wait for element's text does not contains:" + text);
                
                try
                {
                    new WebDriverWait(driver, timeout) {
                    }.until(new ExpectedCondition<Boolean>() {
                        
                        public Boolean apply(WebDriver driver) {
                            return !driver.findElement(elementLocator).getText().equals(text);
                        }
                    });
                }
                catch(TimeoutException e)
                {
                    String ErrorMsg = "Failed wait for elementTextDoesNotEqual text: " + text + " within timeout of : " + timeout + " seconds  at url:" + driver.getCurrentUrl();
                    
                    
                    throw new TimeoutException(ErrorMsg);
                }
            }

            
            //TODO fix the error message to return the previous text
            public void elementTextContains(final String text,final By elementLocator) 
            {
                System.out.println("Wait for element [" + elementLocator + "] to contains text [" + text + "]");
                
                try
                {
                    new WebDriverWait(driver, timeout) {
                    }.until(new ExpectedCondition<Boolean>() {
                        
                        public Boolean apply(WebDriver driver) {
                            return driver.findElement(elementLocator).getText().toLowerCase().contains(text.toLowerCase());
                        }
                    });
                }
                catch(TimeoutException e)
                {
                    String ErrorMsg = "Failed wait for element [" + elementLocator + "] to contains text [" + text + "] within timeout of : " + timeout + " seconds  at url:" + driver.getCurrentUrl();
                    ErrorMsg = ErrorMsg + "\n Actual content is [" + driver.findElement(elementLocator).getText().toLowerCase() + "]";
                    
                    throw new TimeoutException(ErrorMsg);
                }
                
            }

            public void textDoesNotContain(final String text,final By elementLocator) 
            {
                
                new WebDriverWait(driver, timeout) {
                }.until(new ExpectedCondition<Boolean>() {
                    
                    public Boolean apply(WebDriver driver) {
                        return !driver.findElement(elementLocator).getText().contains(text);
                    }
                });
            }

            public void titleIsEqualTo(final String text) 
            {
                System.out.println("Wait for page title equals to:" + text); 
                
                new WebDriverWait(driver, timeout) {
                }.until(new ExpectedCondition<Boolean>() {
                    
                    public Boolean apply(WebDriver driver) {
                        return driver.getTitle().equals(text);
                    }
                });
            }

            public void titleDoesNotEqual(final String text) 
            {
                System.out.println("Wait for page title not equals to:" + text); 
                
                new WebDriverWait(driver, timeout) {
                }.until(new ExpectedCondition<Boolean>() {
                    
                    public Boolean apply(WebDriver driver) {
                        return !driver.getTitle().equals(text);
                    }
                });
            }

            public void titleContains(final String text) {
               
                System.out.println("Wait for page title to contains:" + text); 
                
                try
                {
                    new WebDriverWait(driver, timeout) {
                    }.until(new ExpectedCondition<Boolean>() {
                        
                        public Boolean apply(WebDriver driver) {
                            return driver.getTitle().contains(text);
                        }
                    });
                }
                catch(TimeoutException e)
                {
                    String ErrorMsg = "Failed wait for titleContains text: " + text + " within timeout of : " + timeout + " seconds  at url:" + driver.getCurrentUrl();
                    
                    
                    throw new TimeoutException(ErrorMsg);
                }
            }

            public void titleDoesNotContain(final String text) 
            {
                System.out.println("Wait for title does not contains:" + text); 
                
                try
                {
                    new WebDriverWait(driver, timeout) {
                    }.until(new ExpectedCondition<Boolean>() {
                        
                        public Boolean apply(WebDriver driver) {
                            return !driver.getTitle().contains(text);
                        }
                    });
                }
                catch(TimeoutException e)
                {
                    String ErrorMsg = "Failed wait for titleDoesNotContain text: " + text + " within timeout of : " + timeout + " seconds  at url:" + driver.getCurrentUrl();
                    
                    
                    throw new TimeoutException(ErrorMsg);
                }
            }
        
            public void urlContains(final String text) throws UnsupportedEncodingException 
            {
                System.out.println("Wait for url to contains:" + text); 
                String url = null;
                
                try
                {
                    new WebDriverWait(driver, timeout) {
                    }.until(new ExpectedCondition<Boolean>() {
                        
                        public Boolean apply(WebDriver driver) {
                            String url = null;
                            try
                            {
                                url = java.net.URLDecoder.decode(driver.getCurrentUrl(), "UTF-8");
                            }
                            catch ( UnsupportedEncodingException e )
                            {
                            }
                            return url.contains( text );
                        }
                    });
                }
                catch(TimeoutException e)
                {
                    String ErrorMsg = "Failed wait for urlContains text: " + text + " within timeout of : " + timeout + " seconds  at url:" + url;
                    
                    
                    throw new TimeoutException(ErrorMsg);
                }
        
            }
}
