package com.abaenglish.qa.web;


import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.robotframework.javalib.library.AnnotationLibrary;


public class WebDriverLib extends AnnotationLibrary 
{
	 /*
     *  Means that that this instance of the class well be used
     *  for whole lifecycle of test execution.
     *   Libraries created from modules are always global.
     */
    public static final String ROBOT_LIBRARY_SCOPE = "GLOBAL";
    
	@SuppressWarnings("serial")
	private static final List<String> keywordPatterns = new ArrayList<String>() 
	{	
		{
			add("com/abaenglish/**/keyword/**/*.class");
		}
	};

	public WebDriverLib() {
        super(keywordPatterns);
      }

	/* Only one instance is created during the whole test execution
	 * and it is shared by all test cases and test suites. 
	 */
	protected  static WebDriver driver;
	protected  static Locator locator  = new Locator();
	protected  static Helper helper;
	protected  static Wait	waiter;

	
  
	   @Override
	    public String getKeywordDocumentation(String keywordName) {
	        if (keywordName.equals("__intro__"))
	            return "This is the general java WebDriverLib documentation.";
	        return super.getKeywordDocumentation(keywordName);
	    }	
	
	   
	public void log(String message)
	{
		System.out.println(message);
	}
	
}